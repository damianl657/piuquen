CREATE EVENT baja_lotes ON SCHEDULE EVERY 1 DAY DO 
UPDATE lotes 
set estado = 0 
where date(fecha_vencimiento) > DATE_SUB(CurDate(), INTERVAL 1 DAY)