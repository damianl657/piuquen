<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Servicios_destinos_eloquent extends Eloquent{
    protected $table = 'servicios_destinos';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    /**
     * Proveedores_eloquent has many Pedidos_eloquent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    /**
     * Contactos_destinos has many .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
   public function remitos_egresos()
    {
        return $this->hasMany('Remitos_egresos_eloquent','id_servicio','id');
    }
    public function items_detalle_factura_destino()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = pacientes_eloquent_id, localKey = id)
        return $this->hasMany('Items_detalle_factura_destino_eloquent','id_servicio','id');
    }
    
   
}