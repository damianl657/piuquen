<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Ingresos_eloquent extends Eloquent{
	protected $table = 'ingresos';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    /**
     * Pedidos_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    

    /**
     * Pedidos_eloquent belongs to Proveedores.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function proveedores()
    {
        // belongsTo(RelatedModel, foreignKey = proveedores_id, keyOnRelatedModel = id)
        return $this->belongsTo('Proveedores_eloquent','id_proveedor');
    }
    public function users()
    {
        // belongsTo(RelatedModel, foreignKey = proveedores_id, keyOnRelatedModel = id)
        return $this->belongsTo('Users_Eloquent','id_user');
    }
    /**
     * Ingresos_eloquent has one Pedidos_eloquent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    
    public function pedidos()
    {
        // belongsTo(RelatedModel, foreignKey = ingresos_eloquent_id, keyOnRelatedModel = id)
        return $this->belongsTo('Pedidos_eloquent','id_pedido');
    }
    /**
     * Ingresos_eloquent has many .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function remitos_ingresos()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = ingresos_eloquent_id, localKey = id)
        return $this->hasMany('Remitos_ingresos_eloquent','id_ingreso','id');
    }
  
    /*public function tipos_productos()
    {
        
        return $this->belongsTo('Tipos_productos_eloquent','id_tipo_prod');
    }*/
    
}