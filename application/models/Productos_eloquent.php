<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Productos_eloquent extends Eloquent{
    protected $table = 'productos';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    /**
     * Productos_eloquent has many .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function precios()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = productos_eloquent_id, localKey = id)
        return $this->hasMany('Precios_eloquent','id_producto','id');
    }
    /**
     * Productos_eloquent belongs to Lotes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lotes()
    {
        // belongsTo(RelatedModel, foreignKey = lotes_id, keyOnRelatedModel = id)
        return $this->hasMany('Lotes_eloquent','id_producto','id');
    }
    public function Items_remito_egreso()
    {
        // belongsTo(RelatedModel, foreignKey = lotes_id, keyOnRelatedModel = id)
        return $this->hasMany('Items_remito_egreso_eloquent','id_producto','id');
    }
    public function Items_factura()
    {
        // belongsTo(RelatedModel, foreignKey = lotes_id, keyOnRelatedModel = id)
        return $this->hasMany('Items_factura_eloquent','id_producto','id');
    }
    
    public function Items_detalle_factura()
    {
        // belongsTo(RelatedModel, foreignKey = lotes_id, keyOnRelatedModel = id)
        return $this->hasMany('Items_detalle_factura_eloquent','id_producto','id');
    }
    public function Items_detalle_factura_destino()
    {
        // belongsTo(RelatedModel, foreignKey = lotes_id, keyOnRelatedModel = id)
        return $this->hasMany('Items_detalle_factura_destino_eloquent','id_producto','id');
    }
  
    /*public function tipos_productos()
    {
        
        return $this->belongsTo('Tipos_productos_eloquent','id_tipo_prod');
    }*/
   
}