<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Pedidos_eloquent extends Eloquent{
	protected $table = 'pedidos';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    /**
     * Pedidos_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function items_pedidos()
    {
        // belongsTo(RelatedModel, foreignKey = _id, keyOnRelatedModel = id)
        return $this->hasMany('Items_pedidos_eloquent','id_pedido','id');
    }

    /**
     * Pedidos_eloquent belongs to Proveedores.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function proveedores()
    {
        // belongsTo(RelatedModel, foreignKey = proveedores_id, keyOnRelatedModel = id)
        return $this->belongsTo('Proveedores_eloquent','id_proveedor');
    }
    public function users()
    {
        // belongsTo(RelatedModel, foreignKey = proveedores_id, keyOnRelatedModel = id)
        return $this->belongsTo('Users_Eloquent','id_user');
    }
    /**
     * Pedidos_eloquent belongs to Ingresos_eloquent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    
    public function Ingresos()
    {
        // hasOne(RelatedModel, foreignKeyOnRelatedModel = ingresos_eloquent_id, localKey = id)
        return $this->hasOne('Ingresos_eloquent','id_pedido','id');
    }
  
    
  
    /*public function tipos_productos()
    {
        
        return $this->belongsTo('Tipos_productos_eloquent','id_tipo_prod');
    }*/
    
}