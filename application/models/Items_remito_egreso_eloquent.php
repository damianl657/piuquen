<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Items_remito_egreso_eloquent extends Eloquent{
	protected $table = 'items_remito_egreso';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    /**
     * Items_remito_ingreso_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lotes()
    {
    	// belongsTo(RelatedModel, foreignKey = _id, keyOnRelatedModel = id)
    	return $this->belongsTo('Lotes_eloquent','id_lote');
    }
    /**
     * Items_remito_egreso_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function remitos_egresos()
    {
    	// belongsTo(RelatedModel, foreignKey = _id, keyOnRelatedModel = id)
    	return $this->belongsTo('Remitos_egresos_eloquent','id_remito_egreso');
    }
    public function prductos()
    {
    	// belongsTo(RelatedModel, foreignKey = _id, keyOnRelatedModel = id)
    	return $this->belongsTo('Productos_eloquent','id_producto');
    }
    public function precios()
    {
    	// belongsTo(RelatedModel, foreignKey = _id, keyOnRelatedModel = id)
    	return $this->belongsTo('Precios_eloquent','id_precio');
    }
    public function contratos()
    {
    	// belongsTo(RelatedModel, foreignKey = _id, keyOnRelatedModel = id)
    	return $this->belongsTo('Contratos_eloquent','id_contrato');
    }
    
    /**
     * Items_remito_egreso_eloquent has many .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items_detalle_factura()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = items_remito_egreso_eloquent_id, localKey = id)
        return $this->hasMany('Items_detalle_factura_eloquent','id_items_remito_egreso','id');
    }
    public function items_detalle_factura_destino()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = items_remito_egreso_eloquent_id, localKey = id)
        return $this->hasMany('Items_detalle_factura_destino_eloquent','id_items_remito_egreso','id');
    }

    /**
     * Items_remito_ingreso_eloquent belongs to Remitos_ingresos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    
    /**
     * Pedidos_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    /**
     * Lotes_eloquent has many Productos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    
    
   
    
}