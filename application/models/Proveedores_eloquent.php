<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Proveedores_eloquent extends Eloquent{
    protected $table = 'proveedores';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    /**
     * Proveedores_eloquent has many Pedidos_eloquent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Pedidos()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = proveedores_eloquent_id, localKey = id)
        return $this->hasMany('Pedidos_eloquent','id_proveedor','id');
    }
    /**
     * Proveedores_eloquent has many Ingresos_eloquent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Ingresos()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = proveedores_eloquent_id, localKey = id)
        return $this->hasMany('Ingresos_eloquent','id_proveedores','id');
    }
    public function Remitos_ingresos()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = proveedores_eloquent_id, localKey = id)
        return $this->hasMany('Remitos_ingresos_eloquent','id_proveedores','id');
    }
    
  
    /*public function tipos_productos()
    {
        
        return $this->belongsTo('Tipos_productos_eloquent','id_tipo_prod');
    }*/
   
}