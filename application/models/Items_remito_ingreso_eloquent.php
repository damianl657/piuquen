
<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Items_remito_ingreso_eloquent extends Eloquent{
	protected $table = 'items_remito_ingreso';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    /**
     * Items_remito_ingreso_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lotes()
    {
    	// belongsTo(RelatedModel, foreignKey = _id, keyOnRelatedModel = id)
    	return $this->belongsTo('Lotes_eloquent','id_lote');
    }
    /**
     * Items_remito_ingreso_eloquent belongs to Remitos_ingresos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function remitos_ingresos()
    {
    	// belongsTo(RelatedModel, foreignKey = remitos_ingresos_id, keyOnRelatedModel = id)
    	return $this->belongsTo('Remitos_ingresos_eloquent','id_remito');
    }
    /**
     * Pedidos_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    /**
     * Lotes_eloquent has many Productos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    
    
   
    
}