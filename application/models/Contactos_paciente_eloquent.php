<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Contactos_pacientes_eloquent extends Eloquent{
    protected $table = 'contactos_pacientes';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    /**
     * Contactos_paciente_eloquent belongs to Pacientes_eloquent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pacientes()
    {
        // belongsTo(RelatedModel, foreignKey = pacientes_eloquent_id, keyOnRelatedModel = id)
        return $this->belongsTo('Pacientes_eloquent','id_paciente');
    }
    /**
     * Proveedores_eloquent has many Pedidos_eloquent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    
    
   
}