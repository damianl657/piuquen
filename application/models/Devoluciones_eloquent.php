<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Devoluciones_eloquent extends Eloquent{
	protected $table = 'devoluciones';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    /**
     * Pedidos_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    /**
     * Lotes_eloquent has many Productos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lotes()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = lotes_eloquent_id, localKey = id)
        return $this->belongsTo('Lotes_eloquent','id_lote');
    }
  
    /**
     * Devoluciones_eloquent has one Remitos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function remitos()
    {
        // hasOne(RelatedModel, foreignKeyOnRelatedModel = devoluciones_eloquent_id, localKey = id)
        return $this->belongsTo('Remitos_egresos_eloquent','id_remito_egreso');
    }

    public function users()
    {
        // hasOne(RelatedModel, foreignKeyOnRelatedModel = devoluciones_eloquent_id, localKey = id)
        return $this->belongsTo('Users_Eloquent','id_user');
    }
   
    
}