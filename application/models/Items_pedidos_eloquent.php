
<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Items_pedidos_eloquent extends Eloquent{
	protected $table = 'items_pedidos';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
  
    /**
     * Items_pedidos_eloquent has many Pedidos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pedidos()
    {
    	// hasMany(RelatedModel, foreignKeyOnRelatedModel = items_pedidos_eloquent_id, localKey = id)
    	return $this->belongsTo('Pedidos_eloquent','id_pedido');
    }
  
    /*public function tipos_productos()
    {
        
        return $this->belongsTo('Tipos_productos_eloquent','id_tipo_prod');
    }*/
    
}