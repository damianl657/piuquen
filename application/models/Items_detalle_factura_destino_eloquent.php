<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Items_detalle_factura_destino_eloquent extends Eloquent{
	protected $table = 'items_detalle_factura_destino';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    /**
     * Items_remito_ingreso_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    
    /**
     * Items_remito_egreso_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function facturas()
    {
    	// belongsTo(RelatedModel, foreignKey = _id, keyOnRelatedModel = id)
    	return $this->belongsTo('Facturas_eloquent','id_factura');
    }
    public function productos()
    {
    	// belongsTo(RelatedModel, foreignKey = _id, keyOnRelatedModel = id)
    	return $this->belongsTo('Productos_eloquent','id_producto');
    }
    /**
     * Items_detalle_factura_eloquent belongs to Lotes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lotes()
    {
        // belongsTo(RelatedModel, foreignKey = lotes_id, keyOnRelatedModel = id)
        return $this->belongsTo('Lotes_eloquent','id_lote');
    }
    public function items_remito_egreso()
    {
        // belongsTo(RelatedModel, foreignKey = lotes_id, keyOnRelatedModel = id)
        return $this->belongsTo('Items_remito_egreso_eloquent','id_items_remito_egreso');
    }
    /**
     * items_detalle_factura_destino_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function servicios_destinos()
    {
        // belongsTo(RelatedModel, foreignKey = _id, keyOnRelatedModel = id)
        return $this->belongsTo('Servicios_destinos_eloquent','id_servicio');
    }
    public function pacientes()
    {
        // belongsTo(RelatedModel, foreignKey = _id, keyOnRelatedModel = id)
        return $this->belongsTo('Pacientes_eloquent','id_paciente');
    }
    
}