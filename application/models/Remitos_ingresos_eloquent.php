<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Remitos_ingresos_eloquent extends Eloquent{
	protected $table = 'remitos_ingresos';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    /**
     * Pedidos_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    

    /**
     * Pedidos_eloquent belongs to Proveedores.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function proveedores()
    {
        // belongsTo(RelatedModel, foreignKey = proveedores_id, keyOnRelatedModel = id)
        return $this->belongsTo('Proveedores_eloquent','id_proveedor');
    }
    public function users()
    {
        // belongsTo(RelatedModel, foreignKey = proveedores_id, keyOnRelatedModel = id)
        return $this->belongsTo('Users_Eloquent','id_user');
    }
    public function ingresos()
    {
        // belongsTo(RelatedModel, foreignKey = proveedores_id, keyOnRelatedModel = id)
        return $this->belongsTo('Ingresos_eloquent','id_ingreso');
    }
    /**
     * Ingresos_eloquent has one Pedidos_eloquent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function items_remito_ingreso()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = lotes_eloquent_id, localKey = id)
        return $this->hasMany('Items_remito_ingreso_eloquent','id_remito','id');
    }
    
    
  
    /*public function tipos_productos()
    {
        
        return $this->belongsTo('Tipos_productos_eloquent','id_tipo_prod');
    }*/
    
}