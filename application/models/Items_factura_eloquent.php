<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Items_factura_eloquent extends Eloquent{
	protected $table = 'items_factura';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    /**
     * Items_remito_ingreso_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    
    /**
     * Items_remito_egreso_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function facturas()
    {
    	// belongsTo(RelatedModel, foreignKey = _id, keyOnRelatedModel = id)
    	return $this->belongsTo('Facturas_eloquent','id_factura');
    }
    public function productos()
    {
    	// belongsTo(RelatedModel, foreignKey = _id, keyOnRelatedModel = id)
    	return $this->belongsTo('Productos_eloquent','id_producto');
    }
    

    /**
     * Items_remito_ingreso_eloquent belongs to Remitos_ingresos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    
    /**
     * Pedidos_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    /**
     * Lotes_eloquent has many Productos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    
    
   
    
}