<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Remitos_egresos_eloquent extends Eloquent{
	protected $table = 'remitos_egresos';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    public function scopeNo_facturados($query)
    {
        return $query->where('id_factura', '0');
    }
    /**
     * Pedidos_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    

    /**
     * Pedidos_eloquent belongs to Proveedores.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    
    public function users()
    {
        // belongsTo(RelatedModel, foreignKey = proveedores_id, keyOnRelatedModel = id)
        return $this->belongsTo('Users_Eloquent','id_user');
    }
    public function pacientes()
    {
        // belongsTo(RelatedModel, foreignKey = proveedores_id, keyOnRelatedModel = id)
        return $this->belongsTo('Pacientes_eloquent','id_paciente');
    }
    public function servicios()
    {
        // belongsTo(RelatedModel, foreignKey = proveedores_id, keyOnRelatedModel = id)
        return $this->belongsTo('Servicios_destinos_eloquent','id_servicio');
    }
    /**
     * Ingresos_eloquent has one Pedidos_eloquent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    /**
     * Remitos_egresos_eloquent has many .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items_remito_egreso()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = remitos_egresos_eloquent_id, localKey = id)
        return $this->hasMany('Items_remito_egreso_eloquent','id_remito_egreso','id');
    }
    /**
     * Remitos_egresos_eloquent has many .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function remitos_facturados()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = remitos_egresos_eloquent_id, localKey = id)
        return $this->hasMany('Remitos_facturados_eloquent','id_remito','id');
    }
    public function devoluciones()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = remitos_egresos_eloquent_id, localKey = id)
        return $this->hasMany('Devoluciones_eloquent','id_remito_egreso','id');
    }
  
   
    
}