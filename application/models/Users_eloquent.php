<?php defined('BASEPATH') OR exit('No direct script access allowed');

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Users_Eloquent extends Eloquent{
	protected $table = 'users';
    public $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;
    

    public function users_groups()
    {
        return $this->hasMany('Users_groups_eloquent', 'user_id','id');
    }
    public function pedidos()
    {
    	return $this->hasMany('Users_groups_eloquent', 'user_id','id');
    }
    /**
     * Users_eloquent has many Ingresos_eloquent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Ingresos()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = users_eloquent_id, localKey = id)
        return $this->hasMany('Ingresos_eloquent', 'user_id','id');
    }
    public function Remitos_ingresos()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = proveedores_eloquent_id, localKey = id)
        return $this->hasMany('Remitos_ingresos_eloquent','user_id','id');
    }
    public function remitos_egresos()
    {
        return $this->hasMany('Remitos_egresos_eloquent','user_id','id');
    }
    /**
     * Users_eloquent has many Devoluciones.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function devoluciones()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = users_eloquent_id, localKey = id)
        return $this->hasMany('Devoluciones_eloquent','user_id','id');
    }
    /**
     * Users_eloquent has many Facturas.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function facturas()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = users_eloquent_id, localKey = id)
        return $this->hasMany('Facturas_eloquent','id_user','id');
    }
    
    
}