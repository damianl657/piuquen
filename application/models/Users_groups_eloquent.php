	<?php 
use \Illuminate\Database\Eloquent\Model as Eloquent;

class Users_groups_eloquent extends Eloquent{
	protected $table = 'users_groups';
    protected $primaryKey = 'id';
    public $timestamps = false;
    //public $incrementing = false;

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    /*public function clientes()
    {
        return $this->hasOne('clientes_eloquent','id_user_group','id');
    }*/
    
    
    public function users()
    {
    	return $this->belongsTo('users_eloquent','user_id');
    	//return $this->belongsTo('users_eloquent');
    }
    /**
     * Users_groups_eloquent has one Groups.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function groups()
    {
        // hasOne(RelatedModel, foreignKeyOnRelatedModel = users_groups_eloquent_id, localKey = id)
        return $this->hasOne('Groups_eloquent','id','id');
    }
    
}