<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Contratos_eloquent extends Eloquent{
    protected $table = 'contratos';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    /**
     * Contratos_eloquent has many Precios.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function precios()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = contratos_eloquent_id, localKey = id)
        return $this->hasMany('Precios_eloquent','id_contrato','id');
    }
    /**
     * Contratos_eloquent has many .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items_remito_egreso()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = contratos_eloquent_id, localKey = id)
        return $this->hasMany('Items_remito_egreso_eloquent','id_contrato','id');
    }
    /**
     * Contratos_eloquent has one .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function contrato_actual()
    {
        // hasOne(RelatedModel, foreignKeyOnRelatedModel = contratos_eloquent_id, localKey = id)
        return $this->hasOne('Contrato_actual_eloquent','id_contrato','id');
    }
    /**
     * Contratos_eloquent has many .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function facturas()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = contratos_eloquent_id, localKey = id)
        return $this->hasMany('Facturas_eloquent','id_contrato','id');
    }
    public function institucion()
    {
        // hasOne(RelatedModel, foreignKeyOnRelatedModel = contratos_eloquent_id, localKey = id)
        return $this->hasOne('Instituciones_eloquent','id','id_institucion');
    }
    
  
    /*public function tipos_productos()
    {
        
        return $this->belongsTo('Tipos_productos_eloquent','id_tipo_prod');
    }*/
   
}