<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Instituciones_eloquent extends Eloquent{
    protected $table = 'instituciones';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    
}
?>