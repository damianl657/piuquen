<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Contactos_destinos_eloquent extends Eloquent{
    protected $table = 'contactos_destinos';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    /**
     * Proveedores_eloquent has many Pedidos_eloquent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    /**
     * Contactos_destinos has many .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contactos_pacientes()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = contactos_destinos_id, localKey = id)
        return $this->hasMany('Contactos_pacientes_eloquent','id_contacto','id');
    }
    
   
}