<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Facturas_eloquent extends Eloquent{
	protected $table = 'facturas';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    
    /**
     * Pedidos_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    

    /**
     * Pedidos_eloquent belongs to Proveedores.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    
    public function users()
    {
        // belongsTo(RelatedModel, foreignKey = proveedores_id, keyOnRelatedModel = id)
        return $this->belongsTo('Users_Eloquent','id_user');
    }
    
    /**
     * Ingresos_eloquent has one Pedidos_eloquent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    /**
     * Remitos_egresos_eloquent has many .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items_factura()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = remitos_egresos_eloquent_id, localKey = id)
        return $this->hasMany('Items_factura_eloquent','id_factura','id');
    }
    
    public function items_detalle_factura()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = remitos_egresos_eloquent_id, localKey = id)
        return $this->hasMany('Items_detalle_factura_eloquent','id_factura','id');
    }

    public function contratos()
    {
        // belongsTo(RelatedModel, foreignKey = _id, keyOnRelatedModel = id)
        return $this->belongsTo('Contratos_eloquent','id_contrato');
    }
    /**
     * Facturas_eloquent has many .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function remitos_facturados()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = facturas_eloquent_id, localKey = id)
        return $this->hasMany('Remitos_facturados_eloquent','id_factura','id');
    }
    public function items_detalle_factura_destinos()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = remitos_egresos_eloquent_id, localKey = id)
        return $this->hasMany('Items_detalle_factura_destino_eloquent','id_factura','id');
    }
   
    
}