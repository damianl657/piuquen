<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Precios_eloquent extends Eloquent{
    protected $table = 'precios';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    /**
     * Precios_eloquent belongs to Productos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productos()
    {
        // belongsTo(RelatedModel, foreignKey = productos_id, keyOnRelatedModel = id)
        return $this->belongsTo('Productos_eloquent','id_producto');
    }
    /**
     * Precios_eloquent belongs to Contratos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contratos()
    {
        // belongsTo(RelatedModel, foreignKey = contratos_id, keyOnRelatedModel = id)
        return $this->belongsTo('Contratos_eloquent','id_contrato');
    }
    /**
     * Precios_eloquent has many .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items_remito_egreso()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = precios_eloquent_id, localKey = id)
        return $this->hasMany('Items_remito_egreso_eloquent','id_precio','id');
    }
    
  
    /*public function tipos_productos()
    {
        
        return $this->belongsTo('Tipos_productos_eloquent','id_tipo_prod');
    }*/
   
}