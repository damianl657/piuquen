<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Lotes_eloquent extends Eloquent{
	protected $table = 'lotes';
    protected $primaryKey = 'id';
    public $timestamps = false;
    

    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);
    }*/
    public function scopeActivos($query)
    {
        return $query->where('estado', '1');
    }
    /**
     * Pedidos_eloquent belongs to .
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    /**
     * Lotes_eloquent has many Productos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productos()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = lotes_eloquent_id, localKey = id)
        return $this->belongsTo('Productos_eloquent','id_producto');
    }
    /**
     * Lotes_eloquent has many Items_remito_ingreso.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items_remito_ingreso()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = lotes_eloquent_id, localKey = id)
        return $this->hasMany('Items_remito_ingreso_eloquent','id_lote','id');
    }
    /**
     * Lotes_eloquent has many Items_remito_egreso_eloquent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items_remito_egreso()
    {
        // hasMany(RelatedModel, foreignKeyOnRelatedModel = lotes_eloquent_id, localKey = id)
        return $this->hasMany('Items_remito_egreso_eloquent','id_lote','id');
    }
     public function devoluciones()
    {
        // belongsTo(RelatedModel, foreignKey = lotes_id, keyOnRelatedModel = id)
        return $this->hasMany('Devoluciones_eloquent','id_lote','id');
    }
    public function Items_detalle_factura()
    {
        // belongsTo(RelatedModel, foreignKey = lotes_id, keyOnRelatedModel = id)
        return $this->hasMany('Items_detalle_factura_eloquent','id_lote','id');
    }
    public function Items_detalle_factura_destino()
    {
        // belongsTo(RelatedModel, foreignKey = lotes_id, keyOnRelatedModel = id)
        return $this->hasMany('Items_detalle_factura_destino_eloquent','id_lote','id');
    }
    
   
    
}