<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;



class Groups_eloquent extends Eloquent{
	protected $table = 'groups';
    protected $primaryKey = 'id';
    public $timestamps = false;
    
    /*function __construct()
    {
        
    }*/
    
    
     /**
     * Scope a query to only include users of a given type.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
   /* public function scoperoles($query, $type)
    {
        return $query->where('tipo', $type);

    }*/
    /**
     * Groups_eloquent belongs to Users_groups.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users_groups()
    {
        // belongsTo(RelatedModel, foreignKey = users_groups_id, keyOnRelatedModel = id)
        return $this->belongsTo('Users_groups_eloquent','group_id');
    }
}