<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Facturas_Model extends CI_Model
{
	public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }

	function getListaPaginado($postData=null){

        $response = array();
   
        ## Read value
        $draw = $_POST['draw'];
        $start = $_POST['start'];
        $rowperpage = $_POST['length']; // Rows display per page
        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
        $searchValue = $_POST['search']['value']; // Search value
        
        ## Search 
        $searchQuery = "";
        if($searchValue != ''){
           $searchQuery = " and (facturas.fecha like '%".$searchValue."%' or facturas.señor like '%".$searchValue."%' or facturas.direccion like'%".$searchValue."%' or facturas.monto_total like '%".$searchValue."%' )";
        }
   
        ## Total number of records without filtering

      
        $sql = "SELECT facturas.id as allcount FROM facturas where facturas.visible = 1 and facturas.estado = 1";
        $sql = $this->db->query($sql);
     
        $totalRecords = $sql->num_rows();
      

        $sql = "SELECT facturas.id FROM facturas  where facturas.visible = 1 and facturas.estado = 1";
        if($searchQuery != '')
            $sql = $sql.' '.$searchQuery;
        $sql = $this->db->query($sql);
       
        $totalRecordwithFilter =$sql->num_rows();
   
     
   
        $sql = "SELECT facturas.* FROM facturas where facturas.visible = 1 and facturas.estado = 1 ";
        if($searchQuery != '')
            $sql = $sql.' '.$searchQuery;
        $sql = $sql." ORDER BY $columnName $columnSortOrder";
        $sql = $sql." LIMIT $rowperpage OFFSET $start";
        $records = $this->db->query($sql);
        $records = $records->result();
        $data = array();
   
        foreach($records as $record ){
           
           $data[] = array( 
                "fecha"=>$record->fecha,
                "señor"=>$record->señor,
                "direccion"=>$record->direccion,
                "monto_total"=>$record->monto_total,
                
                "acciones" =>'<a class="btn btn-sm btn-success" onclick="ver_detalles('.$record->id.')" title="Ver Detalles"><i class="fa fa-list"></i> </a>'
           ); 
        }
   
        ## Response
        $response = array(
           "draw" => intval($draw),
           "iTotalRecords" => $totalRecords,
           "iTotalDisplayRecords" => $totalRecordwithFilter,
           "aaData" => $data,
           "lastquery" => $this->db->last_query()
        );
   
        return $response; 
    }
}
