<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Container\Container as Container;
use \Illuminate\Support\Facades\Facade as Facade;



class Lotes extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Groups_eloquent');
		$this->load->model('Users_groups_eloquent');
		$this->load->model('Users_eloquent');
        $this->load->model('Productos_eloquent');
        $this->load->model('Lotes_eloquent');
		//$this->load->library(array('ion_auth','funciones'));
		
	}
	
	public function lotes()
	{
		$aux['controlador'] = 'Productos';
		$aux['metodo'] = 'index';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        	//$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
        	$data['contenido'] = "/parametros/lotes/lotes";
			$this->load->view('templates/templateCodebase', $data);
        }
        else
        {
        	echo "error de permiso";
            die();
        }
	}
    public function listar_lotes()
    {
        $data['listado'] = Lotes_eloquent::activos()                                           
                                            ->get();
                                        
        $this->load->view('/parametros/lotes/listar_lotes', $data);
    }
    public function editVencimiento($id)
	{
		$aux['controlador'] = 'Lotes';
		$aux['metodo'] = 'editVencimiento';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
            $data['lote'] = Lotes_eloquent::find($id);
        	//$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
        	$data['contenido'] = "/parametros/lotes/editVencimiento";
			$this->load->view('templates/templateCodebase', $data);
        }
        else
        {
        	echo "error de permiso";
            die();
        }
	}
    public function registrarEditVencimiento()
    {
        $aux['controlador'] = 'Lotes';
		$aux['metodo'] = 'editVencimiento';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
            $lote = Lotes_eloquent::find($this->input->post('id'));
            $fecha_vencimiento = $this->input->post('fecha_vencimiento');
                    //Procesamiento de fecha y hora
            if($fecha_vencimiento != '')
            {

                      $fecha_vencimiento = str_replace('/', '-', $fecha_vencimiento);
                      $separa = explode("-",$fecha_vencimiento);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha_vencimiento = $anio.'-'.$mes.'-'.$dia;
            }
            $lote->fecha_vencimiento = $fecha_vencimiento;
                //$lote->estado = 1;
            $lote->save();
            echo json_encode(array(
            "status" => $lote->save(),
            ));  
        }
        else
        {
            echo "error de permiso";
            die();
        }
    }
    
	

}