<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Container\Container as Container;
use \Illuminate\Support\Facades\Facade as Facade;



class Egresos_remito extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Groups_eloquent');
		$this->load->model('Users_groups_eloquent');
		$this->load->model('Users_eloquent');
        $this->load->model('Proveedores_eloquent');
        $this->load->model('Lotes_eloquent');
        $this->load->model('Items_remito_ingreso_eloquent');
        $this->load->model('Pedidos_eloquent');
        $this->load->model('Remitos_ingresos_eloquent');
        $this->load->model('Ingresos_eloquent');
        $this->load->model('Productos_eloquent');
        $this->load->model('Remitos_egresos_eloquent');
        $this->load->model('Pacientes_eloquent');
        $this->load->model('Servicios_destinos_eloquent');
        $this->load->model('Items_remito_egreso_eloquent');
        $this->load->model('Precios_eloquent');
        $this->load->model('Contrato_actual_eloquent');
        $this->load->model('Contratos_eloquent');
        
		//$this->load->library(array('ion_auth','funciones'));
		
	}
	
	public function index()
	{
		$aux['controlador'] = 'Egresos_remito';
		$aux['metodo'] = 'index';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {

        	//$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
        	$data['contenido'] = "egresos/index";
            

			$this->load->view('templates/templateCodebase', $data);
        }
        else
        {
        	echo "error de permiso";
            die();
        }
	}
	public function listar_egresos()
    {
         $id_ingreso = $this->input->get('id_ingreso');
        $data['listado'] = Remitos_egresos_eloquent::activos()->get();
          //print_r($id_ingreso);                              
        $this->load->view('egresos/listar_egresos', $data);
        //echo json_encode(array("status" => TRUE, "clientes" => $this->load->view('clientes/listar_clientes', $data)));
    }
    public function nuevo()
	{
		$aux['controlador'] = 'Egresos_remito';
		$aux['metodo'] = 'nuevo';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        	$this->cart->destroy();    	
			
            $data['productos'] = Productos_eloquent::activos()->get();
            $data['pacientes'] = Pacientes_eloquent::activos()->get();
            $data['servicios'] = Servicios_destinos_eloquent::activos()->get();
			$data['contenido'] = "egresos/nuevo";
            
			//print_r($data['bancos']);
			//die();
			$this->load->view('templates/templateCodebase', $data);
        }
        else
        {
        	echo"error permiso";
        	die();
        }
	}
	public function guardar()
	{
		$egreso = new Remitos_egresos_eloquent();
        
        
        //$producto->id_tipo_prod = $this->input->post('id_tipo');
        $fecha = $this->input->post('fecha');
                    //Procesamiento de fecha y hora
                    if($fecha != '')
                    {

                      $fecha = str_replace('/', '-', $fecha);
                      $separa = explode("-",$fecha);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha = $anio.'-'.$mes.'-'.$dia;
                    }
       
        $egreso->fecha = $fecha;            
        
        if($this->input->post('id_servicio')!= 'ninguno')
        {
        	$egreso->id_servicio = $this->input->post('id_servicio');
            $egreso->id_paciente = 0;
        }
        else if($this->input->post('id_paciente')!= 'ninguno')
        {
        	$egreso->id_paciente = $this->input->post('id_paciente');
            $egreso->id_servicio = 0;
        }

        $egreso->id_factura = 0;
        $egreso->id_user = $this->session->userdata('user_id');  
        $egreso->monto_total = 0; 
        $egreso->estado = '1';
        
        $egreso->save();
        $contrato_actual = Contrato_actual_eloquent::find('1');
        foreach ( $this->cart->contents() as $item)
                {
                    if($item['name'] == 'egreso')
                    {
                        $datos = $item['options'];
                        
                        //
                        $cantidad_requerida = $datos['cantidad'];
			            $id_producto = $datos['id_producto'];
			            $lotes = Lotes_eloquent::where('id_producto',$id_producto)
                        ->where('cantidad','>',0)
                        ->activos()->get();
			            $total_cantidad = 0;
			            					            	//obtener_precio
					    $precios_aux = Precios_eloquent::where('id_producto',$id_producto)->where('id_contrato',$contrato_actual->id_contrato)->get();
					    $contador_precio = 0;
					    foreach ($precios_aux as $key ) {
					    	$precio = $key;
					    	$contador_precio = $contador_precio + 1;
					    }
					    if($contador_precio > 0)
					    {
                            $producto = Productos_eloquent::find($id_producto);

					    	foreach ($lotes as $lote) 
				            {
                                if($producto->con_vencimiento == 1)
                                {
                                    $fechaVencimiento=strtotime($lote->fecha_vencimiento);
                                    $fechaActual = date('Y-m-d');
                                    $fechaActual =strtotime($fechaActual);
                                    if($fechaVencimiento > $fechaActual)
                                    {
                                        $total_cantidad = $total_cantidad + $lote->cantidad;
                                    }
                                }
                                else
                                {
                                    $total_cantidad = $total_cantidad + $lote->cantidad;
                                }
				                
				            }
				            if($total_cantidad >= $cantidad_requerida)
				            {
				                while ($cantidad_requerida > 0) 
				                {
				                	//var_dump($this->input->post('id_producto'));
					                //echo "venicmiento:";
					                
                                    if($producto->con_vencimiento == 1)
                                    {
                                        $lotes = Lotes_eloquent::where('id_producto',$id_producto)
                                        ->where('cantidad','>',0)
                                        ->where('fecha_vencimiento','>',date('Y-m-d'))
                                        ->activos()->get();
                                        $min_fecha_vencimiento = $lotes->min('fecha_vencimiento');
                                    }
                                    else
                                    {
                                        $lotes = Lotes_eloquent::where('id_producto',$id_producto)
                                        ->where('cantidad','>',0)
                                        ->activos()->get();
                                        $min_fecha_vencimiento = $lotes->min('fecha_vencimiento');
                                    }
					                //var_dump($min_fecha_vencimiento);

					                foreach ($lotes as $lote) 
						            {
						                if($lote->fecha_vencimiento == $min_fecha_vencimiento)
						                {
						                	//print_r($lote);
						                	$lote_a_decrementar = $lote;
						                }

						            }
						            if($lote_a_decrementar->cantidad <= $cantidad_requerida)
						            {
						            	$items_egreso = new Items_remito_egreso_eloquent();
						            	$items_egreso->cantidad = $lote_a_decrementar->cantidad;
						            	$items_egreso->id_producto =  $id_producto;
						            	$items_egreso->id_lote = $lote_a_decrementar->id;
						            	$items_egreso->id_precio = $precio->id;
						            	$items_egreso->id_contrato = $contrato_actual->id_contrato;
						            	$items_egreso->estado = 1;
						            	$items_egreso->id_remito_egreso = $egreso->id;
						            	$items_egreso->save();
						            	$lote_a_decrementar->cantidad = 0;
						            	$lote_a_decrementar->estado = 0;
						            	$lote_a_decrementar->save();

						            	$cantidad_requerida = $cantidad_requerida - $items_egreso->cantidad;
						            }
						            else
						            {
						            	$items_egreso = new Items_remito_egreso_eloquent();
						            	$items_egreso->cantidad = $cantidad_requerida;
						            	$items_egreso->id_producto =  $id_producto;
						            	$items_egreso->id_lote = $lote_a_decrementar->id;
						            	$items_egreso->id_precio = $precio->id;
						            	$items_egreso->id_contrato = $contrato_actual->id_contrato;
						            	$items_egreso->estado = 1;
						            	$items_egreso->id_remito_egreso = $egreso->id;
						            	$items_egreso->save();

						            	$aux = $lote_a_decrementar->cantidad -$cantidad_requerida;
						            	$lote_a_decrementar->cantidad = $aux;
						            	$lote_a_decrementar->save();
						            	$cantidad_requerida = 0;

						            }
				                }
					                

				            }
					    }
                    }
                }
        $this->cart->destroy(); 
        $acum_total = 0;
        foreach ($egreso->items_remito_egreso as $key ) 
        {
            $precio_unitario = Precios_eloquent::where('id_contrato',$key->id_contrato)->where('id_producto',$key->id_producto)->get();
                                        $cont_pre = 0;
                                        foreach ($precio_unitario as $pre_unit) 
                                        {
                                           $cont_pre = $cont_pre + 1;
                                           $precio = $pre_unit;
                                        }
                                        $acum_total = $acum_total + ($key->cantidad * $precio->precio);           
               }  
        $egreso->monto_total = $acum_total;
        $egreso->save();  
        echo json_encode(array(
            "status" => TRUE,
            "id_egreso" => $egreso->id,
            
            )); 
	}
	public function detalle()
    {
        $id = $this->input->get('id');
        //echo $id;
        $data['egreso'] = Remitos_egresos_eloquent::find($id);
        $this->load->view('egresos/detalle', $data);
             
    }
    public function modificar($id)
    {
        $aux['controlador'] = 'Egresos_remito';
        $aux['metodo'] = 'modificar';
        /*$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        //print_r($permisos);
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        //print_r($obtener_permiso);
        if($obtener_permiso)
        {*/
           // echo "vista en produccion";
        $this->cart->destroy();    
        $data['contenido'] = "egresos/editar";
        $data['egreso'] = Remitos_egresos_eloquent::find($id);
        $data['productos'] = Productos_eloquent::activos()->get();
            $data['pacientes'] = Pacientes_eloquent::activos()->get();
            $data['servicios'] = Servicios_destinos_eloquent::activos()->get();   
            
            //print_r($cliente->users_groups->users->documento);
           // die();
            $this->load->view('templates/templateCodebase', $data);
       // }
       
    }
    public function updateVisibilidad()
    {
        $egreso = Remitos_egresos_eloquent::find($this->input->post('id'));
        $egreso->visible = $this->input->post('visible');
        $egreso->save();

        echo json_encode(array(
            "status" => $egreso->save(),
            ));  
    }
    public function imprimir_remito($id)
    {
        $data['contenido'] = "egresos/editar";
        $egreso = Remitos_egresos_eloquent::find($id);
        $data['egreso'] = $egreso;
        $data['productos'] = Productos_eloquent::activos()->get();
            $data['pacientes'] = Pacientes_eloquent::activos()->get();
            $data['servicios'] = Servicios_destinos_eloquent::activos()->get();   
            if($egreso->id_servicio != 0)
            {
                $this->load->view('egresos/imprimir_remito_servicio',$data);
            }
            else
            {
                $this->load->view('egresos/imprimir_remito',$data);
            }
        
    }
    public function update()
    {
    	$egreso = Remitos_egresos_eloquent::find($this->input->post('id'));
        $fecha = $this->input->post('fecha');
                    //Procesamiento de fecha y hora
                    if($fecha != '')
                    {

                      $fecha = str_replace('/', '-', $fecha);
                      $separa = explode("-",$fecha);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha = $anio.'-'.$mes.'-'.$dia;
                    }
        $egreso->fecha = $fecha;            
        if($this->input->post('id_servicio')!= 'ninguno')
        {
            $egreso->id_servicio = $this->input->post('id_servicio');
            $egreso->id_paciente = 0;
        }
        else if($this->input->post('id_paciente')!= 'ninguno')
        {
            $egreso->id_paciente = $this->input->post('id_paciente');
            $egreso->id_servicio = 0;
        }
        
        $egreso->nro_remito = $this->input->post('nro_remito');
        $egreso->save();

        echo json_encode(array(
            "status" => $egreso->save(),
            ));  
    }
    
    public function agregar_items()//grupo familiar
    {
        if(!$this->input->is_ajax_request())
        {
            redirect('404');
        }
        else 
        {
        	$cantidad_requerida = $this->input->post('cantidad');
            $id_producto = $this->input->post('id_producto');
            $lotes = Lotes_eloquent::where('id_producto',$id_producto)->activos()->get();
            $acum = 0;
            foreach ($lotes as $lote) 
            {
                $acum = $acum + $lote->cantidad;
            }
            if($cantidad_requerida <= $acum)
            {
                $cont = 1;
	            foreach ( $this->cart->contents() as $item) 
	            {
	                $cont = $cont + 1;  
	            }
	            $data = array(
	            'id'      => $cont,
	            'qty'     => 1,
	            'price'   => 1,
	            'name'    => 'egreso',
	            'options' => array(
	                'id_producto' => $this->input->post('id_producto'), 
	                'cantidad' => $this->input->post('cantidad'),
	                ));
	            $this->cart->insert($data);
	            $this->load->view('egresos/carrito_egreso');
            }
            else
            {
                $this->load->view('egresos/carrito_egreso'); 
            }
	            
            
        
        }
    }
    public function eliminar_items()//eliminar integrante del grupo familiar del carrito
    {
        if(!$this->input->is_ajax_request())
        {
            redirect('404');
        }
        else 
        {
            $id = $this->input->get('id_items');
            $this->cart->remove($id);
            //print_r($this->cart->contents());
            //die();
            $this->load->view('egresos/carrito_egreso');
            
        
        }
    }
   

}