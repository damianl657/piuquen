<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Container\Container as Container;
use \Illuminate\Support\Facades\Facade as Facade;



class Servicios_destinos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Groups_eloquent');
		$this->load->model('Users_groups_eloquent');
		$this->load->model('Users_eloquent');
        $this->load->model('Servicios_destinos_eloquent');
		//$this->load->library(array('ion_auth','funciones'));
		
	}
	
	public function index()
	{
		$aux['controlador'] = 'Servicios_destinos';
		$aux['metodo'] = 'index';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        	//$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
        	$data['contenido'] = "/parametros/servicios_destinos/index";
			$this->load->view('templates/template_admin', $data);
        }
        else
        {
        	echo "error de permiso";
            die();
        }
	}
	public function listar_servicios_destinos()
    {
        $data['listado'] = Servicios_destinos_eloquent::activos()                                           
                                            ->get();
                                        
        $this->load->view('/parametros/servicios_destinos/listar_servicios_destinos', $data);
        //echo json_encode(array("status" => TRUE, "clientes" => $this->load->view('clientes/listar_clientes', $data)));
    }
    public function nuevo()
	{
		$aux['controlador'] = 'Servicios_destinos';
		$aux['metodo'] = 'nuevo';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        		
			
			$data['contenido'] = "/parametros/servicios_destinos/nuevo";
            
			//print_r($data['bancos']);
			//die();
			$this->load->view('templates/template_admin', $data);
        }
        else
        {
        	echo"error permiso";
        	die();
        }
	}
	public function guardar()
	{
		//echo "ok";
		$servicio = new Servicios_destinos_eloquent();
        $servicio->nombre = $this->input->post('nombre');
        $servicio->descripcion = $this->input->post('descripcion');
        $servicio->direccion = $this->input->post('direccion');
        $servicio->iva = $this->input->post('iva');
        $servicio->condicion_venta = $this->input->post('condicion_venta');
        $servicio->cuit = $this->input->post('cuit');
        $servicio->razon_social = $this->input->post('razon_social');
        

        $servicio->contacto1 = $this->input->post('contacto1');
        
        $servicio->contacto2 = $this->input->post('contacto2');
        
        $servicio->contacto3 = $this->input->post('contacto3');
        
        $servicio->contacto4 = $this->input->post('contacto4');
        
        $servicio->contacto5 = $this->input->post('contacto5');
        
        //$producto->id_tipo_prod = $this->input->post('id_tipo');
        
        $servicio->estado = '1';
        $servicio->save();

        echo json_encode(array(
            "status" => $servicio->save(),
            
            ));        
	}
	public function detalle()
    {
        $id = $this->input->get('id');
        //echo $id;
        $servicio = Servicios_destinos_eloquent::find($id);
        $data['servicio'] =  $servicio;
        $this->load->view('/parametros/servicios_destinos/detalle', $data);
             
    }
    public function modificar($id)
    {
        $aux['controlador'] = 'Servicios_destinos';
        $aux['metodo'] = 'modificar';
        /*$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        //print_r($permisos);
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        //print_r($obtener_permiso);
        if($obtener_permiso)
        {*/
           // echo "vista en produccion";
            
        $data['contenido'] = "/parametros/servicios_destinos/editar";
        $data['servicio'] = Servicios_destinos_eloquent::find($id);
            
            
            //print_r($cliente->users_groups->users->documento);
           // die();
            $this->load->view('templates/template_admin', $data);
       // }
       
    }
    public function update()
    {
    	$servicio = Servicios_destinos_eloquent::find($this->input->post('id'));
    	$servicio->nombre = $this->input->post('nombre');
        $servicio->descripcion = $this->input->post('descripcion');
        $servicio->direccion = $this->input->post('direccion');
        $servicio->contacto = $this->input->post('contacto');
        $servicio->iva = $this->input->post('iva');
        $servicio->condicion_venta = $this->input->post('condicion_venta');
        $servicio->cuit = $this->input->post('cuit');
        $servicio->razon_social = $this->input->post('razon_social');
        $servicio->contacto1 = $this->input->post('contacto1');
        
        $servicio->contacto2 = $this->input->post('contacto2');
        
        $servicio->contacto3 = $this->input->post('contacto3');
        
        $servicio->contacto4 = $this->input->post('contacto4');
        
        $servicio->contacto5 = $this->input->post('contacto5');
        
        //$producto->id_tipo_prod = $this->input->post('id_tipo');
        
        $servicio->estado = '1';
        $servicio->save();


        echo json_encode(array(
            "status" => $servicio->save(),
            ));  
    }  
    public function baja()
    {
        //$cliente = Clientes_eloquent::find($id);
        $servicio = Servicios_destinos_eloquent::find($this->input->post('id_servicio'));
        $servicio->estado = '0';
        $servicio->save();
        echo json_encode(array("status" => true));
        //echo "hola mundo";
    } 

}