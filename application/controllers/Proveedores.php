<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Container\Container as Container;
use \Illuminate\Support\Facades\Facade as Facade;



class Proveedores extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Groups_eloquent');
		$this->load->model('Users_groups_eloquent');
		$this->load->model('Users_eloquent');
        $this->load->model('Proveedores_eloquent');
		//$this->load->library(array('ion_auth','funciones'));
		
	}
	
	public function index()
	{
		$aux['controlador'] = 'Proveedores';
		$aux['metodo'] = 'index';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        	//$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
        	$data['contenido'] = "/parametros/proveedores/index";
			$this->load->view('templates/template_admin', $data);
        }
        else
        {
        	echo "error de permiso";
            die();
        }
	}
	public function listar_proveedores()
    {
        $data['listado'] = Proveedores_eloquent::activos()                                           
                                            ->get();
                                        
        $this->load->view('/parametros/proveedores/listar_proveedores', $data);
        //echo json_encode(array("status" => TRUE, "clientes" => $this->load->view('clientes/listar_clientes', $data)));
    }
    public function nuevo()
	{
		$aux['controlador'] = 'Proveedores';
		$aux['metodo'] = 'nuevo';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        		
			
			$data['contenido'] = "/parametros/proveedores/nuevo";
            
			//print_r($data['bancos']);
			//die();
			$this->load->view('templates/template_admin', $data);
        }
        else
        {
        	echo"error permiso";
        	die();
        }
	}
	public function guardar()
	{
		//echo "ok";
		$proveedor = new Proveedores_eloquent();
        $proveedor->nombre = $this->input->post('nombre');
        $proveedor->cuit = $this->input->post('cuit');
        $proveedor->demora = $this->input->post('demora');
        //$producto->id_tipo_prod = $this->input->post('id_tipo');
        
        $proveedor->estado = '1';
        $proveedor->save();

        echo json_encode(array(
            "status" => $proveedor->save(),
            
            ));        
	}
	public function detalle()
    {
        $id = $this->input->get('id');
        //echo $id;
        $proveedor = Proveedores_eloquent::find($id);
        
             echo json_encode(array("status" => TRUE, 
            "proveedor" => $proveedor, 
           
            ));
    }
    public function modificar($id)
    {
        $aux['controlador'] = 'Proveedores';
        $aux['metodo'] = 'modificar';
        /*$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        //print_r($permisos);
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        //print_r($obtener_permiso);
        if($obtener_permiso)
        {*/
           // echo "vista en produccion";
            
        $data['contenido'] = "/parametros/proveedores/editar";
        $data['proveedor'] = Proveedores_eloquent::find($id);
            
            
            //print_r($cliente->users_groups->users->documento);
           // die();
            $this->load->view('templates/template_admin', $data);
       // }
       
    }
    public function update()
    {
    	$proveedor = Proveedores_eloquent::find($this->input->post('id'));
    	$proveedor->nombre = $this->input->post('nombre');
        $proveedor->cuit = $this->input->post('cuit');
        $proveedor->demora = $this->input->post('demora');
       // $producto->id_tipo_prod = $this->input->post('id_tipo');
        //$tipo->estado = '1';
        $proveedor->save();

        echo json_encode(array(
            "status" => $proveedor->save(),
            ));  
    }  
    public function baja()
    {
        //$cliente = Clientes_eloquent::find($id);
        $proveedor = Proveedores_eloquent::find($this->input->post('id_proveedor'));
        $proveedor->estado = '0';
        $proveedor->save();
        echo json_encode(array("status" => true));
        //echo "hola mundo";
    } 

}