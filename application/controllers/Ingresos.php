<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Container\Container as Container;
use \Illuminate\Support\Facades\Facade as Facade;



class Ingresos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Groups_eloquent');
		$this->load->model('Users_groups_eloquent');
		$this->load->model('Users_eloquent');
        $this->load->model('Proveedores_eloquent');
        $this->load->model('Lotes_eloquent');
        $this->load->model('Items_remito_ingreso_eloquent');
        $this->load->model('Pedidos_eloquent');
        $this->load->model('Remitos_ingresos_eloquent');
        $this->load->model('Ingresos_eloquent');
        $this->load->model('Devoluciones_eloquent');
        $this->load->model('Remitos_egresos_eloquent');
        $this->load->model('Productos_eloquent');
		//$this->load->library(array('ion_auth','funciones'));
		
	}
	
	public function index()
	{
		$aux['controlador'] = 'Ingresos';
		$aux['metodo'] = 'index';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        	//$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
        	$data['contenido'] = "ingresos/index";
			$this->load->view('templates/templateCodebase', $data);
        }
        else
        {
        	echo "error de permiso";
            die();
        }
	}
	public function listar_ingresos()
    {
        $data['listado'] = Ingresos_eloquent::activos()                                           
                                            ->get();
                                        
        $this->load->view('ingresos/listar_ingresos', $data);
        //echo json_encode(array("status" => TRUE, "clientes" => $this->load->view('clientes/listar_clientes', $data)));
    }
    public function nuevo($id_pedido)
	{
		$aux['controlador'] = 'Ingresos';
		$aux['metodo'] = 'nuevo';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        		
			$data['pedido'] = Pedidos_eloquent::find($id_pedido);
			$data['contenido'] = "ingresos/nuevo";
            
			//print_r($data['bancos']);
			//die();
			$this->load->view('templates/templateCodebase', $data);
        }
        else
        {
        	echo"error permiso";
        	die();
        }
	}
	public function guardar()
	{
		//echo "ok";
		$ingreso = new Ingresos_eloquent();
        $fecha = $this->input->post('fecha');
                    //Procesamiento de fecha y hora
                    if($fecha != '')
                    {

                      $fecha = str_replace('/', '-', $fecha);
                      $separa = explode("-",$fecha);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha = $anio.'-'.$mes.'-'.$dia;
                    }
        $ingreso->fecha = $fecha;
        $ingreso->factura = $this->input->post('factura');
        $ingreso->total = $this->input->post('total');
        $ingreso->id_proveedor = $this->input->post('id_proveedor');
        $ingreso->id_pedido = $this->input->post('id_pedido');
        $ingreso->id_user = $this->session->userdata('user_id');
        $pedido = Pedidos_eloquent::find($ingreso->id_pedido);
        $pedido->estado_pedido = '---';
        $pedido->save();
        //$producto->id_tipo_prod = $this->input->post('id_tipo');
        
        $ingreso->estado = '1';
        $ingreso->save();

        echo json_encode(array(
            "status" => $ingreso->save(),
            ));        
	}
	public function detalle()
    {
        $id = $this->input->get('id');
        //echo $id;
        $ingreso = Ingresos_eloquent::find($id);
        
             echo json_encode(array("status" => TRUE, 
            "ingreso" => $ingreso, 
           
            ));
    }
    public function modificar($id)
    {
        $aux['controlador'] = 'Ingresos';
        $aux['metodo'] = 'modificar';
        /*$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        //print_r($permisos);
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        //print_r($obtener_permiso);
        if($obtener_permiso)
        {*/
           // echo "vista en produccion";
            
        $data['contenido'] = "ingresos/editar";
        $data['ingreso'] = Ingresos_eloquent::find($id);
            
            
            //print_r($cliente->users_groups->users->documento);
           // die();
            $this->load->view('templates/templateCodebase', $data);
       // }
       
    }
    public function update()
    {
    	$ingreso = Ingresos_eloquent::find($this->input->post('id'));
    	$fecha = $this->input->post('fecha');
                    //Procesamiento de fecha y hora
                    if($fecha != '')
                    {

                      $fecha = str_replace('/', '-', $fecha);
                      $separa = explode("-",$fecha);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha = $anio.'-'.$mes.'-'.$dia;
                    }
        $ingreso->fecha = $fecha;
        $ingreso->factura = $this->input->post('factura');
        $ingreso->total = $this->input->post('total');
        
        $ingreso->id_user = $this->session->userdata('user_id');
       // $producto->id_tipo_prod = $this->input->post('id_tipo');
        //$tipo->estado = '1';
        $ingreso->save();

        echo json_encode(array(
            "status" => $ingreso->save(),
            ));  
    }  
    public function baja()
    {
        //$cliente = Clientes_eloquent::find($id);
        $ingreso = Proveedores_eloquent::find($this->input->post('id_ingreso'));
        $ingreso->estado = '0';
        $ingreso->save();
        echo json_encode(array("status" => true));
        //echo "hola mundo";
    } 
    public function pedidos()
    {
        $aux['controlador'] = 'Ingresos';
        $aux['metodo'] = 'index';
        $permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
            //$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
            $data['contenido'] = "/ingresos/pedidos";
            $this->load->view('templates/templateCodebase', $data);
        }
        else
        {
            echo "error de permiso";
            die();
        }
    }
    
    public function listar_pedidos()
    {
        $data['listado'] = Pedidos_eloquent::activos()                                           
                                            ->get();
                                        
        $this->load->view('/ingresos/listar_pedidos', $data);
        //echo json_encode(array("status" => TRUE, "clientes" => $this->load->view('clientes/listar_clientes', $data)));
    }
    //////-----------------------devoluciones------------------
    public function devoluciones()
    {
        $aux['controlador'] = 'Ingresos';
        $aux['metodo'] = 'devoluciones';
        $permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
            //$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
            $data['contenido'] = "ingresos/devoluciones";
            $this->load->view('templates/template_admin', $data);
        }
        else
        {
            echo "error de permiso";
            die();
        }
    }
    
    public function listar_devoluciones()
    {
        $data['listado'] = Devoluciones_eloquent::activos()                                           
                                            ->get();
                                        
        $this->load->view('ingresos/listar_devoluciones', $data);
        //echo json_encode(array("status" => TRUE, "clientes" => $this->load->view('clientes/listar_clientes', $data)));
    }
    public function nueva_devolucion()
    {
        $aux['controlador'] = 'Ingresos';
        $aux['metodo'] = 'nueva_devolucion';
        $permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
            $this->cart->destroy();     
            
            $data['lotes']= Lotes_eloquent::get();
            $data['remitos'] = Remitos_egresos_eloquent::activos()->get();
            //$data['productos'] = Productos_eloquent::activos()->get();
            $data['contenido'] = "ingresos/nueva_devolucion";
            
            //print_r($data['bancos']);
            //die();
            $this->load->view('templates/template_admin', $data);
        }
        else
        {
            echo"error permiso";
            die();
        }
    }
    public function guardar_devolucion()
    {
        $fecha = $this->input->post('fecha');
                    //Procesamiento de fecha y hora
                    if($fecha != '')
                    {

                      $fecha = str_replace('/', '-', $fecha);
                      $separa = explode("-",$fecha);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha = $anio.'-'.$mes.'-'.$dia;
                    }
        
        $devolucion = new Devoluciones_eloquent();
        $devolucion->id_remito_egreso = $this->input->post('id_remito');
        $devolucion->id_lote = $this->input->post('id_lote');
        $devolucion->motivo = $this->input->post('motivo');
        $devolucion->cantidad = $this->input->post('cantidad');
        $devolucion->fecha = $fecha;
        $devolucion->id_user = $this->session->userdata('user_id');
        $devolucion->estado = 1;
        $devolucion->save();

        $lote = Lotes_eloquent::find($this->input->post('id_lote'));
        $cantidad = $lote->cantidad + $this->input->post('cantidad');
        $lote->estado = 1;
        $lote->cantidad = $cantidad;
        $lote->save();
        echo json_encode(array(
            "status" => $devolucion->save(),
            )); 
    }
}