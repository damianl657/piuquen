<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Container\Container as Container;
use \Illuminate\Support\Facades\Facade as Facade;



class Pedidos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Groups_eloquent');
		$this->load->model('Users_groups_eloquent');
		$this->load->model('Users_eloquent');
        $this->load->model('Productos_eloquent');
        $this->load->model('Pedidos_eloquent');
        $this->load->model('Items_pedidos_eloquent');
        $this->load->model('Proveedores_eloquent');
		//$this->load->library(array('ion_auth','funciones'));
		
	}
	
	public function index()
	{
		$aux['controlador'] = 'Pedidos';
		$aux['metodo'] = 'index';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        	//$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
        	$data['contenido'] = "/pedidos/index";
			$this->load->view('templates/template_admin', $data);
        }
        else
        {
        	echo "error de permiso";
            die();
        }
	}
    public function prueba_relacion()
    {
        $pedido = Pedidos_eloquent::find(1);
        
        print_r($pedido->items_pedidos);
        //var_dump($pedido->items_pedidos);
        
    }
	public function listar_pedidos()
    {
        $data['listado'] = Pedidos_eloquent::activos()                                           
                                            ->get();
                                        
        $this->load->view('/pedidos/listar_pedidos', $data);
        //echo json_encode(array("status" => TRUE, "clientes" => $this->load->view('clientes/listar_clientes', $data)));
    }
    public function nuevo()
	{
		$aux['controlador'] = 'Pedidos';
		$aux['metodo'] = 'nuevo';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        	$this->cart->destroy();	
			$data['proveedores'] = Proveedores_eloquent::activos()->get();
            $data['productos'] = Productos_eloquent::activos()->get();
			$data['contenido'] = "/pedidos/nuevo";
            
			//print_r($data['bancos']);
			//die();
			$this->load->view('templates/template_admin', $data);
        }
        else
        {
        	echo"error permiso";
        	die();
        }
	}
    public function agregar_items()//grupo familiar
    {
        if(!$this->input->is_ajax_request())
        {
            redirect('404');
        }
        else 
        {
            $cont = 1;
            foreach ( $this->cart->contents() as $item) 
            {
                $cont = $cont + 1;  
            }
            
            /*if($this->input->post('precio_txt') == 0)
            {
                $precio = 1;
            }*/
            
            $data = array(
            'id'      => $cont,
            'qty'     => 1,
            'price'   => 1,
            'name'    => 'pedido',
            'options' => array(
                'id_producto' => $this->input->post('id_producto'), 
                'cantidad' => $this->input->post('cantidad'),
                ));
            $this->cart->insert($data);
            $this->load->view('pedidos/carrito_pedido');
            
        
        }
    }
    public function eliminar_items()//eliminar integrante del grupo familiar del carrito
    {
        if(!$this->input->is_ajax_request())
        {
            redirect('404');
        }
        else 
        {
            $id = $this->input->get('id_individuo');
            $this->cart->remove($id);
            //print_r($this->cart->contents());
            //die();
            $this->load->view('pedidos/carrito_pedido');
            
        
        }
    }
	public function guardar()
	{
		//echo "ok";
		$pedido = new Pedidos_eloquent();
        
        
        //$producto->id_tipo_prod = $this->input->post('id_tipo');
        $fecha = $this->input->post('fecha');
                    //Procesamiento de fecha y hora
                    if($fecha != '')
                    {

                      $fecha = str_replace('/', '-', $fecha);
                      $separa = explode("-",$fecha);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha = $anio.'-'.$mes.'-'.$dia;
                    }
        $fecha_llegada = $this->input->post('fecha_llegada');
                    //Procesamiento de fecha y hora
                    if($fecha_llegada != '')
                    {

                      $fecha_llegada = str_replace('/', '-', $fecha_llegada);
                      $separa = explode("-",$fecha_llegada);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha_llegada = $anio.'-'.$mes.'-'.$dia;
                    }
        $pedido->fecha = $fecha;            
        $pedido->fecha_llegada = $fecha_llegada;  
        $pedido->id_proveedor =  $this->input->post('id_proveedor'); 
        $pedido->id_user = $this->session->userdata('user_id');   
        $pedido->estado = '1';
        $pedido->estado_pedido = 'pedido';
        $pedido->save();

        foreach ( $this->cart->contents() as $item)
                {
                    if($item['name'] == 'pedido')
                    {
                        $datos = $item['options'];
                        
                        $items_pedidos = new Items_pedidos_eloquent();
                        $items_pedidos->id_producto = $datos['id_producto'];
                        $items_pedidos->cantidad = $datos['cantidad'];
                        $items_pedidos->estado = '1';
                        $items_pedidos->id_pedido = $pedido->id;
                        $items_pedidos->save();

                        
                    }
                }
        $this->cart->destroy();        
        echo json_encode(array(
            "status" => $pedido->save(),
            
            ));        
	}
	public function detalle()
    {
        $id = $this->input->get('id');
        //echo $id;
        $pedido = Pedidos_eloquent::find($id);
        
             echo json_encode(array("status" => TRUE, 
            "pedido" => $pedido, 
            "proveedor" =>$pedido->proveedores,
            "items_pedidos" =>$pedido->items_pedidos,
            "productos" => Productos_eloquent::activos()->get(),
           
            ));
    }
    public function modificar($id)
    {
        $aux['controlador'] = 'Pedidos';
        $aux['metodo'] = 'modificar';
        /*$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        //print_r($permisos);
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        //print_r($obtener_permiso);
        if($obtener_permiso)
        {*/
           // echo "vista en produccion";
            
        $data['contenido'] = "/pedidos/editar";
        $data['pedido'] = Pedidos_eloquent::find($id);
        $data['proveedores'] = Proveedores_eloquent::activos()->get();
        $data['productos'] = Productos_eloquent::activos()->get();  
            
            //print_r($cliente->users_groups->users->documento);
           // die();
            $this->load->view('templates/template_admin', $data);
       // }
       
    }
    public function update()
    {
    	$pedido = Pedidos_eloquent::find($this->input->post('id'));
    	$fecha = $this->input->post('fecha');
                    //Procesamiento de fecha y hora
                    if($fecha != '')
                    {

                      $fecha = str_replace('/', '-', $fecha);
                      $separa = explode("-",$fecha);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha = $anio.'-'.$mes.'-'.$dia;
                    }
        $fecha_llegada = $this->input->post('fecha_llegada');
                    //Procesamiento de fecha y hora
                    if($fecha_llegada != '')
                    {

                      $fecha_llegada = str_replace('/', '-', $fecha_llegada);
                      $separa = explode("-",$fecha_llegada);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha_llegada = $anio.'-'.$mes.'-'.$dia;
                    }
        $pedido->fecha = $fecha;            
        $pedido->fecha_llegada = $fecha_llegada; 
        $pedido->id_proveedor =  $this->input->post('id_proveedor'); 
        $pedido->id_user = $this->session->userdata('user_id');   
       // $producto->id_tipo_prod = $this->input->post('id_tipo');
        //$tipo->estado = '1';
        $pedido->save();
        foreach ( $this->cart->contents() as $item)
                {
                   if($item['name'] == 'pedido')
                    {
                        $datos = $item['options'];
                        if($datos['id_pedido'] == $this->input->post('id'))
                        {

                            $datos = $item['options'];
                        
                            $items_pedidos = new Items_pedidos_eloquent();
                            $items_pedidos->id_producto = $datos['id_producto'];
                            $items_pedidos->cantidad = $datos['cantidad'];
                            $items_pedidos->estado = '1';
                            $items_pedidos->id_pedido = $pedido->id;
                            $items_pedidos->save();
                        }
                            
                    }
                }
                $this->cart->destroy();

        echo json_encode(array(
            "status" => $pedido->save(),
            ));  
    }  
    public function baja()
    {
        //$cliente = Clientes_eloquent::find($id);
        $pedido = Pedidos_eloquent::find($this->input->post('id_pedido'));
        $pedido->estado = '0';
        $pedido->save();
        echo json_encode(array("status" => true));
        //echo "hola mundo";
    } 
    public function baja_items_pedido()
    {
        $items = Items_pedidos_eloquent::find($this->input->post('id_items_pedido'));
        $items->estado = '0';
        $items->save();
        echo json_encode(array("status" => true, "id_pedido" => $items->pedidos->id ));
    }
    public function modificar_items($id)
    {
        $aux['controlador'] = 'Pedidos';
        $aux['metodo'] = 'modificar_items';
        $data['items'] = Items_pedidos_eloquent::find($id);
        $data['productos'] = Productos_eloquent::activos()->get();  
        $data['contenido'] = "/pedidos/editar_items";
        $this->load->view('templates/template_admin', $data);
    }
    public function update_items()
    {
        $items = Items_pedidos_eloquent::find($this->input->post('id'));
        $items->cantidad = $this->input->post('cantidad');
        $items->id_producto = $this->input->post('id_producto');
       // $producto->id_tipo_prod = $this->input->post('id_tipo');
        //$tipo->estado = '1';
        $items->save();

        echo json_encode(array(
            "status" => $items->save(),
            ));  
    }
    public function agregar_items_2()
    {
        if(!$this->input->is_ajax_request())
        {
            redirect('404');
        }
        else 
        {
            $cont = 1;
            foreach ( $this->cart->contents() as $item) 
            {
                $cont = $cont + 1;  
            }
            
            /*if($this->input->post('precio_txt') == 0)
            {
                $precio = 1;
            }*/
            
            $data = array(
            'id'      => $cont,
            'qty'     => 1,
            'price'   => 1,
            'name'    => 'pedido',
            'options' => array(
                'id_producto' => $this->input->post('id_producto'), 
                'cantidad' => $this->input->post('cantidad'),
                'id_pedido' => $this->input->post('id')
                ));
            $this->cart->insert($data);
            $pedido = Pedidos_eloquent::find($this->input->post('id'));
            $data['pedido'] = $pedido;
            $this->load->view('Pedidos/carrito_pedido_editar', $data);
        }
    }
    public function eliminar_items_cart()//eliminar integrante del grupo familiar del carrito
    {
        if(!$this->input->is_ajax_request())
        {
            redirect('404');
        }
        else 
        {
            $this->CI = get_instance();
            $id = $this->input->get('id_items');
            //$id_cliente = $this->input->get('id_cliente');
            $this->cart->remove($id);
            //print_r($this->cart->contents());
            //die();
            $id_pedido = $this->CI->input->get_request_header('id_pedido');
            $pedido = Pedidos_eloquent::find($id_pedido);
            $data['pedido'] = $pedido;
             $this->load->view('Pedidos/carrito_pedido_editar', $data);
            
        
        }
    }

}