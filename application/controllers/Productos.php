<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Container\Container as Container;
use \Illuminate\Support\Facades\Facade as Facade;



class Productos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Groups_eloquent');
		$this->load->model('Users_groups_eloquent');
		$this->load->model('Users_eloquent');
        $this->load->model('Productos_eloquent');
        $this->load->model('Lotes_eloquent');
		//$this->load->library(array('ion_auth','funciones'));
		
	}
	
	public function index()
	{
		$aux['controlador'] = 'Productos';
		$aux['metodo'] = 'index';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        	//$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
        	$data['contenido'] = "/parametros/productos/index";
			$this->load->view('templates/templateCodebase', $data);
        }
        else
        {
        	echo "error de permiso";
            die();
        }
	}
	public function listar_productos()
    {
        $data['listado'] = Productos_eloquent::activos()                                           
                                            ->get();
                                        
        $this->load->view('/parametros/productos/listar_productos', $data);
        //echo json_encode(array("status" => TRUE, "clientes" => $this->load->view('clientes/listar_clientes', $data)));
    }
    public function nuevo()
	{
		$aux['controlador'] = 'Productos';
		$aux['metodo'] = 'nuevo';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        		
			
			$data['contenido'] = "/parametros/productos/nuevo";
            
			//print_r($data['bancos']);
			//die();
			$this->load->view('templates/templateCodebase', $data);
        }
        else
        {
        	echo"error permiso";
        	die();
        }
	}
	public function guardar()
	{
		//echo "ok";
		$producto = new Productos_eloquent();
        $producto->nombre = $this->input->post('nombre');
        $producto->alias1 = $this->input->post('alias1');
        $producto->alias2 = $this->input->post('alias2');
        $producto->alias3 = $this->input->post('alias3');
        $producto->descripcion = $this->input->post('descripcion');
        
        $producto->con_vencimiento = $this->input->post('con_vencimiento');
        //$producto->descripcion = $this->input->post('descripcion');
        //$producto->id_tipo_prod = $this->input->post('id_tipo');
        
        $producto->estado = '1';
        $producto->save();

        echo json_encode(array(
            "status" => $producto->save(),
            
            ));        
	}
	public function detalle()
    {
        $id = $this->input->get('id');
        //echo $id;
        $producto = Productos_eloquent::find($id);
        
             echo json_encode(array("status" => TRUE, 
            "producto" => $producto, 
           
            ));
    }
    public function modificar($id)
    {
        $aux['controlador'] = 'Productos';
        $aux['metodo'] = 'modificar';
        /*$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        //print_r($permisos);
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        //print_r($obtener_permiso);
        if($obtener_permiso)
        {*/
           // echo "vista en produccion";
            
        $data['contenido'] = "/parametros/productos/editar";
        $data['producto'] = Productos_eloquent::find($id);
            
            
            //print_r($cliente->users_groups->users->documento);
           // die();
            $this->load->view('templates/templateCodebase', $data);
       // }
       
    }
    public function update()
    {
    	$producto = Productos_eloquent::find($this->input->post('id'));
    	$producto->nombre = $this->input->post('nombre');
        $producto->alias1 = $this->input->post('alias1');
        $producto->alias2 = $this->input->post('alias2');
        $producto->alias3 = $this->input->post('alias3');
        $producto->descripcion = $this->input->post('descripcion');
        $producto->con_vencimiento = $this->input->post('con_vencimiento');
        //$producto->descripcion = $this->input->post('descripcion');
       // $producto->id_tipo_prod = $this->input->post('id_tipo');
        //$tipo->estado = '1';
        $producto->save();

        echo json_encode(array(
            "status" => $producto->save(),
            ));  
    }  
    public function baja()
    {
        //$cliente = Clientes_eloquent::find($id);
        $producto = Productos_eloquent::find($this->input->post('id_producto'));
        $producto->estado = '0';
        $producto->save();
        echo json_encode(array("status" => true));
        //echo "hola mundo";
    } 
    public function obtener_cantidad()
    {
        if(!$this->input->is_ajax_request())
        {
            redirect('404');
        }
        else 
        {
            $this->CI = get_instance();
            $cantidad_requerida = $this->input->get('cantidad');
            $id_producto = $this->CI->input->get_request_header('id_producto');
            $lotes = Lotes_eloquent::where('id_producto',$id_producto)->activos()->get();
            $acum = 0;
            $producto = Productos_eloquent::find($id_producto);
            foreach ($lotes as $lote) 
            {
                /*validar vencimiento */
                if($producto->con_vencimiento == 1)
                {
                    $fechaVencimiento=strtotime($lote->fecha_vencimiento);
                    $fechaActual = date('Y-m-d');
                    $fechaActual =strtotime($fechaActual);
                    if($fechaVencimiento > $fechaActual)
                    {
                        $acum = $acum + $lote->cantidad;
                    }
                }
                else
                {
                    $acum = $acum + $lote->cantidad;
                }
                
                
            }
            if($cantidad_requerida  <= $acum)
            {
                echo json_encode(array(
                    "status" => true,
                    "mensaje"=> "Hay disponibles: ".$acum." unidades ",
                    )); 
            }
            else
            {
                echo json_encode(array(
                    "status" => true,
                    "mensaje"=> "No hay : ".$cantidad_requerida. " unidades, Hay disponibles: ".$acum." unidades ",
                    )); 
            }
        
        }
    }
    public function stock()
    {
        $aux['controlador'] = 'Productos';
        $aux['metodo'] = 'stock';
        $permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
            $data['productos'] = Productos_eloquent::activos()                                           
                                            ->get();
           
            //echo "hola";
            //die();
            //$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
            $data['contenido'] = "/parametros/productos/stock";
            $this->load->view('templates/templateCodebase', $data);
        }
        else
        {
            echo "error de permiso";
            die();
        }
    }
    public function traer_lotes()
    {
        $id = $this->input->get('id_producto');
        
        //echo $id;
        $data['producto'] = Productos_eloquent::find($id);
        $data['listado'] = $lotes = Lotes_eloquent::where('id_producto',$id)->get();
                                        
        $this->load->view('/parametros/productos/detalle_stock', $data);

    }

}