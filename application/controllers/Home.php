<?php defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}



	public function index()
	{
		
		/*if($this->session->userdata('nombregrupo') == 'admin')
		{
			$data['contenido'] = 'home/index';
			$data['titulo'] = 'Tienda';
			$this->load->view('templates/template_admin', $data);
		}
		else redirect()*/

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			//redirect('auth/login', 'refresh');
			//$data['contenido'] = 'auth/login';
			
			$this->load->view('auth/login');
		}
		/*elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			//return show_error('You must be an administrator to view this page.');
			
		}*/
		else if ($this->session->userdata('name_group') == 'admin')
		{
			print_r( $this->session->userdata('name_group') );
			$data['contenido'] = 'home/indexCB';
			//$data['titulo'] = 'xxx';
			$this->load->view('templates/templateCodebase', $data);
		}
		else if ($this->session->userdata('name_group') == 'drogueriaHospital')
		{
			$data['contenido'] = 'home/indexCB';
			//$data['titulo'] = 'xxx';
			$this->load->view('templates/templateCodebase', $data);
		}	
		else
		{
			return show_error('No tiene roles asignados');	
		}
	}
}
?>