﻿<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Container\Container as Container;
use \Illuminate\Support\Facades\Facade as Facade;
set_time_limit(9000);
ini_set('MAX_EXECUTION_TIME', 9000);


class Facturas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Groups_eloquent');
		$this->load->model('Users_groups_eloquent');
		$this->load->model('Users_eloquent');
        $this->load->model('Productos_eloquent');
        $this->load->model('Contratos_eloquent');
        $this->load->model('Contrato_actual_eloquent');
        $this->load->model('Proveedores_eloquent');
        $this->load->model('Lotes_eloquent');
        $this->load->model('Items_remito_ingreso_eloquent');
        $this->load->model('Pedidos_eloquent');
        $this->load->model('Remitos_ingresos_eloquent');
        $this->load->model('Ingresos_eloquent');
        $this->load->model('Remitos_egresos_eloquent');
        $this->load->model('Pacientes_eloquent');
        $this->load->model('Servicios_destinos_eloquent');
        $this->load->model('Items_remito_egreso_eloquent');
        $this->load->model('Precios_eloquent');
        $this->load->model('Facturas_eloquent');
        $this->load->model('Remitos_facturados_eloquent');
        $this->load->model('Items_factura_eloquent');
        $this->load->model('Items_detalle_factura_eloquent');
        $this->load->model('Items_detalle_factura_destino_eloquent');
       
		//$this->load->library(array('ion_auth','funciones'));
		
	}
	public function index()
    {
        $aux['controlador'] = 'Facturas';
        $aux['metodo'] = 'index';
        $permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
            //$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
            $data['contenido'] = "/facturas/index";
            $this->load->view('templates/templateCodebase', $data);
        }
        else
        {
            echo "error de permiso";
            die();
        }
    }
    public function listar_facturas()
    {
        $data['listado'] = Facturas_eloquent::activos()->get();
          //print_r($id_ingreso);                              
        $this->load->view('facturas/listar_facturas', $data);
    }
	public function remitos_no_facturados()
	{
		$aux['controlador'] = 'Facturas';
		$aux['metodo'] = 'remitos_no_facturados';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        	//$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
             $data['contratos'] = Contratos_eloquent::activos()                                           
                                            ->get();
            $actual = Contrato_actual_eloquent::find('1');
            $data['actual'] = Contratos_eloquent::find($actual->id_contrato);
        	$data['contenido'] = "/facturas/remitos_no_facturados";
			$this->load->view('templates/templateCodebase', $data);
        }
        else
        {
        	echo "error de permiso";
            die();
        }
	}

	public function listar_remitos_no_facturados()
    {
        $data['listado'] = Remitos_egresos_eloquent::activos()->No_facturados()->get();
        $this->load->view('/facturas/lista_remitos_no_facturados', $data);
        //echo json_encode(array("status" => TRUE, "clientes" => $this->load->view('clientes/listar_clientes', $data)));
    }
    public function nuevo()
    {
        //print_r($_POST);
        $this->cart->destroy(); 
        $id_contrato = $this->input->post('id_contrato');
        $remitos = Remitos_egresos_eloquent::activos()->No_facturados()->get();
        $precios = Precios_eloquent::where('id_contrato',$id_contrato)->activos()->get();

        foreach ($remitos as $remito) 
        {
                if(isset($_POST['checkbox'.$remito->id]))
                {
                    //echo "ok";
                    $cont = 1;
                    foreach ( $this->cart->contents() as $item) 
                    {
                        $cont = $cont + 1;  
                    }
                    $data = array(
                    'id'      => $cont,
                    'qty'     => 1,
                    'price'   => 1,
                    'name'    => 'factura',
                    'options' => array(
                        'id_remito' => $remito->id, 
                        ));
                    $this->cart->insert($data);
                }
        } 
        $data['precios'] = $precios;
        $data['id_contrato'] = $id_contrato;
        $data['contenido'] = "/facturas/nuevo";
        $data['productos'] = Productos_eloquent::activos()->get();
        $this->load->view('templates/templateCodebase', $data);

    }
    public function guardar()
    {
        $factura = new Facturas_eloquent();
        $factura->señor = $this->input->post('señor');
        $factura->direccion = $this->input->post('direccion');
        $factura->id_contrato = $this->input->post('id_contrato');
        $factura->id_user = $this->session->userdata('user_id'); 
        $fecha = $this->input->post('fecha');

                    //Procesamiento de fecha y hora
                    if($fecha != '')
                    {

                      $fecha = str_replace('/', '-', $fecha);
                      $separa = explode("-",$fecha);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha = $anio.'-'.$mes.'-'.$dia;
                    }
        $factura->fecha = $fecha;
        $factura->monto_total = 0;
        $factura->estado = 1;
        $factura->save();

        $productos = Productos_eloquent::activos()->get();
        
        $acum = 0;
        foreach ($productos as $producto)
        {
                              foreach ($this->cart->contents() as $item)
                              {

                                if($item['name'] == 'factura')
                                {
                                  $dato = $item['options'];

                                  
                                  $remito = Remitos_egresos_eloquent::find($dato['id_remito']);
                                  foreach ($remito->items_remito_egreso as $item_remito) 
                                  {
                                    if($item_remito->id_producto == $producto->id)
                                    {
                                      $acum = $acum + $item_remito->cantidad;
                                     
                                      //buscar si ya existe el items detalle factura, si existe si incrementa la cantidad
                                      $aux_items_detalle = Items_detalle_factura_eloquent::where('id_factura',$factura->id)->where('id_producto',$producto->id)->where('id_lote',$item_remito->id_lote)->get();
                                      $cont_items_detalle = 0;
                                      foreach ($aux_items_detalle as $aux_item_detalle) 
                                      {
                                        $cont_items_detalle =$cont_items_detalle + 1;
                                        $aux_detalle = $aux_item_detalle;
                                      }
                                      if($cont_items_detalle == 1)
                                        {   
                                            $cantidad_aux = $aux_detalle->cantidad;
                                            $sub_total_aux = $aux_detalle->sub_total;

                                            $aux_detalle->cantidad = $cantidad_aux + $item_remito->cantidad;
                                            $aux_detalle->sub_total = $sub_total_aux + ($item_remito->cantidad * $aux_detalle->precio_unitario);
                                            $aux_detalle->remitos_egresos = $aux_detalle->remitos_egresos.','.$item_remito->remitos_egresos->nro_remito;
                                            $aux_detalle->save();
                                        }
                                        else
                                        {
                                            //buscar precio unitario
                                            $precio_unitario = Precios_eloquent::where('id_contrato',$this->input->post('id_contrato'))->where('id_producto',$producto->id)->get();
                                            $cont_pre = 0;
                                            foreach ($precio_unitario as $pre_unit) 
                                            {
                                               $cont_pre = $cont_pre + 1;
                                               $precio = $pre_unit;
                                            }
                                            if($cont_pre > 0)
                                            {
                                                $items_detalle_factura = new Items_detalle_factura_eloquent();
                                                $items_detalle_factura->id_lote = $item_remito->id_lote;
                                                $items_detalle_factura->sub_total = ($item_remito->cantidad * $precio->precio);
                                                $items_detalle_factura->precio_unitario = $precio->precio;
                                                $items_detalle_factura->id_factura = $factura->id;
                                                $items_detalle_factura->remitos_egresos = $item_remito->remitos_egresos->nro_remito;
                                                $items_detalle_factura->id_producto = $producto->id;
                                                $items_detalle_factura->cantidad = $item_remito->cantidad;
                                                $items_detalle_factura->estado = 1;
                                                $items_detalle_factura->save();

                                            }
                                                
                                        }
                                          
                                    }

                                  }
                                }
                              }
                              if($acum > 0)
                                    {
                                        //buscar precio unitario
                                        $precio_unitario = Precios_eloquent::where('id_contrato',$this->input->post('id_contrato'))->where('id_producto',$producto->id)->get();
                                        $cont_pre = 0;
                                        foreach ($precio_unitario as $pre_unit) 
                                        {
                                           $cont_pre = $cont_pre + 1;
                                           $precio = $pre_unit;
                                        }
                                        if($cont_pre > 0)
                                        {
                                            $items_factura = new Items_factura_eloquent();
                                            $items_factura->id_factura = $factura->id;
                                            $items_factura->id_producto = $producto->id;
                                            $items_factura->cantidad = $acum;
                                            $items_factura->precio_unitario = $precio->precio;
                                            $items_factura->sub_total = ($precio->precio * $acum);
                                            $items_factura->estado = 1;
                                            $items_factura->save();

                                        }
                                    }
                              $acum =0 ;
        }
        foreach ($this->cart->contents() as $item)
        {
                                if($item['name'] == 'factura')
                                {
                                  $dato = $item['options'];
                                  $remito_facturado = new Remitos_facturados_eloquent();
                                  $remito_facturado->id_factura = $factura->id;
                                  $remito_facturado->id_remito = $dato['id_remito'];
                                  $remito_facturado->estado = 1;
                                  $remito_facturado->save();
                                  
                                }
        }
        //CREAR LOS DETALLES POR DESTINOS, PACIENTES O SERVICIOS
        //primero pacientes
        $pacientes = Pacientes_eloquent::all();
        foreach ($pacientes as $paciente)
        {
            foreach ($productos as $producto)
            {
                foreach ($this->cart->contents() as $item)
                            {

                                if($item['name'] == 'factura')
                                {
                                  $dato = $item['options'];
                                  $remito = Remitos_egresos_eloquent::find($dato['id_remito']);
                                  if($remito->id_paciente == $paciente->id)
                                  {
                                    foreach ($remito->items_remito_egreso as $item_remito) 
                                      {
                                        if($item_remito->id_producto == $producto->id)
                                        {
                                          $acum = $acum + $item_remito->cantidad;
                                         
                                          //buscar si ya existe el items detalle factura, si existe si incrementa la cantidad
                                          $aux_items_detalle_destino = Items_detalle_factura_destino_eloquent::where('id_factura',$factura->id)->where('id_producto',$producto->id)->where('id_lote',$item_remito->id_lote)->where('id_paciente',$paciente->id)->get();
                                          $cont_items_detalle = 0;
                                          foreach ($aux_items_detalle_destino as $aux_item_detalle_destino) 
                                          {
                                            $cont_items_detalle =$cont_items_detalle + 1;
                                            $aux_detalle_destino = $aux_item_detalle_destino;
                                          }
                                          if($cont_items_detalle == 1)
                                            {   
                                                $cantidad_aux = $aux_detalle_destino->cantidad;
                                                $sub_total_aux = $aux_detalle_destino->sub_total;

                                                $aux_detalle_destino->cantidad = $cantidad_aux + $item_remito->cantidad;
                                                $aux_detalle_destino->sub_total = $sub_total_aux + ($item_remito->cantidad * $aux_detalle_destino->precio_unitario);
                                                $aux_detalle_destino->remitos_egresos = $aux_detalle_destino->remitos_egresos.','.$item_remito->remitos_egresos->nro_remito;
                                                $aux_detalle_destino->save();
                                            }
                                            else
                                            {
                                                //buscar precio unitario
                                                $precio_unitario = Precios_eloquent::where('id_contrato',$this->input->post('id_contrato'))->where('id_producto',$producto->id)->get();
                                                $cont_pre = 0;
                                                foreach ($precio_unitario as $pre_unit) 
                                                {
                                                   $cont_pre = $cont_pre + 1;
                                                   $precio = $pre_unit;
                                                }
                                                if($cont_pre > 0)
                                                {
                                                    $items_detalle_factura_destino = new Items_detalle_factura_destino_eloquent();
                                                    $items_detalle_factura_destino->id_servicio = 0;
                                                    $items_detalle_factura_destino->id_paciente = 0;
                                                    $items_detalle_factura_destino->id_lote = $item_remito->id_lote;
                                                    $items_detalle_factura_destino->sub_total = ($item_remito->cantidad * $precio->precio);
                                                    $items_detalle_factura_destino->precio_unitario = $precio->precio;
                                                    $items_detalle_factura_destino->id_factura = $factura->id;
                                                    $items_detalle_factura_destino->remitos_egresos = $item_remito->remitos_egresos->nro_remito;
                                                    $items_detalle_factura_destino->id_producto = $producto->id;
                                                    $items_detalle_factura_destino->cantidad = $item_remito->cantidad;
                                                    $items_detalle_factura_destino->estado = 1;
                                                    $items_detalle_factura_destino->tipo_destino ='paciente';
                                                    $items_detalle_factura_destino->id_paciente = $paciente->id;
                                                    $items_detalle_factura_destino->save();

                                                }
                                                    
                                            }
                                              
                                        }
                                      }//end for
                                  }//end if item[name]==factura
                                      

                                }//end if
                            }//end for caart

            }   //end for productos
                
        }//end for pacientes
        //ahora servicios
        $servicios = Servicios_destinos_eloquent::all();
        foreach ($servicios as $servicio)
        {
            foreach ($productos as $producto)
            {
                foreach ($this->cart->contents() as $item)
                            {

                                if($item['name'] == 'factura')
                                {
                                  $dato = $item['options'];
                                  $remito = Remitos_egresos_eloquent::find($dato['id_remito']);
                                  if($remito->id_servicio == $servicio->id)
                                  {
                                    foreach ($remito->items_remito_egreso as $item_remito) 
                                      {
                                        if($item_remito->id_producto == $producto->id)
                                        {
                                          $acum = $acum + $item_remito->cantidad;
                                         
                                          //buscar si ya existe el items detalle factura, si existe si incrementa la cantidad
                                          $aux_items_detalle_destino = Items_detalle_factura_destino_eloquent::where('id_factura',$factura->id)->where('id_producto',$producto->id)->where('id_lote',$item_remito->id_lote)->where('id_servicio',$servicio->id)->get();
                                          $cont_items_detalle = 0;
                                          foreach ($aux_items_detalle_destino as $aux_item_detalle_destino) 
                                          {
                                            $cont_items_detalle =$cont_items_detalle + 1;
                                            $aux_detalle_destino = $aux_item_detalle_destino;
                                          }
                                          if($cont_items_detalle == 1)
                                            {   
                                                $cantidad_aux = $aux_detalle_destino->cantidad;
                                                $sub_total_aux = $aux_detalle_destino->sub_total;

                                                $aux_detalle_destino->cantidad = $cantidad_aux + $item_remito->cantidad;
                                                $aux_detalle_destino->sub_total = $sub_total_aux + ($item_remito->cantidad * $aux_detalle_destino->precio_unitario);
                                                $aux_detalle_destino->remitos_egresos = $aux_detalle_destino->remitos_egresos.','.$item_remito->remitos_egresos->nro_remito;
                                                $aux_detalle_destino->save();
                                            }
                                            else
                                            {
                                                //buscar precio unitario
                                                $precio_unitario = Precios_eloquent::where('id_contrato',$this->input->post('id_contrato'))->where('id_producto',$producto->id)->get();
                                                $cont_pre = 0;
                                                foreach ($precio_unitario as $pre_unit) 
                                                {
                                                   $cont_pre = $cont_pre + 1;
                                                   $precio = $pre_unit;
                                                }
                                                if($cont_pre > 0)
                                                {
                                                    $items_detalle_factura_destino = new Items_detalle_factura_destino_eloquent();
                                                    $items_detalle_factura_destino->id_servicio = 0;
                                                    $items_detalle_factura_destino->id_paciente = 0;
                                                    $items_detalle_factura_destino->id_lote = $item_remito->id_lote;
                                                    $items_detalle_factura_destino->sub_total = ($item_remito->cantidad * $precio->precio);
                                                    $items_detalle_factura_destino->precio_unitario = $precio->precio;
                                                    $items_detalle_factura_destino->id_factura = $factura->id;
                                                    $items_detalle_factura_destino->remitos_egresos = $item_remito->remitos_egresos->nro_remito;
                                                    $items_detalle_factura_destino->id_producto = $producto->id;
                                                    $items_detalle_factura_destino->cantidad = $item_remito->cantidad;
                                                    $items_detalle_factura_destino->estado = 1;
                                                    $items_detalle_factura_destino->tipo_destino ='servicio';
                                                    $items_detalle_factura_destino->id_servicio = $servicio->id;
                                                    $items_detalle_factura_destino->save();

                                                }
                                                    
                                            }
                                              
                                        }
                                      }//end for
                                  }//end if item[name]==factura
                                      

                                }//end if
                            }//end for caart

            }   //end for productos
                
        }//end for pacientes
        //
        $this->cart->destroy();
        $factura->save();  
        $acum_total = 0;
        foreach ($factura->items_factura as $item_f) {
            $acum_total = $acum_total + $item_f->sub_total;
            # code...
        }
        
        $factura->monto_total = $acum_total;
        $factura->save();      
        echo json_encode(array(
            "status" => TRUE,
            
            )); 

    }
    public function detalles_completos()
    {
        $id = $this->input->get('id');
        //echo $id;
        $data['factura'] = Facturas_eloquent::find($id);
        $this->load->view('facturas/detalles_completos', $data);
             
    }
    public function updateVisibilidad()
    {
        $factura = Facturas_eloquent::find($this->input->post('id'));
        $factura->visible = $this->input->post('visible');
        $factura->save();

        echo json_encode(array(
            "status" => $factura->save(),
            ));  
    }
    public function detalles_generales()
    {
        $id = $this->input->get('id');
        //echo $id;
        $data['factura'] = Facturas_eloquent::find($id);
        $this->load->view('facturas/detalles_generales', $data);
             
    }
   
}
    