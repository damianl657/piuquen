<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Container\Container as Container;
use \Illuminate\Support\Facades\Facade as Facade;



class Contratos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Groups_eloquent');
		$this->load->model('Users_groups_eloquent');
		$this->load->model('Users_eloquent');
        $this->load->model('Productos_eloquent');
        $this->load->model('Contratos_eloquent');
        $this->load->model('Contrato_actual_eloquent');
        $this->load->model('Instituciones_eloquent');
        
		//$this->load->library(array('ion_auth','funciones'));
		
	}
	
	public function index()
	{
		$aux['controlador'] = 'Contratos';
		$aux['metodo'] = 'index';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        	//$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
        	$data['contenido'] = "/parametros/contratos/index";
			$this->load->view('templates/templateCodebase', $data);
        }
        else
        {
        	echo "error de permiso";
            die();
        }
	}
	public function listar_contratos()
    {
        $data['listado'] = Contratos_eloquent::activos()                                           
                                            ->get();
                                        
        $this->load->view('/parametros/contratos/listar_contratos', $data);
        //echo json_encode(array("status" => TRUE, "clientes" => $this->load->view('clientes/listar_clientes', $data)));
    }
    public function nuevo()
	{
		$aux['controlador'] = 'Contratos';
		$aux['metodo'] = 'nuevo';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        		
			$data['instituciones'] = Instituciones_eloquent::activos()                                           
                                            ->get();
			$data['contenido'] = "/parametros/contratos/nuevo";
            
			//print_r($data['bancos']);
			//die();
			$this->load->view('templates/templateCodebase', $data);
        }
        else
        {
        	echo"error permiso";
        	die();
        }
	}
	public function guardar()
	{
		//echo "ok";
		$contrato = new Contratos_eloquent();
         $fecha = $this->input->post('fecha');
                    //Procesamiento de fecha y hora
                    if($fecha != '')
                    {

                      $fecha = str_replace('/', '-', $fecha);
                      $separa = explode("-",$fecha);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha = $anio.'-'.$mes.'-'.$dia;
                    }
        $contrato->fecha = $fecha;
        $contrato->resolucion = $this->input->post('resolucion');
        $contrato->expediente = $this->input->post('expediente');
        $contrato->id_institucion = $this->input->post('id_institucion');
        
        $contrato->usaProducto = 0;
        $contrato->usaAlias1 = 0;
        $contrato->usaAlias2 = 0;
        $contrato->usaAlias3 = 0;
        if($this->input->post('tipoProdAlias')==1)
            $contrato->usaProducto = 1;
        if($this->input->post('tipoProdAlias')==2)
            $contrato->usaAlias1 = 1;
        if($this->input->post('tipoProdAlias')==3)
            $contrato->usaAlias2 = 1;
        if($this->input->post('tipoProdAlias')==4)
            $contrato->usaAlias3 = 1;
        //$producto->descripcion = $this->input->post('descripcion');
        //$producto->id_tipo_prod = $this->input->post('id_tipo');
        
        $contrato->estado = '1';
        $contrato->save();

        echo json_encode(array(
            "status" => $contrato->save(),
            
            ));        
	}
	public function detalle()
    {
        $id = $this->input->get('id');
        //echo $id;
        $contrato = Contratos_eloquent::find($id);
        
             echo json_encode(array("status" => TRUE, 
            "contrato" => $contrato, 
           
            ));
    }
    public function modificar($id)
    {
        $aux['controlador'] = 'Contratos';
        $aux['metodo'] = 'modificar';
        /*$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        //print_r($permisos);
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        //print_r($obtener_permiso);
        if($obtener_permiso)
        {*/
           // echo "vista en produccion";
        $data['instituciones'] = Instituciones_eloquent::activos()                                           
           ->get();
        $data['contenido'] = "/parametros/contratos/editar";
        $data['contrato'] = Contratos_eloquent::find($id);
            
            
            //print_r($cliente->users_groups->users->documento);
           // die();
            $this->load->view('templates/templateCodebase', $data);
       // }
       
    }
    public function update()
    {
    	$contrato = Contratos_eloquent::find($this->input->post('id'));
    	 $fecha = $this->input->post('fecha');
                    //Procesamiento de fecha y hora
                    if($fecha != '')
                    {

                      $fecha = str_replace('/', '-', $fecha);
                      $separa = explode("-",$fecha);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha = $anio.'-'.$mes.'-'.$dia;
                    }
        $contrato->fecha = $fecha;
        $contrato->resolucion = $this->input->post('resolucion');
        $contrato->expediente = $this->input->post('expediente');
        $contrato->id_institucion = $this->input->post('id_institucion');
        
        $contrato->usaProducto = 0;
        $contrato->usaAlias1 = 0;
        $contrato->usaAlias2 = 0;
        $contrato->usaAlias3 = 0;
        if($this->input->post('tipoProdAlias')==1)
            $contrato->usaProducto = 1;
        if($this->input->post('tipoProdAlias')==2)
            $contrato->usaAlias1 = 1;
        if($this->input->post('tipoProdAlias')==3)
            $contrato->usaAlias2 = 1;
        if($this->input->post('tipoProdAlias')==4)
            $contrato->usaAlias3 = 1;
        //$producto->descripcion = $this->input->post('descripcion');
       // $producto->id_tipo_prod = $this->input->post('id_tipo');
        //$tipo->estado = '1';
        $contrato->save();

        echo json_encode(array(
            "status" => $contrato->save(),
            ));  
    }  
    public function baja()
    {
        //$cliente = Clientes_eloquent::find($id);
        $contrato = Contratos_eloquent::find($this->input->post('id_contrato'));
        $contrato->estado = '0';
        $contrato->save();
        echo json_encode(array("status" => true));
        //echo "hola mundo";
    } 
    public function asignacion_contrato()
    {
        $data['contratos'] = Contratos_eloquent::activos()                                           
                                            ->get();
        $actual = Contrato_actual_eloquent::find('1');
        $data['actual'] = Contratos_eloquent::find($actual->id_contrato);
        $data['contenido'] = "/parametros/contratos/asignacion_contrato";                                
        
        $this->load->view('templates/templateCodebase', $data);
    }
    public function asignar_contrato_actual()
    {
        $actual = Contrato_actual_eloquent::find('1');
        $actual->id_contrato = $this->input->post('id_contrato');
        $actual->save();
        echo json_encode(array(
            "status" => $actual->save(),
            ));  

    }

}