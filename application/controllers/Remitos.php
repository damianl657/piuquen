<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Container\Container as Container;
use \Illuminate\Support\Facades\Facade as Facade;

set_time_limit(9000);
ini_set('MAX_EXECUTION_TIME', 9000);

class Remitos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Groups_eloquent');
		$this->load->model('Users_groups_eloquent');
		$this->load->model('Users_eloquent');
        $this->load->model('Proveedores_eloquent');
        $this->load->model('Lotes_eloquent');
        $this->load->model('Items_remito_ingreso_eloquent');
        $this->load->model('Pedidos_eloquent');
        $this->load->model('Remitos_ingresos_eloquent');
        $this->load->model('Ingresos_eloquent');
        $this->load->model('Productos_eloquent');
		//$this->load->library(array('ion_auth','funciones'));
		
	}
	
	public function index($id_ingreso)
	{
		$aux['controlador'] = 'Remitos';
		$aux['metodo'] = 'index';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {

        	//$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
        	$data['contenido'] = "remitos/index";
            $data['ingreso'] = Ingresos_eloquent::find($id_ingreso);

			$this->load->view('templates/templateCodebase', $data);
        }
        else
        {
        	echo "error de permiso";
            die();
        }
	}
	public function listar_remitos()
    {
         $id_ingreso = $this->input->get('id_ingreso');
        $data['listado'] = Remitos_ingresos_eloquent::where('id_ingreso',$id_ingreso)->get();
          //print_r($id_ingreso);                              
        $this->load->view('remitos/listar_remitos', $data);
        //echo json_encode(array("status" => TRUE, "clientes" => $this->load->view('clientes/listar_clientes', $data)));
    }
    public function nuevo($id_ingreso)
	{
		$aux['controlador'] = 'Ingresos';
		$aux['metodo'] = 'nuevo';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        	$this->cart->destroy();    	
			$data['ingreso'] = Ingresos_eloquent::find($id_ingreso);
            $data['productos'] = Productos_eloquent::activos()->get();
			$data['contenido'] = "remitos/nuevo";
            
			//print_r($data['bancos']);
			//die();
			$this->load->view('templates/templateCodebase', $data);
        }
        else
        {
        	echo"error permiso";
        	die();
        }
	}
	public function registar_remito()
	{
		//echo "ok";
		$remito = new Remitos_ingresos_eloquent();
        $fecha = $this->input->post('fecha');
                    //Procesamiento de fecha y hora
                    if($fecha != '')
                    {

                      $fecha = str_replace('/', '-', $fecha);
                      $separa = explode("-",$fecha);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha = $anio.'-'.$mes.'-'.$dia;
                    }
        $remito->fecha = $fecha;
        $remito->remito = $this->input->post('remito');
        
        $remito->id_proveedor = $this->input->post('id_proveedor');
        $remito->id_ingreso = $this->input->post('id_ingreso');
        $remito->id_user = $this->session->userdata('user_id');

        //$producto->id_tipo_prod = $this->input->post('id_tipo');
        
        $remito->estado = '1';
        $remito->save();

        echo json_encode(array(
            "status" => TRUE,
            "id_remito" => $remito->id,
            ));        
	}
	public function detalle()
    {
        $id = $this->input->get('id');
        //echo $id;
        $data['remito'] = Remitos_ingresos_eloquent::find($id);
        $this->load->view('remitos/detalle_remito', $data);
             
    }
    public function modificar($id)
    {
        $aux['controlador'] = 'Ingresos';
        $aux['metodo'] = 'modificar';
        /*$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        //print_r($permisos);
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        //print_r($obtener_permiso);
        if($obtener_permiso)
        {*/
           // echo "vista en produccion";
        $this->cart->destroy();    
        $data['contenido'] = "remitos/editar";
        $data['remito'] = Remitos_ingresos_eloquent::find($id);
        $data['productos'] = Productos_eloquent::activos()->get();    
            
            //print_r($cliente->users_groups->users->documento);
           // die();
            $this->load->view('templates/template_admin', $data);
       // }
       
    }
    public function update()
    {
    	$remito = Remitos_ingresos_eloquent::find($this->input->post('id'));
    	$fecha = $this->input->post('fecha');
                    //Procesamiento de fecha y hora
                    if($fecha != '')
                    {

                      $fecha = str_replace('/', '-', $fecha);
                      $separa = explode("-",$fecha);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha = $anio.'-'.$mes.'-'.$dia;
                    }
        $remito->fecha = $fecha;
        $remito->remito = $this->input->post('remito');
       // $producto->id_tipo_prod = $this->input->post('id_tipo');
        //$tipo->estado = '1';
        $remito->save();

        echo json_encode(array(
            "status" => $remito->save(),
            ));  
    }
    public function registrar_lote()
    {

        $aux_lotes = Lotes_eloquent::where('lote',$this->input->post('lote'))->where('id_producto',$this->input->post('id_producto'))->get();
        $cont = 0;
        foreach ($aux_lotes as $aux_lote)
        {
            $cont = 1;
            $lote_encontrado = $aux_lote;
        }
        if($cont == 1)//lote encontrado, incrementar stock
        {
            $aux_lote->cantidad = $aux_lote->cantidad + $this->input->post('cantidad');
            $aux_lote->estado = 1;
            $aux_lote->save();

            $items_remito = new Items_remito_ingreso_eloquent();
            $items_remito->id_lote = $aux_lote->id;
            $items_remito->id_remito = $this->input->post('id_remito');
            $items_remito->cantidad = $this->input->post('cantidad');
            $items_remito->estado = 1;
            $items_remito->save();
        }
        else//
        {
            $fecha_vencimiento = $this->input->post('fecha_vencimiento');
                    //Procesamiento de fecha y hora
                    if($fecha_vencimiento != '')
                    {

                      $fecha_vencimiento = str_replace('/', '-', $fecha_vencimiento);
                      $separa = explode("-",$fecha_vencimiento);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha_vencimiento = $anio.'-'.$mes.'-'.$dia;
                    }
            
            $lote = new Lotes_eloquent();
            $lote->cantidad = $this->input->post('cantidad');
            $lote->lote = $this->input->post('lote');
            $lote->id_producto = $this->input->post('id_producto');
            if($fecha_vencimiento == null)
            {
                $fecha_vencimiento = '0000-00-00';
            }
            $lote->fecha_vencimiento = $fecha_vencimiento;
            $lote->estado = 1;
            $lote->save();

            $items_remito = new Items_remito_ingreso_eloquent();
            $items_remito->id_lote = $lote->id;
            $items_remito->id_remito = $this->input->post('id_remito');
            $items_remito->cantidad = $this->input->post('cantidad');
            $items_remito->estado = 1;
            $items_remito->save();
        }

            $cont = 1;
            foreach ( $this->cart->contents() as $item) 
            {
                $cont = $cont + 1;  
            }
            
            /*if($this->input->post('precio_txt') == 0)
            {
                $precio = 1;
            }*/
            
            $data = array(
            'id'      => $cont,
            'qty'     => 1,
            'price'   => 1,
            'name'    => 'lote',
            'options' => array(
                'id_producto' => $this->input->post('id_producto'), 
                'cantidad' => $this->input->post('cantidad'),
                'lote' => $this->input->post('lote'),
                'fecha_vencimiento'  => $this->input->post('fecha_vencimiento'),
                ));
            $this->cart->insert($data);
            $this->load->view('remitos/carrito_lotes_registrados');
        

    }
   

}