<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Container\Container as Container;
use \Illuminate\Support\Facades\Facade as Facade;

class Reportes_excel extends CI_Controller {
     
     
     
     
     
    /**
     * 
     * __construct
     */
    public function __construct () {
        parent::__construct();
         
        // inicializamos la librería
        //$this->load->library('Classes/PHPExcel.php');

    }
    // end: construc
     
     
     
     
     
    /**
     * 
     * setExcel
     */
    public function index () {
         


        // configuramos las propiedades del documento
        $this->phpexcel->getProperties()->setCreator("Arkos Noem Arenom")
                                     ->setLastModifiedBy("Arkos Noem Arenom")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");
         
         
        // agregamos información a las celdas
        $this->phpexcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Hello')
                    ->setCellValue('B2', 'world!')
                    ->setCellValue('C1', 'Hello')
                    ->setCellValue('D2', 'world!');
         
        // La librería puede manejar la codificación de caracteres UTF-8
        $this->phpexcel->setActiveSheetIndex(0)
                    ->setCellValue('A4', 'Miscellaneous glyphs')
                    ->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');
         
        // Renombramos la hoja de trabajo
        $this->phpexcel->getActiveSheet()->setTitle('Simple');
         
         
        // configuramos el documento para que la hoja
        // de trabajo número 0 sera la primera en mostrarse
        // al abrir el documento
        $this->phpexcel->setActiveSheetIndex(0);
         
         
        // redireccionamos la salida al navegador del cliente (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="01simple2.xlsx"');
        header('Cache-Control: max-age=0');
         
        $objWriter = PHPExcel_IOFactory::createWriter($this->phpexcel, 'Excel2007');
        $objWriter->save('php://output');
         
    }
    // end: setExcel
    public function exportar_excel_detalles_completos($id)
    {
        $this->load->model('Productos_eloquent');
       
        $this->load->model('Lotes_eloquent');
        $this->load->model('Items_remito_egreso_eloquent');
        $this->load->model('Precios_eloquent');
        $this->load->model('Facturas_eloquent');
        $this->load->model('Remitos_facturados_eloquent');
        $this->load->model('Items_factura_eloquent');
        $this->load->model('Items_detalle_factura_eloquent');
        $factura = Facturas_eloquent::find($id);
        //date_default_timezone_set('America/Mexico_City');

       /* if (PHP_SAPI == 'cli')
            die('Este archivo solo se puede ver desde un navegador web');*/

        /** Se agrega la libreria PHPExcel */
        //require_once 'lib/PHPExcel/PHPExcel.php';

        // Se crea el objeto PHPExcel
        $objPHPExcel = new PHPExcel();

        // Se asignan las propiedades del libro
        $objPHPExcel->getProperties()->setCreator("Lic. Damian Leal") //Autor
                             ->setLastModifiedBy("Lic. Damian Leal") //Ultimo usuario que lo modificó
                             ->setTitle("Detalle Remito General")
                             ->setSubject("Remito General")
                             ->setDescription("Remito ")
                             ->setKeywords("Detalle Reporte")
                             ->setCategory("Reporte excel");

        $tituloReporte = "Detalle Remito General";
        $titulosColumnas = array('#','Producto', 'Lote','Vencimiento','Remitos');
        
        $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:D1');
                        
        // Se agregan los titulos del reporte
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1',$tituloReporte)
                    ->setCellValue('A3',  $titulosColumnas[0])
                    ->setCellValue('B3',  $titulosColumnas[1])
                    ->setCellValue('C3',  $titulosColumnas[2])
                    ->setCellValue('D3',  $titulosColumnas[3])
                    ->setCellValue('E3',  $titulosColumnas[4]);
                   

        //recorrer facturacion
        $i = 4;
        $cont = 1;
        foreach($factura->items_detalle_factura as $fila):
            if($fila->lotes->productos->con_vencimiento == 1)
            {
                $venicimiento = substr($fila->lotes->fecha_vencimiento,5,2).'/'.substr($fila->lotes->fecha_vencimiento,0,4);
            }
            else
            {
                $venicimiento = 'Sin Vto';
            }
            
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i,  $cont)
                    ->setCellValue('B'.$i,  $fila->productos->nombre)
                    ->setCellValue('C'.$i, $fila->lotes->lote)
                    ->setCellValue('D'.$i, $venicimiento)
                    
                    ->setCellValue('E'.$i, $fila->remitos_egresos);
                    $i++;
                    $cont++;
        endforeach;
       /* $estiloTituloReporte = array(
            'font' => array(
                'name'      => 'Verdana',
                'bold'      => true,
                'italic'    => false,
                'strike'    => false,
                'size' =>16,
                    'color'     => array(
                        'rgb' => 'FFFFFF'
                    )
            ),
            'fill' => array(
                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => 'FF220835')
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_NONE                    
                )
            ), 
            'alignment' =>  array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'rotation'   => 0,
                    'wrap'          => TRUE
            )
        );

        $estiloTituloColumnas = array(
            'font' => array(
                'name'      => 'Arial',
                'bold'      => true,                          
                'color'     => array(
                    'rgb' => 'FFFFFF'
                )
            ),
            'fill'  => array(
                'type'      => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                'rotation'   => 90,
                'startcolor' => array(
                    'rgb' => 'c47cf2'
                ),
                'endcolor'   => array(
                    'argb' => 'FF431a5d'
                )
            ),
            'borders' => array(
                'top'     => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                    'color' => array(
                        'rgb' => '143860'
                    )
                ),
                'bottom'     => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                    'color' => array(
                        'rgb' => '143860'
                    )
                )
            ),
            'alignment' =>  array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap'          => TRUE
            ));
            
        $estiloInformacion = new PHPExcel_Style();
        $estiloInformacion->applyFromArray(
            array(
                'font' => array(
                'name'      => 'Arial',               
                'color'     => array(
                    'rgb' => '000000'
                )
            ),
            'fill'  => array(
                'type'      => PHPExcel_Style_Fill::FILL_SOLID,
                'color'     => array('argb' => 'FFd9b7f4')
            ),
            'borders' => array(
                'left'     => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN ,
                    'color' => array(
                        'rgb' => '3a2a47'
                    )
                )             
            )
        ));
         
        $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($estiloTituloReporte);
        $objPHPExcel->getActiveSheet()->getStyle('A3:D3')->applyFromArray($estiloTituloColumnas);       
        $objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:D".($i-1));*/
                
        for($i = 'A'; $i <= 'D'; $i++){
            $objPHPExcel->setActiveSheetIndex(0)            
                ->getColumnDimension($i)->setAutoSize(TRUE);
        }
        
        // Se asigna el nombre a la hoja
        $objPHPExcel->getActiveSheet()->setTitle('Detalle General');

        // Se activa la hoja para que sea la que se muestre cuando el archivo se abre
        $objPHPExcel->setActiveSheetIndex(0);
        // Inmovilizar paneles 
        //$objPHPExcel->getActiveSheet(0)->freezePane('A4');
        $objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,4);

        // Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
        /*header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="ReporteGeneral.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');*/
       // $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
       // $objWriter->save(str_replace('.php', '.xlsx', __FILE__));

////////////////////////////////////////////////
         $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel); //Escribir archivo
         
        // Establecer formado de Excel 2003
        header("Content-Type: application/vnd.ms-excel");
         
        // nombre del archivo
        header('Content-Disposition: attachment; filename="Detalle Remito.xls"');
        $objWriter->save('php://output');
        //exit;
        redirect('Facturas/listar_facturas');

    }
    public function exportar_excel_general($id)
    {
        $this->load->model('Productos_eloquent');
       
        $this->load->model('Lotes_eloquent');
        $this->load->model('Items_remito_egreso_eloquent');
        $this->load->model('Precios_eloquent');
        $this->load->model('Facturas_eloquent');
        $this->load->model('Remitos_facturados_eloquent');
        $this->load->model('Items_factura_eloquent');
        $this->load->model('Items_detalle_factura_eloquent');
        $factura = Facturas_eloquent::find($id);
        //date_default_timezone_set('America/Mexico_City');

       /* if (PHP_SAPI == 'cli')
            die('Este archivo solo se puede ver desde un navegador web');*/

        /** Se agrega la libreria PHPExcel */
        //require_once 'lib/PHPExcel/PHPExcel.php';

        // Se crea el objeto PHPExcel
        $objPHPExcel = new PHPExcel();

        // Se asignan las propiedades del libro
        $objPHPExcel->getProperties()->setCreator("Lic. Damian Leal") //Autor
                             ->setLastModifiedBy("Lic. Damian Leal") //Ultimo usuario que lo modificó
                             ->setTitle("Detalle  General")
                             ->setSubject(" General")
                             ->setDescription("Remito ")
                             ->setKeywords("General")
                             ->setCategory("Reporte excel");

        $tituloReporte = "Remito General";
        $titulosColumnas = array('#','Producto', 'Cantidad', 'Precio Unitario','Sub Total');
        
        $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:D1');
                        
        // Se agregan los titulos del reporte
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1',$tituloReporte)
                    ->setCellValue('A3',  $titulosColumnas[0])
                    ->setCellValue('B3',  $titulosColumnas[1])
                    ->setCellValue('C3',  $titulosColumnas[2])
                    ->setCellValue('D3',  $titulosColumnas[3])
                    ->setCellValue('E3',  $titulosColumnas[4]);
                   

        //recorrer facturacion
        $i = 4;
        $cont = 1;
        foreach($factura->items_factura as $fila):
            
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i,  $cont)
                    ->setCellValue('B'.$i,  $fila->productos->nombre)
                    ->setCellValue('C'.$i,  $fila->cantidad)
                    ->setCellValue('D'.$i, $fila->precio_unitario)
                    ->setCellValue('E'.$i, $fila->sub_total);
                    $i++;
                    $cont++;
        endforeach;
        /*$estiloTituloReporte = array(
            'font' => array(
                'name'      => 'Verdana',
                'bold'      => true,
                'italic'    => false,
                'strike'    => false,
                'size' =>16,
                    'color'     => array(
                        'rgb' => 'FFFFFF'
                    )
            ),
            'fill' => array(
                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('argb' => 'FF220835')
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_NONE                    
                )
            ), 
            'alignment' =>  array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'rotation'   => 0,
                    'wrap'          => TRUE
            )
        );

        $estiloTituloColumnas = array(
            'font' => array(
                'name'      => 'Arial',
                'bold'      => true,                          
                'color'     => array(
                    'rgb' => 'FFFFFF'
                )
            ),
            'fill'  => array(
                'type'      => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
                'rotation'   => 90,
                'startcolor' => array(
                    'rgb' => 'c47cf2'
                ),
                'endcolor'   => array(
                    'argb' => 'FF431a5d'
                )
            ),
            'borders' => array(
                'top'     => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                    'color' => array(
                        'rgb' => '143860'
                    )
                ),
                'bottom'     => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
                    'color' => array(
                        'rgb' => '143860'
                    )
                )
            ),
            'alignment' =>  array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap'          => TRUE
            ));
            
        $estiloInformacion = new PHPExcel_Style();
        $estiloInformacion->applyFromArray(
            array(
                'font' => array(
                'name'      => 'Arial',               
                'color'     => array(
                    'rgb' => '000000'
                )
            ),
            'fill'  => array(
                'type'      => PHPExcel_Style_Fill::FILL_SOLID,
                'color'     => array('argb' => 'FFd9b7f4')
            ),
            'borders' => array(
                'left'     => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN ,
                    'color' => array(
                        'rgb' => '3a2a47'
                    )
                )             
            )
        ));
         
        $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($estiloTituloReporte);
        $objPHPExcel->getActiveSheet()->getStyle('A3:D3')->applyFromArray($estiloTituloColumnas);       
        $objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:D".($i-1));*/
                
        for($i = 'A'; $i <= 'D'; $i++){
            $objPHPExcel->setActiveSheetIndex(0)            
                ->getColumnDimension($i)->setAutoSize(TRUE);
        }
        
        // Se asigna el nombre a la hoja
        $objPHPExcel->getActiveSheet()->setTitle('Detalle General');

        // Se activa la hoja para que sea la que se muestre cuando el archivo se abre
        $objPHPExcel->setActiveSheetIndex(0);
        // Inmovilizar paneles 
        //$objPHPExcel->getActiveSheet(0)->freezePane('A4');
        $objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,4);

        // Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
        /*header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="ReporteGeneral.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');*/
       // $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
       // $objWriter->save(str_replace('.php', '.xlsx', __FILE__));

////////////////////////////////////////////////
         $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel); //Escribir archivo
         
        // Establecer formado de Excel 2003
        header("Content-Type: application/vnd.ms-excel");
         
        // nombre del archivo
        header('Content-Disposition: attachment; filename="Detalle General.xls"');
        $objWriter->save('php://output');
        //exit;
        redirect('Facturas/listar_facturas');

    }
    public function exportar_excel_detalles_completos_destinos($id)
    {
        $this->load->model('Productos_eloquent');
       
        $this->load->model('Lotes_eloquent');
        $this->load->model('Items_remito_egreso_eloquent');
        $this->load->model('Precios_eloquent');
        $this->load->model('Facturas_eloquent');
        $this->load->model('Remitos_facturados_eloquent');
        $this->load->model('Items_factura_eloquent');
        $this->load->model('Items_detalle_factura_eloquent');
        $this->load->model('Items_detalle_factura_destino_eloquent');
        $this->load->model('Pacientes_eloquent');
        $this->load->model('Servicios_destinos_eloquent');
        $this->load->model('Contratos_eloquent');
        $this->load->model('Contrato_actual_eloquent');
        
        $factura = Facturas_eloquent::find($id);
        $contrato_actual = Contrato_actual_eloquent::find('1');
        $contrato_actual = Contratos_eloquent::find($contrato_actual->id_contrato);
        //date_default_timezone_set('America/Mexico_City');

       /* if (PHP_SAPI == 'cli')
            die('Este archivo solo se puede ver desde un navegador web');*/

        /** Se agrega la libreria PHPExcel */
        //require_once 'lib/PHPExcel/PHPExcel.php';

        // Se crea el objeto PHPExcel
        $objPHPExcel = new PHPExcel();

        // Se asignan las propiedades del libro
        $objPHPExcel->getProperties()->setCreator("Lic. Damian Leal") //Autor
                             ->setLastModifiedBy("Lic. Damian Leal") //Ultimo usuario que lo modificó
                             ->setTitle("Detalle Remito General")
                             ->setSubject("Remito General")
                             ->setDescription("Remito ")
                             ->setKeywords("Detalle Reporte")
                             ->setCategory("Reporte excel");
                             //print_r($contrato_actual);
       // die();
        $tituloReporte = "NOTA ACLARACIÓN REMITO GENERAL. Ref. EXPTE ".$contrato_actual->expediente;
        $titulosColumnas = array('Cant','Producto', 'Lote','Vencimiento0','Paciente','Remitos');
        
        $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:D1');
                        
        // Se agregan los titulos del reporte
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1',$tituloReporte)
                    ->setCellValue('A3',  $titulosColumnas[0])
                    ->setCellValue('B3',  $titulosColumnas[1])
                    ->setCellValue('C3',  $titulosColumnas[2])
                    ->setCellValue('D3',  $titulosColumnas[3])
                    ->setCellValue('E3',  $titulosColumnas[4])
                    ->setCellValue('F3',  $titulosColumnas[5]);

        //recorrer facturacion
        $i = 4;
        $cont = 1;
        foreach($factura->items_detalle_factura_destinos as $fila):
            if($fila->lotes->productos->con_vencimiento == 1)
            {
                $venicimiento = substr($fila->lotes->fecha_vencimiento,5,2).'/'.substr($fila->lotes->fecha_vencimiento,0,4);
            }
            else
            {
                $venicimiento = 'Sin Vto';
            }
            if($fila->id_paciente > 0)
            {
                $paciente = $fila->pacientes->apellido.' '.$fila->pacientes->nombre;
                
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i,  $fila->cantidad)
                        ->setCellValue('B'.$i,  $fila->productos->nombre)
                        ->setCellValue('C'.$i, $fila->lotes->lote)
                        ->setCellValue('D'.$i, $venicimiento)
                        ->setCellValue('E'.$i, $paciente)
                        ->setCellValue('F'.$i, $fila->remitos_egresos);
                        $i++;
                        $cont++;
            }
            if($fila->id_servicio > 0)
            {
                $servicio = $fila->servicios_destinos->nombre;
                
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i,  $fila->cantidad)
                        ->setCellValue('B'.$i,  $fila->productos->nombre)
                        ->setCellValue('C'.$i, $fila->lotes->lote)
                        ->setCellValue('D'.$i, $venicimiento)
                        ->setCellValue('E'.$i, $servicio)
                        ->setCellValue('F'.$i, $fila->remitos_egresos);
                        $i++;
                        $cont++;
            }
                
        endforeach;
       
                
        for($i = 'A'; $i <= 'D'; $i++){
            $objPHPExcel->setActiveSheetIndex(0)            
                ->getColumnDimension($i)->setAutoSize(TRUE);
        }
        
        // Se asigna el nombre a la hoja
        $objPHPExcel->getActiveSheet()->setTitle('Detalle General');

        // Se activa la hoja para que sea la que se muestre cuando el archivo se abre
        $objPHPExcel->setActiveSheetIndex(0);
        // Inmovilizar paneles 
        //$objPHPExcel->getActiveSheet(0)->freezePane('A4');
        $objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,4);

        // Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
        /*header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="ReporteGeneral.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');*/
       // $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
       // $objWriter->save(str_replace('.php', '.xlsx', __FILE__));

////////////////////////////////////////////////
         $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel); //Escribir archivo
         
        // Establecer formado de Excel 2003
        header("Content-Type: application/vnd.ms-excel");
         
        // nombre del archivo
        header('Content-Disposition: attachment; filename="Detalle Remito.xls"');
        $objWriter->save('php://output');
        //exit;
        //redirect('Facturas/listar_facturas');
        //print_r($contrato_actual);

    }
}
// end: excel