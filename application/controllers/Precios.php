<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Container\Container as Container;
use \Illuminate\Support\Facades\Facade as Facade;



class Precios extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Groups_eloquent');
		$this->load->model('Users_groups_eloquent');
		$this->load->model('Users_eloquent');
        $this->load->model('Productos_eloquent');
        $this->load->model('Contratos_eloquent');
        $this->load->model('Contrato_actual_eloquent');
        $this->load->model('Precios_eloquent');
		//$this->load->library(array('ion_auth','funciones'));
		
	}
	public function index()
	{
		$aux['controlador'] = 'Precios';
		$aux['metodo'] = 'index';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        	$data['contratos'] = Contratos_eloquent::activos()                                           
                                            ->get();
           
        	//echo "hola";
        	//die();
        	//$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
        	$data['contenido'] = "/parametros/precios/index";
			$this->load->view('templates/templateCodebase', $data);
        }
        else
        {
        	echo "error de permiso";
            die();
        }
	}
	//paso 1
	public function nuevo()
	{
		$aux['controlador'] = 'Precios';
		$aux['metodo'] = 'nuevo';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        	$data['contratos'] = Contratos_eloquent::activos()                                           
                                            ->get();
            $data['productos'] = Productos_eloquent::activos()                                           
                                            ->get();
        	//echo "hola";
        	//die();
        	//$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
        	$data['contenido'] = "/parametros/precios/nuevo";
			$this->load->view('templates/templateCodebase', $data);
        }
        else
        {
        	echo "error de permiso";
            die();
        }
	}
	public function guardar()
	{
		$precio_aux = Precios_eloquent::where('id_contrato',$this->input->post('id_contrato'))->where('id_producto',$this->input->post('id_producto'))->get();
		$cont = 0;
		foreach ($precio_aux as $aux) {
			$cont = $cont + 1;
			# code...
		}
		if($cont == 0)
		{
			$precio = new Precios_eloquent();
			$precio->id_contrato = $this->input->post('id_contrato');
			$precio->id_producto = $this->input->post('id_producto');
			$precio->precio = $this->input->post('precio');
			$precio->estado = 1;
			$precio->save();

			echo json_encode(array(
	            "status" => $precio->save(),
	            
	            ));
		}
		else
		{
			echo json_encode(array(
	            "status" => FALSE,
	            
	            ));
		}

		
	}
	public function traer_productos()
	{
		$id = $this->input->get('id_contrato');
		
		//echo $id;
		$data['contrato'] = Contratos_eloquent::find($id);
		$data['listado'] = $precios = Precios_eloquent::where('id_contrato',$id)->get();
                                        
        $this->load->view('/parametros/precios/precios_contrato', $data);

	}
	public function modificar($id)
    {
        $aux['controlador'] = 'Precios';
        $aux['metodo'] = 'modificar';
        /*$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        //print_r($permisos);
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        //print_r($obtener_permiso);
        if($obtener_permiso)
        {*/
           // echo "vista en produccion";
            
        $data['contenido'] = "/parametros/precios/editar";
        $data['precio'] = Precios_eloquent::find($id);
            
            
            //print_r($cliente->users_groups->users->documento);
           // die();
            $this->load->view('templates/templateCodebase', $data);
       // }
       
    }
    public function update()
    {
    	$precio = Precios_eloquent::find($this->input->post('id'));
    	$precio->precio = $this->input->post('precio');
        //$producto->descripcion = $this->input->post('descripcion');
       // $producto->id_tipo_prod = $this->input->post('id_tipo');
        //$tipo->estado = '1';
        $precio->save();

        echo json_encode(array(
            "status" => $precio->save(),
            ));  
    }  
}
