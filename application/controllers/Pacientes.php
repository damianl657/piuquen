<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Container\Container as Container;
use \Illuminate\Support\Facades\Facade as Facade;



class Pacientes extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Groups_eloquent');
		$this->load->model('Users_groups_eloquent');
		$this->load->model('Users_eloquent');
        $this->load->model('Pacientes_eloquent');
		//$this->load->library(array('ion_auth','funciones'));
		
	}
	
	public function index()
	{
		$aux['controlador'] = 'Pacientes';
		$aux['metodo'] = 'index';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        	//$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
        	$data['contenido'] = "/parametros/pacientes/index";
			$this->load->view('templates/templateCodebase', $data);
        }
        else
        {
        	echo "error de permiso";
            die();
        }
	}
	public function listar_pacientes()
    {
        $data['listado'] = Pacientes_eloquent::activos()                                           
                                            ->get();
                                        
        $this->load->view('/parametros/pacientes/listar_pacientes', $data);
        //echo json_encode(array("status" => TRUE, "clientes" => $this->load->view('clientes/listar_clientes', $data)));
    }
    public function nuevo()
	{
		$aux['controlador'] = 'Pacientes';
		$aux['metodo'] = 'nuevo';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        		
			
			$data['contenido'] = "/parametros/pacientes/nuevo";
            
			//print_r($data['bancos']);
			//die();
			$this->load->view('templates/templateCodebase', $data);
        }
        else
        {
        	echo"error permiso";
        	die();
        }
	}
	public function guardar()
	{
		//echo "ok";
		$paciente = new Pacientes_eloquent();
        $paciente->nombre = $this->input->post('nombre');
        $paciente->apellido = $this->input->post('apellido');
        $paciente->dni = $this->input->post('dni');
        $paciente->contacto = $this->input->post('contacto');
        $paciente->domicilio_entrega = $this->input->post('domicilio_entrega');
        $paciente->iva = $this->input->post('iva');
        $paciente->condicion_venta = $this->input->post('condicion_venta');
        $paciente->cuit = $this->input->post('cuit');

        $paciente->contacto1 = $this->input->post('contacto1');
        $paciente->nombre1 = $this->input->post('nombre1');
        $paciente->contacto2 = $this->input->post('contacto2');
        $paciente->nombre2 = $this->input->post('nombre2');
        $paciente->contacto3 = $this->input->post('contacto3');
        $paciente->nombre3 = $this->input->post('nombre3');
        $paciente->contacto4 = $this->input->post('contacto4');
        $paciente->nombre4 = $this->input->post('nombre4');
        $paciente->contacto5 = $this->input->post('contacto5');
        $paciente->nombre5 = $this->input->post('nombre5');
        //$producto->id_tipo_prod = $this->input->post('id_tipo');
        
        $paciente->estado = '1';
        $paciente->save();

        echo json_encode(array(
            "status" => $paciente->save(),
            
            ));        
	}
	public function detalle()
    {
        $id = $this->input->get('id');
        //echo $id;
        //$paciente = Pacientes_eloquent::find($id);
        
             $data['paciente'] = Pacientes_eloquent::find($id);
        $this->load->view('parametros/pacientes/detalle', $data);
    }
    public function modificar($id)
    {
        $aux['controlador'] = 'Pacientes';
        $aux['metodo'] = 'modificar';
        /*$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        //print_r($permisos);
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        //print_r($obtener_permiso);
        if($obtener_permiso)
        {*/
           // echo "vista en produccion";
            
        $data['contenido'] = "/parametros/pacientes/editar";
        $data['paciente'] = Pacientes_eloquent::find($id);
            
            
            //print_r($cliente->users_groups->users->documento);
           // die();
            $this->load->view('templates/templateCodebase', $data);
       // }
       
    }
    public function update()
    {
    	$paciente = Pacientes_eloquent::find($this->input->post('id'));
    	$paciente->nombre = $this->input->post('nombre');
        $paciente->apellido = $this->input->post('apellido');
        $paciente->dni = $this->input->post('dni');
        $paciente->contacto = $this->input->post('contacto');
        $paciente->domicilio_entrega = $this->input->post('domicilio_entrega');
        $paciente->iva = $this->input->post('iva');
        $paciente->condicion_venta = $this->input->post('condicion_venta');
        $paciente->cuit = $this->input->post('cuit');
        $paciente->contacto1 = $this->input->post('contacto1');
        $paciente->nombre1 = $this->input->post('nombre1');
        $paciente->contacto2 = $this->input->post('contacto2');
        $paciente->nombre2 = $this->input->post('nombre2');
        $paciente->contacto3 = $this->input->post('contacto3');
        $paciente->nombre3 = $this->input->post('nombre3');
        $paciente->contacto4 = $this->input->post('contacto4');
        $paciente->nombre4 = $this->input->post('nombre4');
        $paciente->contacto5 = $this->input->post('contacto5');
        $paciente->nombre5 = $this->input->post('nombre5');
        //$producto->id_tipo_prod = $this->input->post('id_tipo');
        
        $paciente->estado = '1';
        $paciente->save();

        echo json_encode(array(
            "status" => $paciente->save(),
            ));  
    }  
    public function baja()
    {
        //$cliente = Clientes_eloquent::find($id);
        $paciente = Pacientes_eloquent::find($this->input->post('id_paciente'));
        $paciente->estado = '0';
        $paciente->save();
        echo json_encode(array("status" => true));
        //echo "hola mundo";
    } 

}