<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Container\Container as Container;
use \Illuminate\Support\Facades\Facade as Facade;
set_time_limit(9000);
ini_set('MAX_EXECUTION_TIME', 9000);


class Facturacion extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Groups_eloquent');
		$this->load->model('Users_groups_eloquent');
		$this->load->model('Users_eloquent');
        $this->load->model('Productos_eloquent');
        $this->load->model('Contratos_eloquent');
        $this->load->model('Contrato_actual_eloquent');
        $this->load->model('Proveedores_eloquent');
        $this->load->model('Lotes_eloquent');
        $this->load->model('Items_remito_ingreso_eloquent');
        $this->load->model('Pedidos_eloquent');
        $this->load->model('Remitos_ingresos_eloquent');
        $this->load->model('Ingresos_eloquent');
        $this->load->model('Remitos_egresos_eloquent');
        $this->load->model('Pacientes_eloquent');
        $this->load->model('Servicios_destinos_eloquent');
        $this->load->model('Items_remito_egreso_eloquent');
        $this->load->model('Precios_eloquent');
        $this->load->model('Facturas_eloquent');
        $this->load->model('Remitos_facturados_eloquent');
        $this->load->model('Items_factura_eloquent');
        $this->load->model('Items_detalle_factura_eloquent');
        $this->load->model('Items_detalle_factura_destino_eloquent');
        $this->load->model('Facturas_Model');
        
       
		//$this->load->library(array('ion_auth','funciones'));
		
	}
	public function index()
    {
        $aux['controlador'] = 'Facturacion';
        $aux['metodo'] = 'index';
        $permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
            //$data['tarjetas'] = Tarjetas_eloquent::activos()->get();
            $data['contenido'] = "centroSalud/facturacion/index";
            $this->load->view('templates/templateCodebase', $data);
        }
        else
        {
            echo "error de permiso";
            die();
        }
    }
    
    public function listar()
    {
        //echo "string";
        $postData = $_POST;
   
        // Get data
        $data = $this->Facturas_Model->getListaPaginado($postData);  
        /*$array_data = array();  
        print_r($data); die();
        foreach($data as $row)
        {
            $array_data[] = $row;
        }*/
        echo json_encode($data);
    }
}