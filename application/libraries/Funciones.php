<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth
*
* Version: 2.5.2
*
* Author: Ben Edmunds
*		  ben.edmunds@gmail.com
*         @benedmunds
*
* Added Awesomeness: Phil Sturgeon
*
* Location: http://github.com/benedmunds/CodeIgniter-Ion-Auth
*
* Created:  10.01.2009
*
* Description:  Modified auth system based on redux_auth with extensive customization.  This is basically what Redux Auth 2 should be.
* Original Author name has been kept but that does not mean that the method has not been modified.
*
* Requirements: PHP5 or above
*
*/

class Funciones
{
	
	/**
	 * account status ('not_activated', etc ...)
	 *
	 * @var string
	 **/
	//protected $status;

	/**
	 * extra where
	 *
	 * @var array
	 **/
	//public $_extra_where = array();

	/**
	 * extra set
	 *
	 * @var array
	 **/
	//public $_extra_set = array();

	/**
	 * caching of users and their groups
	 *
	 * @var array
	 **/
	//public $_cache_user_in_group;

	/**
	 * __construct
	 *
	 * @author Ben
	 */
	function __construct() 
        {
	        //$this->load->config('ion_auth', TRUE);
	        $this->CI = & get_instance();
	        $this->CI->load->database();
			//$this->load->library(array('email'));
			//$this->lang->load('ion_auth');
			//$this->load->helper(array('cookie', 'language','url'));
			//$this->load->library('session');
			//$this->load->model('ion_auth_model');
        }

	function obtener_menu()
	{
		$sql_menu ="SELECT * FROM menu_acciones where estado = 1 ORDER BY orden ASC";
		$sql_menu = $this->CI->db->query($sql_menu);
		$menu = $sql_menu->result();
		
		return $menu;
	}
	public function validar_permiso($acceso)
	{
		
		$permisos_bd = $this->obtener_menu();
		
       // print_r($acceso['controlador']);
        //print_r($acceso['metodo']);
        //die();
        $bandera = 0;
        foreach ($permisos_bd as $permiso) 
        {
        	
        	
        	if(($acceso['controlador'] == $permiso->controlador) && ($acceso['metodo'] == $permiso->metodo) && ($permiso->estado == 1))
        	{
        		foreach ($acceso['pemisos_user'] as $key ) 
        		{
        			//print_r($key);
        			//die();
        			if($key == $permiso->id_menu)
        			{
        				$bandera = 1;
        			}
        		}
        	}
        		
        	
        }
        if($bandera == 1)
        {
        	return true;
        }
        else
        {
        	return false;
        }
        
	}
}