        <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;" <?php if( $institucionSistema == 'rawson') {?> style="background:#092c4e;" <?php } else {?> style="background:#420707;" <?php } ?>>
             <!--<a href="<?php base_url('home');?>" class="site_title"><img src="<?=base_url('assets/img/logo.jpg')?>" alt="..." class="i" width="200" height="50"> <span></span></a>-->
              <!--<a href="<?php //base_url('home');?>" class="site_title">--><!--<i class="fa fa-windows"></i> --><!--<span style="">Drogueria Piuquen</span></a>-->
              <a href="<?php base_url('home');?>" class="site_title" <?php if( $institucionSistema == 'rawson') {?> style="background:#092c4e; color: #164d88" <?php } else {?> style="background:#420707; color: #380b0b" <?php } ?>  ><!--<i class="fa fa-windows"></i> --><span style="font-size: 16px">Drogueria Piuquen</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu prile quick info -->
            <div class="profile">
              <div class="profile_pic">
                <img src="<?=base_url('assets/img/avatar_2x.png')?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bienvenido,</span>
                <h2><?=$this->session->userdata('first_name');?>, <?=$this->session->userdata('last_name');?></h2>
              </div>
            </div>
            <!-- /menu prile quick info -->

            <br />
            <?php $menus = $this->funciones->obtener_menu();
                  
                  $permisos_string = $this->session->userdata('permisos');
                  $permisos = explode(",", $permisos_string);
           //echo"menu: ".print_r($menus); 
          //echo" permisos: ".print_r($permisos);?>
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <!-- <h3>General</h3>-->
                <?php
                  if( $institucionSistema == 'rawson')
                  {
                    echo "<h3>Hospital Rawson</h3>";
                  }
                  else
                  {
                    echo "<h3>Hospital Marcial Quiroga</h3>";
                  }
                ?>
                <ul class="nav side-menu">
                  <?php
                  foreach ($menus as $menu)
                  {
                  //  print_r($menus);
                    //die();
                    if($menu->tipo == 'menu')
                    {
                      foreach ($permisos as $permiso)
                      {
                        if($permiso == $menu->id_menu)
                        {
                          ?>
                            
                            <li>
                                <a href="<?php echo base_url('index.php/'.$menu->link); ?> "><i class="<?php echo $menu->icono;?>"></i> <?php echo $menu->nombre; ?></a>
                            </li>
                           
                          <?php
                        }
                      }
                    }
                    else if($menu->tipo == 'menu_padre')
                    {
                      //print_r($menu);
                      //die();
                      foreach ($permisos as $permiso)
                      {
                        if($permiso == $menu->id_menu)
                        {
                          ?>
                            <li>
                                <a><i class="<?php echo $menu->icono; ?>"></i><?php echo $menu->nombre?><span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                    <?php 
                                      foreach ($menus as $menu_aux)
                                      {
                                          if(($menu_aux->tipo == 'sub_menu') && ($menu->id_menu == $menu_aux->id_padre_menu))
                                          {
                                            ?>
                                            <li><a href="<?php echo base_url('index.php/'.$menu_aux->link); ?> "><i class="<?php echo $menu_aux->icono; ?>"></i><?php echo $menu_aux->nombre?></a>
                                            </li>
                                            <?php
                                          }
                                          if(($menu_aux->tipo == 'sub_menu_padre') && ($menu->id_menu == $menu_aux->id_padre_menu))
                                          {
                                            //var_dump($menus);
                                           // die();
                                            ?>
                                            <li><a ><i class="<?php echo $menu_aux->icono; ?>"></i><?php echo $menu_aux->nombre?><span class="fa fa-chevron-down"></span></a>
                                                <ul class="nav child_menu">
                                                  <?php
                                                    foreach ($menus as $menu_aux_sub) 
                                                    {
                                                      if(($menu_aux_sub->tipo == 'sub_menu') && ($menu_aux->id_menu == $menu_aux_sub->id_padre_menu))
                                                        {
                                                          ?>
                                                            <li><a href="<?php echo base_url('index.php/'.$menu_aux_sub->link); ?> "><i class="<?php echo $menu_aux_sub->icono; ?>"></i><?php echo $menu_aux_sub->nombre?></a>
                                                            </li>
                                                          <?php
                                                        }
                                                    }
                                                  ?>
                                                  
                                                </ul>
                                            </li>
                                            <?php
                                          }
                                      }
                                    ?>
                                  </ul>
                            </li>
                          <?php
                        }
                      }
                    }

                    
                  }
                  ?>
                  
                </ul>
              </div>
              
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" href="<?=base_url('index.php/'.'auth/logout');?>" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

          <div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?=base_url('assets/img/avatar_2x.png')?>" alt=""><?=$this->session->userdata('first_name');?>, <?=$this->session->userdata('last_name');?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?=base_url('index.php/'.'auth/cambio_password');?>">Cambiar Contraseña</a>
                    </li>
                   
                    
                    <li><a href="<?=base_url('index.php/'.'auth/logout');?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    </li>
                  </ul>
                </li>

                

              </ul>
            </nav>
          </div>

        </div>
        <!-- /top navigation -->