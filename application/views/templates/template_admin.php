<?php //header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Methods: GET, POST');?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Drogueria Piuquen</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        
        

         <!-- Bootstrap -->
        <link rel="stylesheet" href="<?=base_url('assets/bootstrap/dist/css/bootstrap.min.css')?>" media="screen">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?=base_url('assets/font-awesome/css/font-awesome.min.css')?>" media="screen">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?=base_url('assets/iCheck/skins/flat/green.css')?>" media="screen">
         <!-- jQuery custom content scroller -->
        <link href="<?=base_url('assets/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css')?>"  rel="stylesheet"/>

        <!-- bootstrap-progressbar -->
        <link rel="stylesheet" href="<?=base_url('assets/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')?>" media="screen">
        
       
         <!-- Datepicker -->
        <link rel="stylesheet" href="<?=base_url('assets/css/bootstrap-datepicker.css')?>" media="screen">
        <link rel="stylesheet" href="<?=base_url('assets/css/bootstrap-datepicker.min.css')?>" media="screen">
        <!-- Custom Theme Style -->
        <link rel="stylesheet" href="<?=base_url('assets/css/custom.css')?>" media="screen">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?=base_url('assets/iCheck/skins/flat/green.css')?>">
        <!-- Datatables -->
        <link rel="stylesheet" href="<?=base_url('assets/datatables.net-bs/css/dataTables.bootstrap.min.css')?>">
        <link rel="stylesheet" href="<?=base_url('assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')?>">
        <link rel="stylesheet" href="<?=base_url('assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')?>">
        <link rel="stylesheet" href="<?=base_url('assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')?>">
        <link rel="stylesheet" href="<?=base_url('assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')?>">
        <link rel="stylesheet" href="<?=base_url('assets/toastr/toastr.min.css')?>">
        
        <!-- jQuery -->
        <script src="<?=base_url('assets/jquery/dist/jquery.min.js')?>"></script>
        <script src="<?=base_url('assets/js/jquery.validate.min.js')?>"></script>
        <!-- Bootstrap -->
        <script src="<?=base_url('assets/bootstrap/dist/js/bootstrap.min.js')?>"></script>
         <!-- Select -->
        <link rel="stylesheet" href="<?=base_url('assets/select/bootstrap-select.min.css')?>">
        <script src="<?=base_url('assets/select/bootstrap-select.min.js')?>"></script>

        <script>
            var base_url = "<?=base_url()?>";
        </script> 
        
    </head>
    <?php 
    //echo base_url();die();
      $cadena_de_texto = base_url();
      $cadena_buscada   = 'mq';
     
      $posicion_coincidencia = strpos($cadena_de_texto, $cadena_buscada);
     
    //se puede hacer la comparacion con 'false' o 'true' y los comparadores '===' o '!=='
      if ($posicion_coincidencia === false) {
        $institucionSistema = 'rawson';
      } 
      else {
        $institucionSistema = 'mq';
      }

      if( $institucionSistema == 'rawson')
      {
                ?>
                    <body class="nav-md" style="background:#092c4e; color: #164d88">
                <?php
      }
      else
      {
                ?>
                    <body class="nav-md" style="background:#420707; color: #380b0b">
                <?php
      }
      
     
    ?>
   
    <div class="container body">
      <div class="main_container">
        <?
            $data2['institucionSistema']=$institucionSistema;  
            $this->load->view('templates/menu', $data2);

        ?>

        <?
        
           // $this->load->view('templates/admin_menu');

        ?>
        <!-- page content -->
        <?
        
            //$this->load->view('principal/principal');

        ?>
         <!-- page content -->
        <div class="right_col" role="main">
          
            <?
                        if(!empty($contenido))
                            $this->load->view($contenido);
          ?>
        </div>
       
        
         <!-- /page content 

        <!-- page content -->
        

        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            <i class="fa fa-user"> </i> Desarrollado por Damian Leal - Lic. Sistemas de Información - Contacto: <i class="fa fa-phone"></i> 2645671551 - <i class="fa fa-google-plus"></i> damianl657@gmail.com
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
    
        <!-- FastClick -->
        <script src="<?=base_url('assets/fastclick/lib/fastclick.js')?>"></script>
        
        <!-- NProgress -->
        <script src="<?=base_url('assets/nprogress/nprogress.js')?>"></script>
        <!-- Chart.js -->
        <script src="<?=base_url('assets/Chart.js/dist/Chart.min.js')?>"></script>
        <!-- gauge.js -->
        <script src="<?=base_url('assets/bernii/gauge.js/dist/gauge.min.js')?>"></script>
        <!-- bootstrap-progressbar -->
        <script src="<?=base_url('assets/bootstrap-progressbar/bootstrap-progressbar.min.js')?>"></script>
         <!-- jQuery custom content scroller -->
        <script src="<?=base_url('assets/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js')?>"></script>
        <!-- iCheck -->
        <script src="<?=base_url('assets/iCheck/icheck.min.js')?>"></script>
        <!-- Skycons -->
        <script src="<?=base_url('assets/skycons/skycons.js')?>"></script>
        
        <script src="<?=base_url('assets/js/maps/jquery-jvectormap-2.0.3.min.js')?>"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="<?=base_url('assets/js/moment/moment.min.js')?>"></script>
        <script src="<?=base_url('assets/js/datepicker/daterangepicker.js')?>"></script>
        <!-- Datatables -->
        <script src="<?=base_url('assets/datatables.net/js/jquery.dataTables.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-buttons/js/dataTables.buttons.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-buttons/js/buttons.flash.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-buttons/js/buttons.html5.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-buttons/js/buttons.print.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-keytable/js/dataTables.keyTable.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-responsive/js/dataTables.responsive.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-responsive-bs/js/responsive.bootstrap.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-scroller/js/datatables.scroller.min.js')?>"></script>
        <script src="<?=base_url('assets/jszip/dist/jszip.min.js')?>"></script>
        <script src="<?=base_url('assets/pdfmake/build/pdfmake.min.js')?>"></script>
        <script src="<?=base_url('assets/pdfmake/build/vfs_fonts.js')?>"></script>
        <script src="<?=base_url('assets/toastr/toastr.min.js')?>"></script>
        <!-- Datatables -->
       
        <!-- /Flot -->

        

        <?//Direccion base de la aplicacion por si es necesario usarla en algun momento desde javascript?>
         <!-- Datepicker -->
        <script src="<?=base_url('assets/js/bootstrap-datepicker.js')?>"></script>    
        <!-- Custom Theme Scripts -->
        <script src="<?=base_url('assets/js/custom.js')?>"></script>
        
    </body>
    
</html>