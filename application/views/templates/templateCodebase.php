<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Drogueria Piuquen</title>

        <meta name="description" content="Damian">
        <meta name="author" content="Damian Leal">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Drogueria Piuquen">
        <meta property="og:site_name" content="Drogueria Piuquen">
        <meta property="og:description" content="Drogueria Piuquen">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?=base_url('assets/codebase/media/favicons/favicon.png')?>">
        
        <link rel="icon" type="image/png" sizes="192x192" href="<?=base_url('assets/codebase/media/favicons/favicon-192x192.png')?>" media="screen">
        
        <link rel="apple-touch-icon" sizes="180x180" href="<?=base_url('assets/codebase/media/favicons/apple-touch-icon-180x180.png')?>" media="screen">
       
        <!-- END Icons -->

        <!-- Stylesheets -->

        <!-- Fonts and Codebase framework -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
       
        
        <link rel="stylesheet" id="css-main" href="<?=base_url('assets/codebase/css/codebase.min.css')?>">
        <link rel="stylesheet" id="css-main" href="<?=base_url('assets/codebase/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')?>">
        <link rel="stylesheet" id="css-main" href="<?=base_url('assets/codebase/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')?>">
        <link rel="stylesheet" href="<?=base_url('assets/toastr/toastr.min.css')?>">
        <link rel="stylesheet" id="css-main" href="<?=base_url('assets/codebase/js/plugins/select2/css/select2.min.css')?>">
        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
        
        <script src="<?=base_url('assets/codebase/js/codebase.core.min.js')?>"></script>
        <script src="<?=base_url('assets/codebase/js/codebase.app.min.js')?>"></script>
        <script src="<?=base_url('assets/codebase/js/plugins/datatables/jquery.dataTables.min.js')?>"></script>
        <script src="<?=base_url('assets/codebase/js/plugins/datatables/dataTables.bootstrap4.min.js')?>"></script>
        
        <script src="<?=base_url('assets/codebase/js/plugins/sweetalert2/sweetalert2.min.js')?>"></script>
        <script src="<?=base_url('assets/codebase/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
        <script src="<?=base_url('assets/codebase/js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')?>"></script>
        <script src="<?=base_url('assets/js/jquery.validate.min.js')?>"></script>
        <script src="<?=base_url('assets/toastr/toastr.min.js')?>"></script>
        <script src="<?=base_url('assets/codebase/js/plugins/select2/js/select2.full.min.js')?>"></script>
        <script src="<?=base_url('assets/codebase/js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js')?>"></script>

    </head>
    <?php 
    //echo base_url();die();
      $cadena_de_texto = base_url();
      $cadena_buscada   = 'mq';
     
      $posicion_coincidencia = strpos($cadena_de_texto, $cadena_buscada);
     
      if ($posicion_coincidencia === false) {
        $institucionSistema = 'rawson';
      } 
      else {
        $institucionSistema = 'mq';
      }
      $data2['institucionSistema']=$institucionSistema;  
      $this->load->view('templates/menuCodebase', $data2);

                        if(!empty($contenido))
                            $this->load->view($contenido);
          
     
    ?>
            <!-- Footer -->
        <footer id="page-footer" class="opacity-0">
                <div class="content py-20 font-size-xs clearfix">
                    <div class="float-left">
                       Desarrollado por Damian Leal - Licenciado en Sistemas de Información 
                    </div>
                    <div class="float-right">
                        Contacto: <i class="fa fa-phone"></i> 2645671551 - <i class="fa fa-google-plus"></i> damianl657@gmail.com
                    </div>
                </div>
            </footer>
    </div>
        <!-- END Page Container -->
   
        
        <!--
            Codebase JS

            Custom functionality including Blocks/Layout API as well as other vital and optional helpers
            webpack is putting everything together at assets/_es6/main/app.js
        -->
        

        <!-- Page JS Plugins -->
        <script src="<?=base_url('assets/codebase/js/plugins/chartjs/Chart.bundle.min.js')?>"></script>

        <!-- Page JS Code -->
        <script src="<?=base_url('assets/codebase/js/pages/be_pages_dashboard.min.js')?>"></script>
        <!-- Page JS Plugins -->
        
       

        <!-- Page JS Code -->
        
        <script>
            
            jQuery(document).ready(function($) {
           
               
                
            });
        </script>
    </body>
</html>