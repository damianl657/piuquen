<!-- Main Container -->
<main id="main-container">
<!-- Page Content -->
<div class="content">
    
    <div class="col-md-12 col-lg-12">
            <!-- Default Elements -->
            <div class="block block-themed">
                <div class="block-header bg-gd-aqua">
                    <h3 class="block-title"><a class="btn btn-secondary" href="javascript:window.history.back();"><i class="fa fa-arrow-left"></i></a>  </h3>
                    <h3 class="block-title">Remitos del Pedido Nº <? echo $ingreso->pedidos->id;?></h3>
                    <div class="block-options">
                        <button type="button" id="alta" class="btn btn-rounded btn-primary min-width-125 mb-10">
                            <i class="si si-plus"></i> Nuevo Remito - Carga Stock
                        </button>
                    </div>
                </div>
                <div id="loading"></div>
                <div class="block-content block-content-full" id="listado_clientes">
                    
                </div>
            </div>
            <!-- END Default Elements -->
    </div>
    
</div>
<!-- END Page Content -->
</main>
<!-- MODAL DETALLES-->
<div class="modal fade" id="detalle" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-popout" role="document" id="mdialTamanio">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title" id="">Detalle</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                        <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content" id="detalle_remito">
                    
                
                </div>
                <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>       
            </div>
        </div>
    </div>
</div> 
<!-- MODAL BAJA PROD-->
<div class="modal fade" id="modal_baja_producto" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-popout" role="document" id="mdialTamanio">
            
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title" id="">Baja de Producto</h3>
                    <div class="block-options">
                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                    <i class="si si-close"></i>
                    </button>
                    </div>
                </div>
                <div class="block-content">
                    <form action="#" id="form_baja" method="post" class="form-horizontal">
                        <input type="hidden" value="" name="id_producto" id ="id_producto"/> 
                        <label>¿Desea dar de baja el producto seleccionado?</label>
                    </form>
                </div>
                </div>
                <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-alt-danger" id="baja_producto" data-dismiss="modal">
                            <i class="fa fa-check"></i> Aceptar
                        </button>
                </div>
            </div>
    </div>
</div>
<!-- -->
<script >
        function confirm_baja(id)
          {
            //alert(id);
             //document.getElementById("id_cliente").innerHTML = id;
             $("#id_proveedor").val(id);
            $("#modal_baja_proveedor").modal('show');
          }
         
        var id_ingreso = <?php echo $ingreso->id;?>;
          traer_remitos(id_ingreso);
          function traer_remitos()
          {
            //alert('hola');
            $.ajax
                ({
                  url: '<?php echo site_url("/Remitos/listar_remitos/"); ?>',
                  dataType:'text',
                 // dataType: "JSON",
                  type: 'get',
                  //dataType: "JSON",
                  data :{id_ingreso: id_ingreso},
                  beforeSend: function() {
           // $('#loadingNiv').show()
                     $('#listado_clientes').append("<img  id='loading_scroll' src='<?=base_url('assets/loading.gif');?>' height='50' width='50' />");
                      //$('#loading_scroll').show();
                     
                  }, 
                  success: function(data)
                  {
                      //alert('entro');
                      $('#loading_scroll').remove();
                      //onsole.log(data);
                      $("#listado_clientes").append(data);
                      reload_datatable();
                            
                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                            //alert('error');
                            $('#loading_scroll').remove();
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            //console.log(data);

                  }
                });
          }
          
          function reload_datatable() 
          {
              if ($("#datatable-buttons").length) 
              {
                $("#datatable-buttons").DataTable({
                  responsive: true

                });
            }
          }
          /*function baja_cliente()
          {
            alert('hola muenndo')
          }*/

       
          $(document).ready(function() 
              {
                $('#baja_proveedor').click(function(event) {
                  //alert('hola mundo');
                  $.ajax
                    ({
                                          url: '<?php echo site_url("/Proveedores/baja/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: $('#form_baja').serialize(),
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              //console.log(data);
                                              console.log('ok');
                                              
                                              $('#listado_clientes').empty();
                                              traer_proveedores();
                                              
                                              
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                    });
                  
                });

              })
       
          $(document).ready(function() {
            var handleDataTableButtons = function() {
              if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                  dom: "Bfrtip",
                  
                  responsive: true

                });
              }
            };

            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons();
                }
              };
            }();

            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });

            $('#datatable-responsive').DataTable();

            $('#datatable-scroller').DataTable({
              ajax: "js/datatables/json/scroller-demo.json",
              deferRender: true,
              scrollY: 380,
              scrollCollapse: true,
              scroller: true
            });

            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });

            //TableManageButtons.init();
          });
       
          $(document).ready(function()
            {
            $('#alta').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Remitos/nuevo').'/'.$ingreso->id?>/";

            });
          });    
        
          function ver_detalles(id)
          {
            
              $("#detalle_remito").empty();
             $.ajax({
            url: '<?php echo site_url("/Remitos/detalle/"); ?>',
              dataType:'text',
                            type: 'get',
                            //dataType: "JSON",
            data :{id: id},
            success: function(data)
            {
              console.log(data);

              $("#detalle_remito").html(data);
              
              
              
              $("#detalle").modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }

            })//alert(id);
            
          }
        </script>