<!-- plantilla nueva-->
<!-- Main Container -->
<main id="main-container">
<!-- Page Content -->
<div class="content">
<!-- Default Elements -->
  <div class="block block-themed ">
    <div class="block-header bg-gd-sea">
      <h3 class="block-title"><a class="btn btn-secondary" href="javascript:window.history.back();"><i class="fa fa-arrow-left"></i></a>  </h3>
      <h3 class="block-title">Formulario de Alta Remito</h3>
      <div class="block-options">
      </div>
    </div>
  
    <div class="block-content block-content-full">
      <div class="row justify-content-center py-20">
        <form id="alta" autocomplete="off" method="post" class="form-horizontal form-label-left">
          <div class="col-xl-12">
            <input type="hidden" id="producto_alta" name="producto_alta" value="nuevo" />
            <input type="hidden" id="id_user_group" name="id_user_group" value="0" />
            <input type="hidden" id="user_id" name="user_id" value="0" />
            <input type="hidden" name="id_ingreso" value="<?=$ingreso->id;?>" />  
            <input type="hidden" name="id_proveedor" value="<?=$ingreso->id_proveedor;?>" /> 
            <div class="form-group">
	                        <label class="control-label " for="first-name" >Pedido Nº: <?php echo $ingreso->pedidos->id;?> <span class="required"></span>
	                        </label>
            </div>
            <div class="form-group">
                          <label class="control-label " for="first-name" >Ingreso Nº: <?php echo $ingreso->id;?> <span class="required"></span>
                          </label>
	                        
	                      </div>
            <div class="form-group">
                          <label class="control-label " for="first-name" >Proveedor: <?php echo $ingreso->proveedores->nombre;?> <span class="required"></span>
                          </label>
                          
            </div> 
            <div class="form-group">
              <div class="form-group row">
                <label class="col-lg-4 col-form-label" for="val-username">Fecha de Remito </label>
                <div class="col-lg-8">
                  <input type="text" class="form-control" id="fecha" name="fecha" >
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-group row">
                <label class="col-lg-4 col-form-label" for="val-username">Nº de Remito </label>
                <div class="col-lg-8">
                  <input type="text" class="form-control" id="remito" name="remito" placeholder="Ingrese Nº de Remito">
                </div>
              </div>
            </div>
            
            
          </div>
          <div class="form-group row">
            <div class="col-lg-8 ml-auto">
              <a href="<?php echo site_url("Ingresos/pedidos/");?>" class="btn btn-success">Cancelar</a>
              <button id ="guardar" type="guardar" class="btn btn-alt-primary">Registrar Lotes</button>
            </div>
          </div>

        </form>
          
        <div id="contenido_form_lote">
        <form id="alta_lote" autocomplete="off" method="post" class="form-horizontal form-label-left">
          <div  type="hidden" class="col-xl-12">
            <input type="hidden" id="id_remito" name="id_remito" value="0" />
                <div class="form-group">
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label" for="val-username">Lote </label>
                        <div class="col-lg-8">
                        <input type="text" class="form-control" id="lote" name="lote" placeholder="Ingrese lote">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label" for="val-username">Producto </label>
                        <div class="col-lg-8">
                        <select id="id_producto" class="js-select2 form-control " data-live-search="true" required name="id_producto">
                                           
                                              <?php foreach ( $productos as $producto) { ?>
                                                    <option  value="<?php echo $producto->id;?>"><?php echo $producto->nombre; ?></option>
                                              <?php } ?>
                              
                                         </select>
                        </div>
                    </div>
                   
                </div>
                
                <div class="form-group">
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label" for="val-username">Fecha de Vencimiento "Dia/Mes/Año" </label>
                        <div class="col-lg-8">
                        <input type="text" class="form-control" id="fecha_vencimiento" name="fecha_vencimiento" placeholder="Ingrese Fecha de Vencimiento Dia/Mes/Año">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label" for="val-username">Cantidad </label>
                        <div class="col-lg-8">
                        <input type="number" class="form-control" id="cantidad" name="cantidad" placeholder="Ingrese cantidad">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-8 ml-auto">
                        <button id ="agregar_carrito" type="agregar_carrito" class="btn btn btn-warning">Registrar Lotes</button>
                    </div>
                </div>
                <div class="form-group">
                            
                    <div id="contenidodelcarrito"></div>
                </div>
                <div class="form-group" id="finalizar">
                            
                    <a href="<?php echo site_url("Ingresos/Index/").'/';?>" class="btn btn-primary">Finalizar</a>
                </div>
          </div>        
        </form>
        </div>
         
            
      </div>
    </div><!-- END block-content block-content-full -->
  </div><!-- END block -->
</div><!-- END content -->          
  
    

<!-- END Page Content -->

</main>

<!-- END Main Container -->
<!-- fin plantilla nueva-->
  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-popout" role="document">
        
      <div class="modal-content">
        <div class="block block-themed block-transparent mb-0">
          <div class="block-header bg-primary-dark">
            <h3 class="block-title" id="myModalLabel"></h3>
            <div class="block-options">
              <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
              <i class="si si-close"></i>
              </button>
            </div>
          </div>
          <div class="block-content">
                  <p id="mensaje"></p>
          </div>
        </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-alt-success" id="cerrar" data-dismiss="modal">
                    <i class="fa fa-check"></i> Cerrar
                </button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal Error-->
  <div class="modal fade" id="myModal_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="block block-themed block-transparent mb-0">
          <div class="block-header bg-primary-dark">
            <h3 class="block-title" id="myModalLabel_Error"></h3>
            <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
            </div>
          </div>
          <div class="block-content">
                  <p id="mensaje_error"></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-alt-success"  data-dismiss="modal">
          <i class="fa fa-check"></i> Cerrar
          </button>
        </div>
      </div>
    </div>
  </div>
<script >
  $('#contenido_form_lote').hide();
  $('#finalizar').hide();
  $(document).ready(function() 
              {
                $('#cerrar').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Ingresos/index')?>/";
                });
                $('#cerrar2').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Ingresos/index')?>/";
                });
                $('#fecha').datepicker({
                    format: 'dd/mm/yyyy',
                    //startDate: '-3d'
                });
                $('#fecha_vencimiento').datepicker({
                    format: 'dd/mm/yyyy',
                    //startDate: '-3d'
                });
                
                $('#id_producto').select2({});
              })

    $(document).ready(function() 
              {   var dni_repetido = 0;
                  $('#guardar').click(function()
                  {   
                     
                          $('#alta').validate
                          ({
                              rules: {
                                  id_pedido: { required: true},
                                  
                              },
                              messages: {
                                  id_pedido:  {required: "Debe introducir el Nombre."}, 
                                  
                              },
                              submitHandler: function(form)
                              {
                                  $.ajax
                                  ({
                                          url: '<?php echo site_url("/Remitos/registar_remito/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: $('#alta').serialize(),
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              console.log(data);
                                              if (data.status == true)
                                              {
                                                  console.log(data);
                                                  $("#fecha").attr('disabled','');
                                                  $("#remito").attr('disabled','');
                                                  $("#id_remito").val(data.id_remito);
                                                  $("#botones_ocultar").hide();
                                                  $("#contenido_form_lote").show();
                                                  $("#finalizar").show();
                                                  //traer_formulario_lote();
                                                  //alert('El Cliente se registro en forma satifactoria');
                                                  /*document.getElementById("myModalLabel").innerHTML = 'Registro de Ingresos';
                                                  document.getElementById("mensaje").innerHTML = 'El Ingreso se registro en forma satifactoria';
                                                  $("#myModal").modal('show');
                                                  jQuery.fn.reset = function () 
                                                  {
                                                      $(this).each (function() 
                                                          { this.reset(); 
                                                          });
                                                  }
                                                  $("#alta").reset();*/

                                                  //document.location.href = "<?php echo site_url('Login')?>/";
                                              }
                                              else
                                              {
                                                  //alert('El Cliente ya esta registrado');
                                                  //document.getElementById("myModalLabel_Error").innerHTML = 'Error';
                                                  //document.getElementById("mensaje_error").innerHTML = 'Error';
                                                  //$("#myModal_error").modal('show');
                                              }
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                            // alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                                  });
                              }
                          });
                  });
                  function traer_formulario_lote()
                  {
                    console.log('hola mundo');
                    
                  }
              });
              </script>
              <script type="text/javascript">
          $(document).ready(function()
            {
            $('#cancelar').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Ingresos/pedidos')?>/";

            });
          });    
        </script>
        <script>
      $(document).ready(function() {
        
      });
   
      


$(document).ready(function()
{
  /*$('#agregar_cuenta').click(function() 
  {
    alert('hola munedo');
  });*/
});      
      

  $(document).ready(function() 
    {
      $('#agregar_carrito').click(function() 
      {
                  
        //alert('hola');
        console.log('hola');
        //if(($('#lote').val() != null) && ($('#fecha_vencimiento').val() != null))
        //{
          $('#alta_lote').validate
                            ({
                              rules: {
                                    cantidad: { required: true, minlength: 1, min: 1, digits: true},
                                    lote: { required: true},
                                },
                                messages: {
                                    cantidad: {required: "Debe introducir la cantidad.", min: "Debe introducir un valor mayor a 0"},
                                    lote: { required: "Debe introducir el Lote."},
                                },
                                submitHandler: function(form)
                                {
                                  $.ajax
                                  ({
                                      url: '<?php echo site_url("Remitos/registrar_lote"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          data: $('#alta_lote').serialize(),
                                          success: function(data)
                                          {
                                            jQuery.fn.reset = function () 
                                                    {
                                                        $(this).each (function() 
                                                            { this.reset(); 
                                                            });
                                                    }
                                                    $("#alta_lote").reset();
                                                    
                                            
                                            $(this).closest('form').find('.selectpicker').each(function() {
                                                $(this).selectpicker('render');
                                            });
                                            $("#contenidodelcarrito").html(data);
                                            
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                          } 
                                  });
                                }
                            });
        //}
          
      });
               
    })
</script>
