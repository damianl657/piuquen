
<div class="">
    <div class="page-title">
      <div class="title_left">
       
        
      </div>
      <div class="clearfix"></div>
	  <div class="row">
	              <div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
	                  <div class="x_title">
	                    <h2>Formulario de Editar Remito</h2>
	                    <ul class="nav navbar-right panel_toolbox">
	                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	                      </li>
	                      
	                      <li><a class="close-link"><i class="fa fa-close"></i></a>
	                      </li>
	                    </ul>
	                    <div class="clearfix"></div>
	                  </div>
	                  <div class="x_content">
	                    <br />
	                    <form id="alta" autocomplete="on" method="post" class="form-horizontal form-label-left">
                        <input type="hidden" id="producto_alta" name="producto_alta" value="nuevo" />
                        <input type="hidden" id="id_user_group" name="id_user_group" value="0" />
                        <input type="hidden" id="id" name="id" value="<?php echo $remito->id;?>" />
                        <input type="hidden" name="id_ingreso" value="<?=$remito->ingresos->id;?>" />  
                        <input type="hidden" name="id_proveedor" value="<?=$remito->ingresos->id_proveedor;?>" />  
	                      <div class="form-group">
	                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name" >Pedido Nº: <?php echo $remito->ingresos->pedidos->id;?> <span class="required"></span>
	                        </label>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name" >Ingreso Nº: <?php echo $remito->ingresos->id;?> <span class="required"></span>
                          </label>
	                        
	                      </div>
                        <div class="form-group">
                          <label class="control-label col-md-6 col-sm-6 col-xs-12" for="first-name" >Proveedor: <?php echo $remito->ingresos->proveedores->nombre;?> <span class="required"></span>
                          </label>
                          
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de Remito<span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="birthday" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" name="fecha" value="<?php echo date('d/m/Y', strtotime($remito->fecha));?>">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" >Nº de Remito <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="remito" name="remito" class="form-control col-md-7 col-xs-12" value="<?php echo $remito->remito;?>"> 
                          </div>
                        </div>
                        <div class="form-group">
                          <center><h2>Lotes Registrados Anteriormente<small></small></h2></center>
                            <table class="table table-striped table-hover table-bordered">
                              <thead>
                                <tr>
                                  
                                    <th>Producto</th>
                                    <th>Lote </th>
                                    <th>Cant </th>
                                    <th>Vencimiento </th>
                                </tr>
                              </thead>
                              <tbody>
                                <tbody>
                                                
                                                    <?php
                                                    
                                                    foreach ( $remito->items_remito_ingreso as $item) 
                                                    { 
                                                      
                                                          
                                                          ?>
                                                          <tr>
                                                            
                                                            <th>
                                                                <?php 
                                                                echo $item->lotes->productos->nombre;?>
                                                            </th>
                                                             <th>
                                                                <?php echo $item->lotes->lote;?>
                                                            </th>
                                                            <th>
                                                                <?php echo $item->cantidad;?>
                                                            </th>
                                                            <th>
                                                                <?php echo $item->lotes->fecha_vencimiento;?>
                                                            </th>
                                                            
                                                           
                                                              
                                                         
                                                          </tr>
                            
                                              <?php 
                                                }
                                                    //echo $acu;
                                                    ?>
                                                    
                                              
                                            
                                            </tbody>
                            </table>
                        </div>
	                      <div class="ln_solid"></div>
	                      

	                    </form>
                      <form id="alta_lote" autocomplete="on" method="post" class="form-horizontal form-label-left">
                        <input type="hidden" id="id_remito" name="id_remito" value="<?php echo $remito->id;?>" />
                        <div id="contenido_form_lote" type="hidden">
                          <div class="form-group">
                            <center><h2>Registrar Lotes Nuevos<small></small></h2></center>
                          </div>
                        
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" >Lote <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="text" id="lote" name="lote" class="form-control col-md-7 col-xs-12">
                            </div>

                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-6" for="first-name" >Producto <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <select id="id_producto" class="selectpicker show-tick form-control col-md-7 col-xs-12 " data-live-search="true" required name="id_producto">
                                           
                                              <?php foreach ( $productos as $producto) { ?>
                                                    <option  value="<?php echo $producto->id;?>"><?php echo $producto->nombre; ?></option>
                                              <?php } ?>
                              
                                         </select>
                            </div>
                            <br></br>
                          <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de Vencimiento<span class="required">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="birthday2" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" name="fecha_vencimiento">
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" >Cantidad <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <input type="number" id="cantidad" name="cantidad" class="form-control col-md-7 col-xs-12">
                            </div>

                          </div>
                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="">
                              
                              <button type="" id="agregar_carrito" class="btn btn-warning"><span class="glyphicon glyphicon-compressed" > Registrar Lote</button>
                            </div>
                          </div>

                          <div class="form-group">
                            
                                  <div id="contenidodelcarrito"></div>
                                  
                                
                          </div>
                         

                            
                          </div>
                        </div>
                      </form>
                       <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="botones_ocultar">
                              <a href="<?php echo site_url("Ingresos/index/").'/';?>" class="btn btn-primary">Cancelar</a>
                              <button type="" id="guardar" class="btn btn-success">Guardar</button>
                            </div>
                          </div>
	                  </div>
	                </div>
	              </div>
	  </div>
<script>
  //$('#contenido_form_lote').hide();
  $('#finalizar').hide();
</script>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="cerrar2"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"></h4>
          </div>
          <div class="modal-body">
            <p id="mensaje"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
          </div>
         </div>
       </div>
    </div>
    <!-- Modal Error-->
    <div class="modal fade" id="myModal_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id=""><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel_Error"></h4>
          </div>
          <div class="modal-body">
            <p id="mensaje_error"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="">Cerrar</button>
          </div>
         </div>
       </div>
    </div>
   </div>
</div>
<script >

  $(document).ready(function() 
              {
                $('#cerrar').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Ingresos/index')?>/";
                });
                $('#cerrar2').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Ingresos/index')?>/";
                });
              })
</script>
   <script type="text/javascript">
              $(document).ready(function() 
              {   var dni_repetido = 0;
                  $('#guardar').click(function()
                  {   
                     
                          
                                  $.ajax
                                  ({
                                          url: '<?php echo site_url("/Remitos/update/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: $('#alta').serialize(),
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              console.log(data);
                                              if (data.status == true)
                                              {
                                                  console.log(data);
                                                  
                                                  //traer_formulario_lote();
                                                  //alert('El Cliente se registro en forma satifactoria');
                                                  document.getElementById("myModalLabel").innerHTML = 'Registro de Remitos';
                                                  document.getElementById("mensaje").innerHTML = 'El Remito se registro en forma satifactoria';
                                                  $("#myModal").modal('show');
                                                  jQuery.fn.reset = function () 
                                                  {
                                                      $(this).each (function() 
                                                          { this.reset(); 
                                                          });
                                                  }
                                                  $("#alta").reset();

                                                  //document.location.href = "<?php echo site_url('Login')?>/";
                                              }
                                              else
                                              {
                                                  //alert('El Cliente ya esta registrado');
                                                  //document.getElementById("myModalLabel_Error").innerHTML = 'Error';
                                                  //document.getElementById("mensaje_error").innerHTML = 'Error';
                                                  //$("#myModal_error").modal('show');
                                              }
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                            // alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                                  });
                           
                  });
                  function traer_formulario_lote()
                  {
                    console.log('hola mundo');
                    
                  }
              });
              </script>
              <script type="text/javascript">
          $(document).ready(function()
            {
            $('#cancelar').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Ingresos/pedidos')?>/";

            });
          });    
        </script>
        <script>
      $(document).ready(function() {
        $('#birthday').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_3",
        showDropdowns: true
          
        }, function(start, end, label) {
          console.log(start.toISOString(), end.format('YYYY-MM-DD'), label);
        });
        $('#birthday2').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_3",
        showDropdowns: true
          
        }, function(start, end, label) {
          console.log(start.toISOString(), end.format('YYYY-MM-DD'), label);
        });
      });
    </script>
<script>
$(document).ready(function()
{
  /*$('#agregar_cuenta').click(function() 
  {
    alert('hola munedo');
  });*/
});      
      
</script>
<script >
  $(document).ready(function() 
    {
      $('#agregar_carrito').click(function() 
      {
                  
        //alert('hola');
        console.log('hola');
        $('#alta_lote').validate
                          ({
                            rules: {
                                  cantidad: { required: true, minlength: 1, min: 1, digits: true},
                                  birthday2: { required: true, date: true},
                              },
                              messages: {
                                  cantidad: {required: "Debe introducir la cantidad.", min: "Debe introducir un valor mayor a 0"},
                                  birthday2: { required: "Debe introducir la fecha de vencimiento."},
                              },
                              submitHandler: function(form)
                              {
                                $.ajax
                                ({
                                    url: '<?php echo site_url("Remitos/registrar_lote"); ?>',
                                        dataType:'text',
                                        type: 'POST',
                                        data: $('#alta_lote').serialize(),
                                        success: function(data)
                                        {
                                          jQuery.fn.reset = function () 
                                                  {
                                                      $(this).each (function() 
                                                          { this.reset(); 
                                                          });
                                                  }
                                                  $("#alta_lote").reset();
                                          $("#contenidodelcarrito").html(data);
                                          
                                        },
                                        error: function (jqXHR, textStatus, errorThrown,data)
                                        {
                                           //alert('error');
                                            console.log(jqXHR);
                                            console.log(textStatus);
                                            console.log(errorThrown);
                                        } 
                                });
                              }
                          });
      });
               
    })
</script>

