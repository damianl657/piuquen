<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>Nº Remito</th>
                          <th>Nº Pedido</th>
                          <th>Nº Ingreso</th>
                          <th>Fecha Remito</th>
                          <th>Usuario</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($listado as $fila):?>
                          <tr>
                                        <td><?=$fila->remito;?></td>
                                        <td><?=$fila->ingresos->id_pedido;?></td>
                                        <td><?=$fila->ingresos->id;?></td>
                                        <td><?php echo substr($fila->fecha,8,2).'/'.substr($fila->fecha,5,2).'/'.substr($fila->fecha,0,4); ?></td>
                                        <td><?=$fila->ingresos->users->last_name.' '.$fila->ingresos->users->first_name;?></td>
                                        <td>
                                          <center>
                                            <a class="btn btn-sm btn-primary" href="<?php echo site_url("Remitos/modificar/".$fila->id);?>" title="Editar" ><i class="glyphicon glyphicon-pencil"></i> </a>
                                            <a class="btn btn-sm btn-success" onclick="ver_detalles('<?php echo $fila->id;?>')" title="Ver Detalles"><i class="fa fa-list"></i> </a>
                                            
                                          </center>
                                          
                                        </td>
                          </tr>
                        <?endforeach;?>
                      </tbody>
                    </table>