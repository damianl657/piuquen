<table id="datatable-nodos2" class="table table-striped table-bordered dt-responsive nowrap">
                                  <tbody id="filas_eventos2">
                                      <tr>
                                        <th>Fecha de Factura: <?php echo substr($factura->fecha,8,2).'/'.substr($factura->fecha,5,2).'/'.substr($factura->fecha,0,4);?></th>
                                        <th>Sr.: <?php echo $factura->señor;?></th>
                                        <!--<th>Usuario: <?php echo $factura->users->last_name.' '.$factura->users->first_name;?></th>-->
                                        
                                      </tr>
                                      
                                  </tbody>
                                </table>
                                <label class="" id="">Remitos Facturados</label>
<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Fecha</th>
                          <th>Destino</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($factura->remitos_facturados as $fila):?>
                          <tr>
                                        
                                        <td><?=$fila->remitos->id;?></td>
                                        <td><?php echo substr($fila->remitos->fecha,8,2).'/'.substr($fila->remitos->fecha,5,2).'/'.substr($fila->remitos->fecha,0,4);?></td>
                                        <td><?php 
                                          if($fila->remitos->id_paciente != 0)
                                          {
                                            
                                               echo $fila->remitos->pacientes->apellido.' '.$fila->remitos->pacientes->nombre;
                                            
                                          }
                                          if ($fila->remitos->id_servicio != 0)
                                          {
                                            
                                               echo $fila->remitos->servicios->nombre;
                                            
                                          }  
                                        ?></td>
                          </tr>
                        <?endforeach;?>
                      </tbody>
                    </table>
                                 <label class="" id="">Productos</label>
                                
<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          
                          <th>Producto</th>
                          <th>Cantidad</th>
                          <th>Precio Unitario</th>
                          <th>Sub Total</th>
                          
                         
                          
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($factura->items_factura as $fila):?>
                          <tr>
                                        
                                        <td><?=$fila->productos->nombre;?></td>
                                        <td><?=$fila->cantidad;?></td>
                                        <td><?=$fila->precio_unitario;?></td>
                                        <td><?=$fila->sub_total;?></td>
                                        
                                        
                                        
                          </tr>
                        <?endforeach;?>
                      </tbody>
                    </table>

                    <table id="datatable-nodos2" class="table table-striped table-bordered dt-responsive nowrap">
                                  <tbody id="filas_eventos2">
                                      <tr>
                                        
                                        <th>Monto Total : <?php echo $factura->monto_total;?></th>
                                        
                                        
                                      </tr>
                                      
                                  </tbody>
                                </table>