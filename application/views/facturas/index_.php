<div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>
                     Facturas
                      
                  </h3>
              </div>

              
            </div>
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    
                    
                    <div id="loading"></div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" id="listado_clientes">
                    
                    
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        <div class="modal fade" id="detalle" role="dialog">
          <div class="modal-dialog modal-lg" role="document" id="mdialTamanio">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" ><label id="titulo_modal"></label></h4>
                 
              </div>
              <div class="modal-body" id="detalle_completo">
                
                <form action="#" id="form_rechazar" method="post" class="form-horizontal">
                  <input type="hidden" value="" name="eventosid22"/> 
                    <div class="form-group">
                      
                                <div class="x_content">
                    <div class="clearfix"></div>
                  </div>
                    </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
        
          <!-- Datatables -->
         
        <script >
          function visibilidad(id){
            
            var visible = 1;
            if($('input:checkbox[name=checkbox'+id+']:checked').val())
            {
              

              toastr.info('Agregando Visibilidad','Registrando. Por favor espere ...')

            }
            else
            {
              //alert("no seleccionado");
              toastr.info('Quitando Visibilidad','Registrando. Por favor espere ...')
              visible = 0;
            }
            updateVisibilidad(id,visible);
           
          }
          function updateVisibilidad(id,visible)
          {
            $.ajax
                    ({
                                          url: '<?php echo site_url("/Facturas/updateVisibilidad/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: {id:id, visible:visible},
                                          success: function(data)
                                          {
                                              if(visible==1 )
                                                toastr.success('Registrado','Factura Visible')
                                              else
                                                toastr.success('Registrado','Factura Oculto')
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                    });
          }
          traer_facturas();
          function traer_facturas()
          {
            //alert('hola');
            $.ajax
                ({
                  url: '<?php echo site_url("/facturas/listar_facturas/"); ?>',
                  dataType:'text',
                 // dataType: "JSON",
                  //type: 'POST',
                  //dataType: "JSON",
                  beforeSend: function() {
           // $('#loadingNiv').show()
                     $('#listado_clientes').append("<img  id='loading_scroll' src='<?=base_url('assets/loading.gif');?>' height='50' width='50' />");
                      //$('#loading_scroll').show();
                     
                  }, 
                  success: function(data)
                  {
                      //alert('entro');
                      $('#loading_scroll').remove();
                      //onsole.log(data);
                      $("#listado_clientes").append(data);
                      reload_datatable();
                            
                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                            //alert('error');
                            $('#loading_scroll').remove();
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            //console.log(data);

                  }
                });
          }
          
          function reload_datatable() 
          {
              if ($("#datatable-buttons").length) 
              {
                $("#datatable-buttons").DataTable({
                  responsive: true,
                  order: [ 1, 'asc' ] 

                });
            }
          }
          /*function baja_cliente()
          {
            alert('hola muenndo')
          }*/

        </script>
        
        <script>
          $(document).ready(function() {
            var handleDataTableButtons = function() {
              if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                  dom: "Bfrtip",
                  
                  responsive: true,
                  order: [ 1, 'asc' ] 

                });
              }
            };

            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons();
                }
              };
            }();

            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });

            $('#datatable-responsive').DataTable();

            $('#datatable-scroller').DataTable({
              ajax: "js/datatables/json/scroller-demo.json",
              deferRender: true,
              scrollY: 380,
              scrollCollapse: true,
              scroller: true
            });

            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });

            //TableManageButtons.init();
          });
        </script>
        <script type="text/javascript">
          $(document).ready(function()
            {
            $('#alta').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Ingresos/nuevo')?>/";

            });
          });    
        </script>
        <script >
          function ver_detalles_completo(id)
          {
            
              
              
             $.ajax({
            url: '<?php echo site_url("/Facturas/detalles_completos/"); ?>',
              dataType:'text',
                            type: 'get',
                            //dataType: "JSON",
            data :{id: id},
            success: function(data)
            {
              //console.log(data);
              $("#detalle_completo").empty();
              $("#detalle_completo").html(data);
              $("#titulo_modal").val('Detalles Completos');
              document.getElementById("titulo_modal").innerHTML = "Detalles Completos";
              
              $("#detalle").modal('show');            
              
              
              
            },
            error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }

            })//alert(id);
            
          }
        </script>
        <script >
          function ver_detalles_generales(id)
          {
            
              
              
             $.ajax({
            url: '<?php echo site_url("/Facturas/detalles_generales/"); ?>',
              dataType:'text',
                            type: 'get',
                            //dataType: "JSON",
            data :{id: id},
            success: function(data)
            {
              //console.log(data);
              $("#detalle_completo").empty();
              $("#detalle_completo").html(data);
              $("#titulo_modal").val('Detalles Genareles');
              document.getElementById("titulo_modal").innerHTML = "Detalles Generales";
              
              $("#detalle").modal('show');            
              
              
              
            },
            error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }

            })//alert(id);
            
          }
        </script>