<!-- Main Container -->
<main id="main-container">
<!-- Page Content -->
<div class="content">
    
    <div class="col-md-12 col-lg-12">
            <!-- Default Elements -->
            <div class="block block-themed">
                <div class="block-header bg-gd-aqua">
                    <h3 class="block-title"><a class="btn btn-secondary" href="javascript:window.history.back();"><i class="fa fa-arrow-left"></i></a>  </h3>
                    <h3 class="block-title">Facturas</h3>
                    <div class="block-options">
                       
                    </div>
                </div>
                <div id="loading"></div>
                <div class="block-content block-content-full" id="listado_clientes">
                    
                </div>
            </div>
            <!-- END Default Elements -->
    </div>
    
</div>
<!-- END Page Content -->
</main>
<!-- MODAL DETALLES-->
<div class="modal fade" id="detalle" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-popout" role="document" id="mdialTamanio">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title" id="titulo_modal"></h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                        <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content" id="detalle_completo">
                    
                </div>
                
                <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>       
            </div>
        </div>
    </div>
</div> 

<script >
    function visibilidad(id){
            
            var visible = 1;
            if($('input:checkbox[name=checkbox'+id+']:checked').val())
            {
              

              toastr.info('Agregando Visibilidad','Registrando. Por favor espere ...')

            }
            else
            {
              //alert("no seleccionado");
              toastr.info('Quitando Visibilidad','Registrando. Por favor espere ...')
              visible = 0;
            }
            updateVisibilidad(id,visible);
           
    }
    function updateVisibilidad(id,visible)
    {
            $.ajax
                    ({
                                          url: '<?php echo site_url("/Facturas/updateVisibilidad/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: {id:id, visible:visible},
                                          success: function(data)
                                          {
                                              if(visible==1 )
                                                toastr.success('Registrado','Factura Visible')
                                              else
                                                toastr.success('Registrado','Factura Oculto')
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                    });
    }
    traer_facturas();
    function traer_facturas()
    {
            //alert('hola');
            $.ajax
                ({
                  url: '<?php echo site_url("/facturas/listar_facturas/"); ?>',
                  dataType:'text',
                 // dataType: "JSON",
                  //type: 'POST',
                  //dataType: "JSON",
                  beforeSend: function() {
           // $('#loadingNiv').show()
                     $('#listado_clientes').append("<img  id='loading_scroll' src='<?=base_url('assets/loading.gif');?>' height='50' width='50' />");
                      //$('#loading_scroll').show();
                     
                  }, 
                  success: function(data)
                  {
                      //alert('entro');
                      $('#loading_scroll').remove();
                      //onsole.log(data);
                      $("#listado_clientes").append(data);
                      reload_datatable();
                            
                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                            //alert('error');
                            $('#loading_scroll').remove();
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            //console.log(data);

                  }
                });
    }
          
    function reload_datatable() 
          {
              if ($("#datatable-buttons").length) 
              {
                $("#datatable-buttons").DataTable({
                  responsive: true,
                  order: [ 1, 'asc' ] 

                });
            }
    }
         
    $(document).ready(function() {
            var handleDataTableButtons = function() {
              if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                  dom: "Bfrtip",
                  
                  responsive: true,
                  order: [ 1, 'asc' ] 

                });
              }
    };

            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons();
                }
              };
            }();

            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });

            $('#datatable-responsive').DataTable();

            $('#datatable-scroller').DataTable({
              ajax: "js/datatables/json/scroller-demo.json",
              deferRender: true,
              scrollY: 380,
              scrollCollapse: true,
              scroller: true
            });

            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });

            //TableManageButtons.init();
          });
        
          $(document).ready(function()
            {
            $('#alta').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Ingresos/nuevo')?>/";

            });
          });    
        
          function ver_detalles_completo(id)
          {
            
              
              
             $.ajax({
            url: '<?php echo site_url("/Facturas/detalles_completos/"); ?>',
              dataType:'text',
                            type: 'get',
                            //dataType: "JSON",
            data :{id: id},
            success: function(data)
            {
              //console.log(data);
              $("#detalle_completo").empty();
              $("#detalle_completo").html(data);
              $("#titulo_modal").val('Detalles Completos');
              document.getElementById("titulo_modal").innerHTML = "Detalles Completos";
              
              $("#detalle").modal('show');            
              
              
              
            },
            error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }

            })//alert(id);
            
          }
       
          function ver_detalles_generales(id)
          {
            
              
              
             $.ajax({
            url: '<?php echo site_url("/Facturas/detalles_generales/"); ?>',
              dataType:'text',
                            type: 'get',
                            //dataType: "JSON",
            data :{id: id},
            success: function(data)
            {
              //console.log(data);
              $("#detalle_completo").empty();
              $("#detalle_completo").html(data);
              $("#titulo_modal").val('Detalles Genareles');
              document.getElementById("titulo_modal").innerHTML = "Detalles Generales";
              
              $("#detalle").modal('show');            
              
              
              
            },
            error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }

            })//alert(id);
            
          }
</script>