<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          
                          <th>Producto</th>
                          <th>Lote</th>
                          <th>Vencimiento</th>
                          <th>Remitos</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($factura->items_detalle_factura as $fila):?>
                          <tr>
                                        
                                        <td><?=$fila->productos->nombre;?></td>
                                        <td><?=$fila->lotes->lote;?></td>
                                        <td><?php echo substr($fila->lotes->fecha_vencimiento,8,2).'/'.substr($fila->lotes->fecha_vencimiento,5,2).'/'.substr($fila->lotes->fecha_vencimiento,0,4); ?></td>
                                        <td><?=$fila->remitos_egresos;?></td>
                                        
                                        
                          </tr>
                        <?endforeach;?>
                      </tbody>
                    </table>