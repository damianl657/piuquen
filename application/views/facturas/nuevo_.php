<div class="">
    <div class="page-title">
      <div class="title_left">
       
        
      </div>
      <div class="clearfix"></div>
	  <div class="row">
	              <div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
	                  <div class="x_title">
	                    <h2>Productos a Facturar conforme a los Remitos seleccionados</h2>
	                    <ul class="nav navbar-right panel_toolbox">
	                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	                      </li>
	                      
	                      <li><a class="close-link"><i class="fa fa-close"></i></a>
	                      </li>
	                    </ul>
	                    <div class="clearfix"></div>
	                  </div>
	                  <div class="x_content">
	                    <br />
	                    <form id="alta" autocomplete="on" method="post" class="form-horizontal form-label-left">
                      <input type="hidden" id="producto_alta" name="producto_alta" value="nuevo" />
                      
                      <input type="hidden" id="id_contrato" name="id_contrato" value="<?php echo $id_contrato?>" />
                        
	                      <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="birthday" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" name="fecha">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" >Señor <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="señor" name="señor" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Direccion <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="direccion" name="direccion" class="form-control col-md-7 col-xs-12">
                          </div>
                      </div>
                      
                        <table id="datatable-buttons2" class="table table-striped table-bordered dt-responsive nowrap">
                          <thead>
                            <tr>
                              <th>Producto</th>
                              <th>Cantidad</th>
                              <th>Precio Unitario</th>
                              <th>Sub Total</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php 
                            $acum = 0;
                            $acum_total = 0;
                            foreach ($productos as $producto)
                            {
                              foreach ($this->cart->contents() as $item)
                              {
                                if($item['name'] == 'factura')
                                {
                                  $dato = $item['options'];
                                  $remito = Remitos_egresos_eloquent::find($dato['id_remito']);
                                  foreach ($remito->items_remito_egreso as $item_remito) 
                                  {
                                    if($item_remito->id_producto == $producto->id)
                                    {
                                      $acum = $acum + $item_remito->cantidad;
                                      
                                    }

                                  }
                                }
                              }
                              if($acum > 0)
                                    {
                                      $precio_unitario = Precios_eloquent::where('id_contrato',$this->input->post('id_contrato'))->where('id_producto',$producto->id)->get();
                                        $cont_pre = 0;
                                        foreach ($precio_unitario as $pre_unit) 
                                        {
                                           $cont_pre = $cont_pre + 1;
                                           $precio = $pre_unit;
                                        }
                                        $acum_total = $acum_total + ($acum * $precio->precio);
                                      ?>
                                      <tr>
                                        <td><?php echo $producto->nombre;?></td>
                                        <td><?php echo $acum;?></td>
                                        <td>$<?php echo $precio->precio;?></td>
                                        <td>$<?php echo ($acum * $precio->precio);?></td>
                                      </tr>
                                      <?php
                                    }
                              $acum =0 ;
                            }
                              
                          ?>
                          </tbody>
                        </table>
                        <table id="datatable-nodos2" class="table table-striped table-bordered dt-responsive nowrap">
                                  <tbody id="filas_eventos2">
                                      <tr>
                                        
                                        <th>Monto Total : <?php echo $acum_total;?></th>
                                        
                                        
                                      </tr>
                                      
                                  </tbody>
                                </table>
                      
                      </div>
                       
                        
	                      <div class="ln_solid"></div>
	                      <div class="form-group">
	                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" id="botones_accion">
	                          <a href="<?php echo site_url("Facturas/index/");?>" class="btn btn-primary">Cancelar</a>
	                          <button type="" id="guardar" class="btn btn-success">Guardar</button>
	                        </div>
	                      </div>

	                    </form>
	                  </div>
	                </div>
	              </div>
	  </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="cerrar2"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"></h4>
          </div>
          <div class="modal-body">
            <p id="mensaje"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
          </div>
         </div>
       </div>
    </div>
    <!-- Modal Error-->
    <div class="modal fade" id="myModal_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id=""><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel_Error"></h4>
          </div>
          <div class="modal-body">
            <p id="mensaje_error"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="">Cerrar</button>
          </div>
         </div>
       </div>
    </div>
   </div>
</div>
<script >
  $(document).ready(function() 
              {
                $('#cerrar').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Facturas/index')?>/";
                });
                $('#cerrar2').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Facturas/index')?>/";
                });
              })
</script>
   <script type="text/javascript">
              $(document).ready(function() 
              {   var dni_repetido = 0;
                  $('#guardar').click(function()
                  {   
                     
                          $('#alta').validate
                          ({
                              rules: {
                                  expediente: { required: true},
                                  resolucion: { required: true},
                                  
                              },
                              messages: {
                                  expediente:  {required: "Debe introducir el expediente."}, 
                                  resolucion:  {required: "Debe introducir el Resolucion."}, 
                                  
                              },
                              submitHandler: function(form)
                              {
                                  
                                  $("#guardar").hide();
                                  
                                  $('#botones_accion').append("<img  id='loading_scroll' src='<?=base_url('assets/loading.gif');?>' height='50' width='50' />");
                                  $.ajax
                                  ({
                                          url: '<?php echo site_url("/Facturas/guardar/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: $('#alta').serialize(),
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              console.log(data);
                                              $('#loading_scroll').remove();
                                              if (data.status)
                                              {
                                                  console.log(data);
                                                  //alert('El Cliente se registro en forma satifactoria');
                                                  document.getElementById("myModalLabel").innerHTML = 'Registro de Facturas';
                                                  document.getElementById("mensaje").innerHTML = 'La Factura se registro en forma satifactoria';
                                                  $("#myModal").modal('show');
                                                  jQuery.fn.reset = function () 
                                                  {
                                                      $(this).each (function() 
                                                          { this.reset(); 
                                                          });
                                                  }
                                                  $("#alta").reset();
                                                  //document.location.href = "<?php echo site_url('Login')?>/";
                                              }
                                              else
                                              {
                                                  //alert('El Cliente ya esta registrado');
                                                  document.getElementById("myModalLabel_Error").innerHTML = 'Error';
                                                  document.getElementById("mensaje_error").innerHTML = 'Error';
                                                  $("#myModal_error").modal('show');
                                              }
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                            alert('error LLAMAR DAMIAN');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                                  });
                              }
                          });
                  });
              });
              </script>
              <script type="text/javascript">
          $(document).ready(function()
            {
            $('#cancelar').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Facturas/remitos_no_facturados')?>/";

            });
          });    
        </script>
        <script>
      $(document).ready(function() {
        $('#birthday').daterangepicker({
          singleDatePicker: true,
          //calender_style: "picker_3",
        showDropdowns: true
          
        }, function(start, end, label) {
          console.log(start.toISOString(), end.format('YYYY-MM-DD'), label);
        });
        $('#birthday2').daterangepicker({
          singleDatePicker: true,
          //calender_style: "picker_3",
        showDropdowns: true
          
        }, function(start, end, label) {
          console.log(start.toISOString(), end.format('YYYY-MM-DD'), label);
        });
      });
    </script>
<script>
$(document).ready(function()
{
  /*$('#agregar_cuenta').click(function() 
  {
    alert('hola munedo');
  });*/
});      
      
</script>

