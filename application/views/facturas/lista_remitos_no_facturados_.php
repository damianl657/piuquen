<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>#</th>
                           <th>Remito</th>
                          <th>Fecha</th>
                          <th>Destino</th>
                          <th>Monto Total</th>
                          <th>Usuario</th>
                          <th>Seleccionar</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($listado as $fila):?>
                          <tr>
                          <td><?=$fila->id;?></td>
                                        <td><?=$fila->nro_remito;?><input type="hidden" id="id_checkbox<?php echo $fila->id;?>" name="id_checkbox<?php echo $fila->id;?>" value="<?=$fila->id;?>" /></td>
                                        <td><?php echo substr($fila->fecha,8,2).'/'.substr($fila->fecha,5,2).'/'.substr($fila->fecha,0,4); ?></td>
                                        <td>
                                        <?php 
                                          if($fila->id_paciente != 0)
                                          {
                                            
                                               echo $fila->pacientes->apellido.' '.$fila->pacientes->nombre;
                                            
                                          }
                                          if ($fila->id_servicio != 0)
                                          {
                                            
                                               echo $fila->servicios->nombre;
                                            
                                          }  
                                        ?>
                                        </td>
                                        <td>$<?=$fila->monto_total;?></td>
                                        <td><?=$fila->users->last_name.' '.$fila->users->first_name;;?></td>
                                        <td><input type="checkbox" name="checkbox<?php echo $fila->id;?>" id="checkbox<?php echo $fila->id;?>" /></td>
                                        <td>
                                          <center>
                                            <a class="btn btn-sm btn-primary" href="<?php echo site_url("Egresos_remito/modificar/".$fila->id);?>" title="Editar" ><i class="glyphicon glyphicon-pencil"></i> </a>
                                            <a class="btn btn-sm btn-success" onclick="ver_detalles('<?php echo $fila->id;?>')" title="Ver Detalles"><i class="fa fa-list"></i> </a>
                                            
                                          </center>
                                          
                                        </td>
                          </tr>
                        <?endforeach;?>
                      </tbody>
                    </table>
                    