<div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>
                     Remitos no Facturados
                      
                  </h3>
              </div>

              
            </div>
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    
                    
                    <div id="loading"></div>
                    <div class="clearfix"></div>
                  </div>
                  <form action="<?php echo site_url("Facturas/nuevo/");?>" method="post" accept-charset="utf-8">
                  <div class="form-group" id="group_prov">
                          <label for="basic" class="control-label col-md-3 col-sm-3 col-xs-12">Seleccionar Contrato</label>
                            <select id="id_contrato" class="selectpicker show-tick control-label col-md-9 col-sm-9 col-xs-12" data-live-search="true" required name="id_contrato">
                            <option value="<?php echo $actual->id;?>"><?php echo "ACTUALMENTE => Expediente: ".$actual->expediente." Resolucion: ".$actual->resolucion." Fecha: ".$actual->fecha; ?></option>
                              <?php foreach ( $contratos as $contrato) { ?>
                                    <option  value="<?php echo $contrato->id;?>"><?php 
                                    echo "Expediente: ".$contrato->expediente." Resolucion: ".$contrato->resolucion." Fecha: ".$contrato->fecha; ?></option>
                              <?php } ?>
                              
                            </select>
                        </div>  
                    
                  
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            
                            <button type="submit" id="guardar" class="btn btn-success">Realizar Factura</button>

                          </div>
                        </div> 
                        
                  
                  <div class="x_content" id="listado_clientes">
                    
                    
                  </div>
                  </form>
                </div>
              </div>

             
            </div>

          </div>
          <div class="modal fade" id="detalle" role="dialog">
          <div class="modal-dialog modal-lg" role="document" id="mdialTamanio">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="">Detalle</h4>
                 
              </div>
              <div class="modal-body">
                
                <form action="#" id="form_rechazar" method="post" class="form-horizontal">
                  <input type="hidden" value="" name="eventosid22"/> 
                    <div class="form-group" id="detalle_completo">
                     
                                 
                                <div class="x_content">
                    <div class="clearfix"></div>
                  </div>
                    </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="modal_baja_contrato" role="dialog">
          <div class="modal-dialog modal-lg" role="document" id="mdialTamanio">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="">Baja de Contrato</h4>
                 
              </div>
              <div class="modal-body">
                
                <form action="#" id="form_baja" method="post" class="form-horizontal">
                  <input type="hidden" value="" name="id_contrato" id ="id_contrato"/> 
                    <div class="form-group">
                      <label>¿Desea dar de baja el Contrato seleccionado?</label>
                      
                    </div>

                    <div class="clearfix"></div>

                  
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="baja_contrato">Aceptar</button>
              </div>
            </div>
          </div>
        </div>
          <!-- Datatables -->
          <script >
            function confirm_baja(id)
          {
            //alert(id);
             //document.getElementById("id_cliente").innerHTML = id;
             $("#id_contrato").val(id);
            $("#modal_baja_contrato").modal('show');
          }
          </script>
        <script >

          traer_remitos();
          function traer_remitos()
          {
            //alert('hola');
            $.ajax
                ({
                  url: '<?php echo site_url("/Facturas/listar_remitos_no_facturados/"); ?>',
                  dataType:'text',
                 // dataType: "JSON",
                  //type: 'POST',
                  //dataType: "JSON",
                  beforeSend: function() {
           // $('#loadingNiv').show()
                     $('#listado_clientes').append("<img  id='loading_scroll' src='<?=base_url('assets/loading.gif');?>' height='50' width='50' />");
                      //$('#loading_scroll').show();
                     
                  }, 
                  success: function(data)
                  {
                      //alert('entro');
                      $('#loading_scroll').remove();
                      //onsole.log(data);
                      $("#listado_clientes").append(data);
                      reload_datatable();
                            
                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                            //alert('error');
                            $('#loading_scroll').remove();
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            //console.log(data);

                  }
                });
          }
          
          function reload_datatable() 
          {
              if ($("#datatable-buttons").length) 
              {
                $("#datatable-buttons").DataTable({
                  responsive: true,
                  order: [ 0, 'DESC' ],
                  pagingType: "full_numbers",
                  lengthMenu: [ 10, 25, 50, 75, 100,200,300 ],
                  pageLength: 50


                });
            }
          }
          /*function baja_cliente()
          {
            alert('hola muenndo')
          }*/

        </script>
        <script >
          $(document).ready(function() 
              {
                $('#baja_contrato').click(function(event) {
                  //alert('hola mundo');
                  $.ajax
                    ({
                                          url: '<?php echo site_url("/Contratos/baja/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: $('#form_baja').serialize(),
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              //console.log(data);
                                              console.log('ok');
                                              
                                              $('#listado_clientes').empty();
                                              traer_contratos();
                                              
                                              
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                    });
                  
                });

              })
        </script>
        <script>
          $(document).ready(function() {
            var handleDataTableButtons = function() {
              if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                  dom: "Bfrtip",
                  
                  responsive: true,
                  order: [ 0, 'DESC' ]

                });
              }
            };

            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons();
                }
              };
            }();

            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });

            $('#datatable-responsive').DataTable();

            $('#datatable-scroller').DataTable({
              ajax: "js/datatables/json/scroller-demo.json",
              deferRender: true,
              scrollY: 380,
              scrollCollapse: true,
              scroller: true
            });

            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });

            //TableManageButtons.init();
          });
        </script>
        <script type="text/javascript">
          $(document).ready(function()
            {
            $('#alta').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Contratos/nuevo')?>/";

            });
          });    
        </script>
        <script >
          function ver_detalles(id)
          {
            
              
             $.ajax({
            url: '<?php echo site_url("/Egresos_remito/detalle/"); ?>',
              dataType:'text',
                            type: 'get',
                            //dataType: "JSON",
            data :{id: id},
            success: function(data)
            {
              console.log(data);
              $("#detalle_completo").empty();
              $("#detalle_completo").html(data);
              
              
              
              $("#detalle").modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }

            })//alert(id);
            
          }
        </script>