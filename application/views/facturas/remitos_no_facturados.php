<!-- Main Container -->
<main id="main-container">
    <!-- Page Content -->
    <div class="content">
        
        <div class="col-md-12 col-lg-12">
                <!-- Default Elements -->
                <div class="block block-themed">
                    <div class="block-header bg-gd-aqua">
                        <h3 class="block-title"><a class="btn btn-secondary" href="javascript:window.history.back();"><i class="fa fa-arrow-left"></i></a>  </h3>
                        <h3 class="block-title"> Remitos no Facturados</h3>
                        <div class="block-options">
                            
                        </div>
                    </div>
                    <div id="loading"></div>
                    <form id="alta" autocomplete="off" method="post" class="form-horizontal form-label-left" action="<?php echo site_url("Facturas/nuevo/");?>">
                    <div class="block-content block-content-full" id="">
                        <div class="row justify-content-center py-20">
                            
                              <div class="col-xl-12">
                                <input type="hidden" id="producto_alta" name="producto_alta" value="nuevo" />
                                <input type="hidden" id="id_user_group" name="id_user_group" value="0" />
                                <input type="hidden" id="user_id" name="user_id" value="0" />
                                
                                
                                
                                <div class="form-group">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="val-username">Seleccionar Contrato <span class="text-danger">*</span></label>
                                        <div class="col-lg-8">
                                            <select class="form-control" id="id_contrato" name="id_contrato">
                                                <?php 
                                                    foreach ( $contratos as $contrato)
                                                    {
                                                        if($actual->id == $contrato->id)
                                                          echo "<option selected value=".$contrato->id.">ACTUALMENTE => Expediente: ".$contrato->expediente." Resolucion: ".$contrato->resolucion." Fecha: ".$contrato->fecha."</option>";
                                                        else echo "<option value=".$contrato->id."> Expediente: ".$contrato->expediente." Resolucion: ".$contrato->resolucion." Fecha: ".$contrato->fecha."</option>";
                                                        
                                                    }
                                                ?>
                                                            
                                                            
                                            </select>
                                        </div>
                                    </div>
                                </div>
                               
                              <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                  <button id ="guardar" type="guardar" class="btn btn-alt-primary">Realizar Factura</button>
                                </div>
                              </div>
                    
                            
                          </div>
                    </div>
                    </form>
                    <div class="block-content block-content-full" id="listado_clientes">
                    </div>
                </div>
                <!-- END Default Elements -->
        </div>
        
    </div>
    <!-- END Page Content -->
    </main>
    <!-- MODAL DETALLES-->
    <div class="modal fade" id="detalle" role="dialog">
        <div class="modal-dialog modal-lg modal-dialog-popout" role="document" id="mdialTamanio">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title" id="">Detalle</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content" id="detalle_completo">
                        
                    </div>
                    
                    <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>       
                </div>
            </div>
        </div>
    </div> 
<script >
    function confirm_baja(id)
          {
            //alert(id);
             //document.getElementById("id_cliente").innerHTML = id;
             $("#id_contrato").val(id);
            $("#modal_baja_contrato").modal('show');
          }

          traer_remitos();
          function traer_remitos()
          {
            //alert('hola');
            $.ajax
                ({
                  url: '<?php echo site_url("/Facturas/listar_remitos_no_facturados/"); ?>',
                  dataType:'text',
                 // dataType: "JSON",
                  //type: 'POST',
                  //dataType: "JSON",
                  beforeSend: function() {
           // $('#loadingNiv').show()
                     $('#listado_clientes').append("<img  id='loading_scroll' src='<?=base_url('assets/loading.gif');?>' height='50' width='50' />");
                      //$('#loading_scroll').show();
                     
                  }, 
                  success: function(data)
                  {
                      //alert('entro');
                      $('#loading_scroll').remove();
                      //onsole.log(data);
                      $("#listado_clientes").append(data);
                      reload_datatable();
                            
                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                            //alert('error');
                            $('#loading_scroll').remove();
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            //console.log(data);

                  }
                });
          }
          
          function reload_datatable() 
          {
              if ($("#datatable-buttons").length) 
              {
                $("#datatable-buttons").DataTable({
                  responsive: true,
                  order: [ 0, 'DESC' ],
                  pagingType: "full_numbers",
                  lengthMenu: [ 10, 25, 50, 75, 100,200,300 ],
                  pageLength: 50


                });
            }
          }
          /*function baja_cliente()
          {
            alert('hola muenndo')
          }*/

        
          $(document).ready(function() 
              {
                $('#id_contrato').select2({});
                
                  
                

              })
        
          $(document).ready(function() {
            var handleDataTableButtons = function() {
              if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                  dom: "Bfrtip",
                  
                  responsive: true,
                  order: [ 0, 'DESC' ]

                });
              }
            };

            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons();
                }
              };
            }();

            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });

            $('#datatable-responsive').DataTable();

            $('#datatable-scroller').DataTable({
              ajax: "js/datatables/json/scroller-demo.json",
              deferRender: true,
              scrollY: 380,
              scrollCollapse: true,
              scroller: true
            });

            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });

            //TableManageButtons.init();
          });
       
         
       
          function ver_detalles(id)
          {
            
              
             $.ajax({
            url: '<?php echo site_url("/Egresos_remito/detalle/"); ?>',
              dataType:'text',
                            type: 'get',
                            //dataType: "JSON",
            data :{id: id},
            success: function(data)
            {
              console.log(data);
              $("#detalle_completo").empty();
              $("#detalle_completo").html(data);
              
              
              
              $("#detalle").modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }

            })//alert(id);
            
          }
</script>