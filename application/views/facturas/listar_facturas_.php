<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          
                          <th>Fecha</th>
                          <th>Señor</th>
                          <th>Direccion</th>
                          <th>Usuario</th>
                          <th>Monto Total</th>
                           <th>Visible</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($listado as $fila):?>
                          <tr>
                                        
                                        <td><?php //echo substr($fila->fecha,8,2).'/'.substr($fila->fecha,5,2).'/'.substr($fila->fecha,0,4); 
                                        echo $fila->fecha
                                        ?></td>
                                        <td><?=$fila->señor;?></td>
                                        <td><?=$fila->direccion;?></td>
                                        <td><?=$fila->users->last_name.' '.$fila->users->first_name;;?></td>
                                        <td><?=$fila->monto_total;?></td>
                                        <td><input class="visibilidad" onclick="visibilidad(<?php echo $fila->id;?>)" type="checkbox" title="VISIBLE EXTERNAMENTE" name="checkbox<?php echo $fila->id;?>" id="checkbox<?php echo $fila->id;?>" /></td>
                                        <td>
                                          <center>
                                             <a class="btn btn-sm btn-danger" href="<?php echo site_url("Reportes_excel/exportar_excel_detalles_completos/".$fila->id);?>" title="Reporte Remito Detalle" ><i class="fa fa-file-excel-o"></i> </a>

                                             <a class="btn btn-sm btn-danger" href="<?php echo site_url("Reportes_excel/exportar_excel_general/".$fila->id);?>" title="Reporte Remito General" ><i class="fa fa-file-excel-o"></i> </a>
                                             <a class="btn btn-sm btn-dark" href="<?php echo site_url("Reportes_excel/exportar_excel_detalles_completos_destinos/".$fila->id);?>" title="Reporte Remito Detalle por Paciente" ><i class="fa fa-file-excel-o"></i> </a>
                                            <a class="btn btn-sm btn-success" onclick="ver_detalles_generales('<?php echo $fila->id;?>')" title="Ver Detalles Completos"><i class="fa fa-list"></i> </a>
                                            <a class="btn btn-sm btn-info" onclick="ver_detalles_completo('<?php echo $fila->id;?>')" title="Ver Detalles Generales"><i class="fa fa-list"></i> </a>
                                          </center>
                                          
                                        </td>
                          </tr>
                        <?endforeach;?>
                      </tbody>
                    </table>
                    