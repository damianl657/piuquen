 <br>
  <br>
<h3>Producto: <?php 
                                    echo $producto->nombre; ?></h3>
                                    <br>
<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>Lote</th>
                          <th>Fecha de Vencimiento</th>
                           <th>Stock</th>
                          <th>Estado</th>
                          <!--<th>Acciones</th>-->
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($listado as $fila):?>
                          <tr>
                                        <td><?=$fila->lote;?></td>
                                        
                                        <td><?php echo substr($fila->fecha_vencimiento,8,2).'/'.substr($fila->fecha_vencimiento,5,2).'/'.substr($fila->fecha_vencimiento,0,4); ?></td>
                                        <td><?=$fila->cantidad;?></td>
                                        <td><?php 
                                        if($fila->estado == 1) 
                                        {
                                          echo "Activo";
                                        }
                                        else
                                        {
                                          echo "Inactivo";
                                        }
                                       ?>
                                          
                                        </td>
                                        <!--<td>
                                          <center>
                                            <a class="btn btn-sm btn-primary" href="<?php echo site_url("Precios/modificar/".$fila->id);?>" title="Editar" ><i class="glyphicon glyphicon-pencil"></i> </a>
                                            
                                          </center>
                                          
                                        </td>-->
                          </tr>
                        <?endforeach;?>
                      </tbody>
                    </table>