<!-- Main Container -->
<main id="main-container">
<!-- Page Content -->
<div class="content">
    
    <div class="col-md-12 col-lg-12">
            <!-- Default Elements -->
            <div class="block block-themed">
                <div class="block-header bg-gd-aqua">
                    <h3 class="block-title"><a class="btn btn-secondary" href="javascript:window.history.back();"><i class="fa fa-arrow-left"></i></a>  </h3>
                    <h3 class="block-title">Stock Total</h3>
                    <div class="block-options">
                       
                    </div>
                </div>
                <div id="loading"></div>
                <div class="block-content block-content-full" >
                    <form id="alta" autocomplete="on" method="post" class="form-horizontal form-label-left">
                    <div class="form-group">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="val-username">Seleccione el Producto <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <select class="js-select2 form-control" id="id_producto" name="id_producto">
                                    <option  value="ninguno">...</option>
                                    <?php 
                                        foreach($productos as $producto)
                                        {
                                            echo "<option value=".$producto->id.">".$producto->nombre."</option>";
                                        }
                                    ?>
                                                
                                                
                                </select>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="block-content block-content-full" id="precios_productos">
                    
                </div>
            </div>
            <!-- END Default Elements -->
    </div>
    
</div>
<!-- END Page Content -->
</main>
<script>
      var id_producto = $('select[name=id_producto]').val();
      if(id_producto !='ninguno')
        $.ajax
                                  ({
                                          url: '<?php echo site_url("/Productos/traer_lotes/"); ?>',
                                          dataType:'text',
                                          type: 'GET',
                                          
                                          data :{id_producto: id_producto},
                                          beforeSend: function() {
                                             $('#precios_productos').append("<img  id='loading_scroll' src='<?=base_url('assets/loading.gif');?>' height='50' width='50' />");
                                              //$('#loading_scroll').show();
                                             
                                          }, 
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              //console.log(data);
                                              $('#loading_scroll').remove();
                                            $("#precios_productos").html(data);
                                            $("#datatable-buttons").DataTable({
                                              responsive: true

                                            });
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                            
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              alert(textStatus);
                                              //console.log(data);

                                          }
                                  });
$(document).ready(function()
{
  /*$('#agregar_cuenta').click(function() 
  {
    alert('hola munedo');
  });*/
  $('#id_producto').select2({});
  $("#id_producto").change(function(event) 
  {
    //alert('hola');
    //console.log($('select[name=id_contrato]').val());
    var id_producto = $('select[name=id_producto]').val();
    $.ajax
                                  ({
                                          url: '<?php echo site_url("/Productos/traer_lotes/"); ?>',
                                          dataType:'text',
                                          type: 'GET',
                                          
                                          data :{id_producto: id_producto},
                                          beforeSend: function() {
           // $('#loadingNiv').show()
                                             $('#precios_productos').append("<img  id='loading_scroll' src='<?=base_url('assets/loading.gif');?>' height='50' width='50' />");
                                              //$('#loading_scroll').show();
                                             
                                          }, 
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              //console.log(data);
                                              $('#loading_scroll').remove();
                                            $("#precios_productos").html(data);
                                            $("#datatable-buttons").DataTable({
                                              responsive: true

                                            });
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                            
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              alert(textStatus);
                                              //console.log(data);

                                          }
                                  });


  });

});      
      
</script>
