<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          
                          <th>Nombre</th>
                          <th>Descripcion</th>
                          <th>Direccion</th>
                          
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($listado as $fila):?>
                          <tr>
                                        
                                        <td><?=$fila->nombre;?></td>
                                        <td><?=$fila->descripcion;?></td>
                                        <td><?=$fila->direccion;?></td>
                                        
                                        <td>
                                          <center>
                                            <a class="btn btn-sm btn-primary" href="<?php echo site_url("Pacientes/modificar/".$fila->id);?>" title="Editar" ><i class="glyphicon glyphicon-pencil"></i> </a>
                                            <a class="btn btn-sm btn-danger" onclick="confirm_baja('<?php echo $fila->id;?>')"  title="Eliminar"><i class="glyphicon glyphicon-trash"></i> </a>
                                            <a class="btn btn-sm btn-success" onclick="ver_detalles('<?php echo $fila->id;?>')" title="Ver Detalles"><i class="fa fa-list"></i> </a>
                                            
                                          </center>
                                          
                                        </td>
                          </tr>
                        <?endforeach;?>
                      </tbody>
                    </table>