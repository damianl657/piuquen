<div class="">
    <div class="page-title">
      <div class="title_left">
       
        
      </div>
      <div class="clearfix"></div>
	  <div class="row">
	              <div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
	                  <div class="x_title">
	                    <h2>Asignacion de Contrato</h2>
	                    <ul class="nav navbar-right panel_toolbox">
	                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	                      </li>
	                      
	                      <li><a class="close-link"><i class="fa fa-close"></i></a>
	                      </li>
	                    </ul>
	                    <div class="clearfix"></div>
	                  </div>
	                  <div class="x_content">
	                    <br />
	                    <form id="alta" autocomplete="on" method="post" class="form-horizontal form-label-left">
                      <input type="hidden" id="producto_alta" name="producto_alta" value="nuevo" />
                      <input type="hidden" id="id_user_group" name="id_user_group" value="0" />
                      <input type="hidden" id="user_id" name="user_id" value="0" />
                        
	                      <div class="form-group" id="group_prov">
                          <CENTER><h1 class=" col-md-9 col-sm-9 col-xs-12">Contrato Actual</h1></CENTER>
                          <h2 for="basic" class=" col-md-9 col-sm-9 col-xs-12"><?php echo "Expediente: ".$actual->expediente;?></h2>
                          <h2 for="basic" class=" col-md-9 col-sm-9 col-xs-12"><?php echo " Resolucion: ".$actual->resolucion;?></h2>
                          <h2 for="basic" class=" col-md-9 col-sm-9 col-xs-12"><?php echo " Fecha: ".$actual->fecha;?></h2>
                            
                        </div>
                        
                         <div class="form-group" id="group_prov">
                          <CENTER><h1 class=" col-md-9 col-sm-9 col-xs-12">Cambiar Contrato</h1></CENTER>
                            
                        </div>
                        <div class="form-group" id="group_prov">
                          <label for="basic" class="control-label col-md-3 col-sm-3 col-xs-12">Contratos Generados</label>
                            <select id="id_contrato" class="selectpicker show-tick control-label col-md-9 col-sm-9 col-xs-12" data-live-search="true" required name="id_contrato">
                            <option value="<?php echo $actual->id;?>"><?php echo "ACTUALMENTE => Expediente: ".$actual->expediente." Resolucion: ".$actual->resolucion." Fecha: ".$actual->fecha; ?></option>
                              <?php foreach ( $contratos as $contrato) { ?>
                                    <option  value="<?php echo $contrato->id;?>"><?php 
                                    echo "Expediente: ".$contrato->expediente." Resolucion: ".$contrato->resolucion." Fecha: ".$contrato->fecha; ?></option>
                              <?php } ?>
                              
                            </select>
                        </div>
                       
                        
	                      <div class="ln_solid"></div>
	                      <div class="form-group">
	                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	                          <a href="<?php echo site_url("Contratos/index/");?>" class="btn btn-primary">Cancelar</a>
	                          <button type="" id="guardar" class="btn btn-success">Guardar</button>
	                        </div>
	                      </div>

	                    </form>
	                  </div>
	                </div>
	              </div>
	  </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="cerrar2"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"></h4>
          </div>
          <div class="modal-body">
            <p id="mensaje"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
          </div>
         </div>
       </div>
    </div>
    <!-- Modal Error-->
    <div class="modal fade" id="myModal_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id=""><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel_Error"></h4>
          </div>
          <div class="modal-body">
            <p id="mensaje_error"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="">Cerrar</button>
          </div>
         </div>
       </div>
    </div>
   </div>
</div>
<script >
  $(document).ready(function() 
              {
                $('#cerrar').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Contratos')?>/";
                });
                $('#cerrar2').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Contratos')?>/";
                });
              })
</script>
   <script type="text/javascript">
              $(document).ready(function() 
              {   
                  $('#guardar').click(function()
                  {   
                    $('#alta').validate
                          ({
                              rules: {
                                  
                                  id_contrato: { required: true},
                                  
                              },
                              messages: {
                                  id_contrato:  {required: "Debe introducir el Contrato."}, 
                                  
                                  
                              },
                              submitHandler: function(form)
                              {
                     
                          
                                  $.ajax
                                  ({
                                          url: '<?php echo site_url("/Contratos/asignar_contrato_actual/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: $('#alta').serialize(),
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              console.log(data);
                                              if (data.status)
                                              {
                                                  console.log(data);
                                                  //alert('El Cliente se registro en forma satifactoria');
                                                  document.getElementById("myModalLabel").innerHTML = 'Seleccion de Contratos';
                                                  document.getElementById("mensaje").innerHTML = 'El Contrato se Selecciono en forma satifactoria';
                                                  $("#myModal").modal('show');
                                                  jQuery.fn.reset = function () 
                                                  {
                                                      $(this).each (function() 
                                                          { this.reset(); 
                                                          });
                                                  }
                                                  $("#alta").reset();
                                                  //document.location.href = "<?php echo site_url('Login')?>/";
                                              }
                                              else
                                              {
                                                  //alert('El Cliente ya esta registrado');
                                                  document.getElementById("myModalLabel_Error").innerHTML = 'Error';
                                                  document.getElementById("mensaje_error").innerHTML = 'Error';
                                                  $("#myModal_error").modal('show');
                                              }
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                            
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              alert(textStatus);
                                              //console.log(data);

                                          }
                                  });
                                }
                          })    
                  })
                          
                 
              });
              </script>
              <script type="text/javascript">
          $(document).ready(function()
            {
            $('#cancelar').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Contratos/index')?>/";

            });
          });    
        </script>
        
<script>
$(document).ready(function()
{
  /*$('#agregar_cuenta').click(function() 
  {
    alert('hola munedo');
  });*/
});      
      
</script>

