<!-- Main Container -->
<main id="main-container">
<!-- Page Content -->
<div class="content">
    
    <div class="col-md-12 col-lg-12">
            <!-- Default Elements -->
            <div class="block block-themed">
                <div class="block-header bg-gd-aqua">
                    <h3 class="block-title"><a class="btn btn-secondary" href="javascript:window.history.back();"><i class="fa fa-arrow-left"></i></a>  </h3>
                    <h3 class="block-title">Asignacion de Contrato</h3>
                    <div class="block-options">
                        
                    </div>
                </div>
                <div id="loading"></div>
                <div class="block-content block-content-full text-center bg-primary-lighter">
                <div class="form-group" id="group_prov">
                                <CENTER><h1 class=" col-md-9 col-sm-9 col-xs-12">Contrato Actual</h1></CENTER>
                                <h3 for="basic" class=" col-md-9 col-sm-9 col-xs-12"><?php echo "Expediente: ".$actual->expediente;?></h3>
                                <h3 for="basic" class=" col-md-9 col-sm-9 col-xs-12"><?php echo " Resolucion: ".$actual->resolucion;?></h3>
                                <h3 for="basic" class=" col-md-9 col-sm-9 col-xs-12"><?php echo " Institucion: ".$actual->institucion->nombre;?></h3>
                                <h3 for="basic" class=" col-md-9 col-sm-9 col-xs-12"><?php echo " Fecha: ".$actual->fecha;?></h3>
                            
                </div>
                </div>
                <div class="block-content block-content-full" id="listado_clientes">
                    <form id="alta" autocomplete="on" method="post" class="form-horizontal form-label-left">
                            
                            
                            <div class="form-group">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="val-username">Contratos Generados <span class="text-danger">*</span></label>
                                    <div class="col-lg-8">
                                        <select class="form-control" id="id_contrato" name="id_contrato">
                                            <?php 
                                                foreach($contratos as $contrato)
                                                {
                                                    if($contrato->id == $actual->id)
                                                        echo "<option selected value=".$contrato->id.">ACTUALMENTE => Expediente: ".$contrato->resolucion."Resolucion: ".$contrato->resolucion."</option>";
                                                    else  echo "<option value=".$contrato->id.">Expediente: ".$contrato->resolucion."Resolucion: ".$contrato->resolucion."</option>";
                                                
                                                }
                                            ?>
                                                        
                                                        
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-8 ml-auto">
                                <button id ="guardar" type="guardar" class="btn btn-alt-primary">Guardar</button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
            <!-- END Default Elements -->
    </div>
    
</div>
<!-- END Page Content -->
</main>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-popout" role="document">
        
      <div class="modal-content">
        <div class="block block-themed block-transparent mb-0">
          <div class="block-header bg-primary-dark">
            <h3 class="block-title" id="myModalLabel"></h3>
            <div class="block-options">
              <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
              <i class="si si-close"></i>
              </button>
            </div>
          </div>
          <div class="block-content">
                  <p id="mensaje"></p>
          </div>
        </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-alt-success" id="cerrar" data-dismiss="modal">
                    <i class="fa fa-check"></i> Cerrar
                </button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal Error-->
  <div class="modal fade" id="myModal_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="block block-themed block-transparent mb-0">
          <div class="block-header bg-primary-dark">
            <h3 class="block-title" id="myModalLabel_Error"></h3>
            <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
            </div>
          </div>
          <div class="block-content">
                  <p id="mensaje_error"></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-alt-success"  data-dismiss="modal">
          <i class="fa fa-check"></i> Cerrar
          </button>
        </div>
      </div>
    </div>
  </div>
<script >
  $(document).ready(function() 
              {
                $('#cerrar').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Contratos')?>/";
                });
                $('#cerrar2').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Contratos')?>/";
                });
              })
</script>
<script type="text/javascript">
              $(document).ready(function() 
              {   
                  $('#guardar').click(function()
                  {   
                    $('#alta').validate
                          ({
                              rules: {
                                  
                                  id_contrato: { required: true},
                                  
                              },
                              messages: {
                                  id_contrato:  {required: "Debe introducir el Contrato."}, 
                                  
                                  
                              },
                              submitHandler: function(form)
                              {
                     
                          
                                  $.ajax
                                  ({
                                          url: '<?php echo site_url("/Contratos/asignar_contrato_actual/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: $('#alta').serialize(),
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              console.log(data);
                                              if (data.status)
                                              {
                                                  console.log(data);
                                                  //alert('El Cliente se registro en forma satifactoria');
                                                  document.getElementById("myModalLabel").innerHTML = 'Seleccion de Contratos';
                                                  document.getElementById("mensaje").innerHTML = 'El Contrato se Selecciono en forma satifactoria';
                                                  $("#myModal").modal('show');
                                                  jQuery.fn.reset = function () 
                                                  {
                                                      $(this).each (function() 
                                                          { this.reset(); 
                                                          });
                                                  }
                                                  $("#alta").reset();
                                                  //document.location.href = "<?php echo site_url('Login')?>/";
                                              }
                                              else
                                              {
                                                  //alert('El Cliente ya esta registrado');
                                                  document.getElementById("myModalLabel_Error").innerHTML = 'Error';
                                                  document.getElementById("mensaje_error").innerHTML = 'Error';
                                                  $("#myModal_error").modal('show');
                                              }
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                            
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              alert(textStatus);
                                              //console.log(data);

                                          }
                                  });
                                }
                          })    
                  })
                          
                 
              });
              </script>
              <script type="text/javascript">
          $(document).ready(function()
            {
            $('#cancelar').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Contratos/index')?>/";

            });
          });    
</script>
