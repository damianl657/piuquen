<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>Resolucion</th>
                          <th>Expediente</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($listado as $fila):?>
                          <tr>
                                        <td><?php echo substr($fila->fecha,8,2).'/'.substr($fila->fecha,5,2).'/'.substr($fila->fecha,0,4); ?></td>
                                        <td><?=$fila->resolucion;?></td>
                                        <td><?=$fila->expediente;?></td>
                                        
                                        <td>
                                          <center>
                                            <a class="btn btn-sm btn-primary" href="<?php echo site_url("Contratos/modificar/".$fila->id);?>" title="Editar" ><i class="glyphicon glyphicon-pencil"></i> </a>
                                            <a class="btn btn-sm btn-danger" onclick="confirm_baja('<?php echo $fila->id;?>')"  title="Eliminar"><i class="glyphicon glyphicon-trash"></i> </a>
                                            
                                          </center>
                                          
                                        </td>
                          </tr>
                        <?endforeach;?>
                      </tbody>
                    </table>