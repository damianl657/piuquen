<!-- plantilla nueva-->
<!-- Main Container -->
<main id="main-container">
<!-- Page Content -->
<div class="content">
<!-- Default Elements -->
  <div class="block block-themed ">
    <div class="block-header bg-gd-sea">
      <h3 class="block-title"><a class="btn btn-secondary" href="javascript:window.history.back();"><i class="fa fa-arrow-left"></i></a>  </h3>
      <h3 class="block-title">Editar Contrato Seleccionado</h3>
      <div class="block-options">
      </div>
    </div>
  
    <div class="block-content block-content-full">
      <div class="row justify-content-center py-20">
        <form id="alta" autocomplete="off" method="post" class="form-horizontal form-label-left">
        <input type="hidden" name="id" value="<?=$contrato->id;?>" />
          <div class="col-xl-12">
            <input type="hidden" id="producto_alta" name="producto_alta" value="nuevo" />
            <input type="hidden" id="id_user_group" name="id_user_group" value="0" />
            <input type="hidden" id="user_id" name="user_id" value="0" />
            <div class="form-group">
              <div class="form-group row">
                <label class="col-lg-4 col-form-label" for="val-username">Expediente <span class="text-danger">*</span></label>
                <div class="col-lg-6">
                  <input type="text" class="form-control" id="expediente" name="expediente" value="<?php echo $contrato->expediente;?>" placeholder="Ingrese expediente">
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-group row">
                <label class="col-lg-4 col-form-label" for="val-username">Resolucion <span class="text-danger">*</span></label>
                <div class="col-lg-8">
                  <input type="text" class="form-control" id="resolucion" name="resolucion" value="<?php echo $contrato->resolucion;?>" placeholder="Ingrese resolucion">
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-group row">
                <label class="col-lg-4 col-form-label" for="val-username">Fecha <span class="text-danger">*</span></label>
                <div class="col-lg-8">
                  <input type="text" class="form-control datepicker" id="fecha" value="<?php echo date('d/m/Y', strtotime($contrato->fecha));?>" name="fecha" >
                </div>
              </div>
            </div>
            <div class="form-group">
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="val-username">Institucion <span class="text-danger">*</span></label>
                    <div class="col-lg-8">
                        <select class="form-control" id="id_institucion" name="id_institucion">
                            <?php 
                                foreach($instituciones as $institucion)
                                {
                                    if($institucion->id == $contrato->id_institucion)
                                        echo "<option selected value=".$institucion->id.">".$institucion->nombre."</option>";
                                    else echo "<option value=".$institucion->id.">".$institucion->nombre."</option>";
                                }
                            ?>
                                        
                                        
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
              <label class="col-12">¿Usa el nombre del Producto o Alias para remitos y Facturacion?</label>
              <?php
              $usaProducto = '';
              $usaAlias1 = '';
              $usaAlias2 = '';
              $usaAlias3 = '';
              if($contrato->usaProducto == 1) 
                $usaProducto = 'checked';
              if($contrato->usaAlias1 == 1) 
                $usaAlias1 = 'checked';
              if($contrato->usaAlias2 == 1) 
                $usaAlias2 = 'checked';
              if($contrato->usaAlias3 == 1) 
                $usaAlias3 = 'checked';
              ?>
              <div class="col-12">
                <div class="custom-control custom-radio custom-control-inline mb-5">
                  <input class="custom-control-input" type="radio" name="tipoProdAlias" id="radioProducto" value="1" <?php echo $usaProducto;?>>
                  <label class="custom-control-label" for="radioProducto">Producto</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline mb-5">
                  <input class="custom-control-input" type="radio" name="tipoProdAlias" id="radioAlias1" value="2" <?php echo $usaAlias1;?>>
                  <label class="custom-control-label" for="radioAlias1">Alias 1</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline mb-5">
                  <input class="custom-control-input" type="radio" name="tipoProdAlias" id="radioAlias2" value="3" <?php echo $usaAlias2;?>>
                  <label class="custom-control-label" for="radioAlias2">Alias 2</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline mb-5">
                  <input class="custom-control-input" type="radio" name="tipoProdAlias" id="radioAlias3" value="4" <?php echo $usaAlias3;?>>
                  <label class="custom-control-label" for="radioAlias3">Alias 3</label>
                </div>
              </div>
            </div>
            
          <div class="form-group row">
            <div class="col-lg-8 ml-auto">
              <a href="<?php echo site_url("Productos/index/");?>" class="btn btn-success">Cancelar</a>
              <button id ="guardar" type="guardar" class="btn btn-alt-primary">Guardar</button>
            </div>
          </div>

        </form>
      </div>
    </div><!-- END block-content block-content-full -->
  </div><!-- END block -->
</div><!-- END content -->          
</main>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-popout" role="document">
        
      <div class="modal-content">
        <div class="block block-themed block-transparent mb-0">
          <div class="block-header bg-primary-dark">
            <h3 class="block-title" id="myModalLabel"></h3>
            <div class="block-options">
              <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
              <i class="si si-close"></i>
              </button>
            </div>
          </div>
          <div class="block-content">
                  <p id="mensaje"></p>
          </div>
        </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-alt-success" id="cerrar" data-dismiss="modal">
                    <i class="fa fa-check"></i> Cerrar
                </button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal Error-->
  <div class="modal fade" id="myModal_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="block block-themed block-transparent mb-0">
          <div class="block-header bg-primary-dark">
            <h3 class="block-title" id="myModalLabel_Error"></h3>
            <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
            </div>
          </div>
          <div class="block-content">
                  <p id="mensaje_error"></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-alt-success"  data-dismiss="modal">
          <i class="fa fa-check"></i> Cerrar
          </button>
        </div>
      </div>
    </div>
  </div>
  <script >
  $(document).ready(function() 
              {
                $('#cerrar').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Contratos')?>/";
                });
                $('#cerrar2').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Contratos')?>/";
                });
              })
</script>
<script type="text/javascript">
             $(document).ready(function() 
              {
                  $('#guardar').click(function()
                  {    
                          $('#alta').validate
                          ({
                              rules: {
                                  expediente: { required: true},
                                  resolucion: { required: true},
                                  
                              },
                              messages: {
                                  expediente:  {required: "Debe introducir el expediente."}, 
                                  resolucion:  {required: "Debe introducir el Resolucion."}, 
                                  
                              },
                              submitHandler: function(form)
                              {
                                  $.ajax
                                  ({
                                          url: '<?php echo site_url("/Contratos/update/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: $('#alta').serialize(),
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              console.log(data);
                                              if (data.status)
                                              {
                                                  //console.log(data);
                                                  //alert('El Cliente se registro en forma satifactoria');
                                                  document.getElementById("myModalLabel").innerHTML = 'Contrato';
                                                  document.getElementById("mensaje").innerHTML = 'El Contrato se modificó en forma satifactoria';
                                                  $("#myModal").modal('show');
                                                  jQuery.fn.reset = function () 
                                                  {
                                                      $(this).each (function() 
                                                          { this.reset(); 
                                                          });
                                                  }
                                                  $("#alta").reset();
                                                  
                                                  //document.location.href = "<?php echo site_url('Login')?>/";
                                              }
                                              else
                                              {
                                                  //alert('El Cliente ya esta registrado');
                                                  document.getElementById("myModalLabel_Error").innerHTML = 'Error';
                                                  document.getElementById("mensaje_error").innerHTML = 'Error';
                                                  $("#myModal_error").modal('show');
                                                  
                                              }
                                              
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                                  });
                              }
                          });
                  });
              });
              </script>
              <script type="text/javascript">
          $(document).ready(function()
            {
            $('#cancelar').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Contratos/index')?>/";

            });
          });    
</script>
<script>
      $(document).ready(function() {
        
        $('#fecha').datepicker({
            format: 'dd/mm/yyyy',
            startDate: '-3d'
        });
      });
</script>
