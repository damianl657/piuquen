<div class="">
    <div class="page-title">
      <div class="title_left">
       
        
      </div>
      <div class="clearfix"></div>
	  <div class="row">
	              <div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
	                  <div class="x_title">
	                    <h2>Formulario de Paciente</h2>
	                    <ul class="nav navbar-right panel_toolbox">
	                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	                      </li>
	                      
	                      <li><a class="close-link"><i class="fa fa-close"></i></a>
	                      </li>
	                    </ul>
	                    <div class="clearfix"></div>
	                  </div>
	                  <div class="x_content">
	                    <br />
	                    <form id="alta" autocomplete="on" method="post" class="form-horizontal form-label-left">
                      <input type="hidden" id="producto_alta" name="producto_alta" value="nuevo" />
                      <input type="hidden" id="id_user_group" name="id_user_group" value="0" />
                      <input type="hidden" id="user_id" name="user_id" value="0" />
                        
	                      <div class="form-group">
	                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" >Nombre <span class="required">*</span>
	                        </label>
	                        <div class="col-md-6 col-sm-6 col-xs-12">
	                          <input type="text" id="nombre" name="nombre" class="form-control col-md-7 col-xs-12">
	                        </div>
	                      </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" >Apellido <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="apellido" name="apellido" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" >DNI <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="dni" name="dni" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" >Domicilio de Entrega <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="domicilio_entrega" name="domicilio_entrega" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" >CUIT <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="cuit" name="cuit" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" >Condicion de Venta <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="condicion_venta" name="condicion_venta" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" >IVA <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="iva" name="iva" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name" >Contacto <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="contacto" name="contacto" class="form-control col-md-7 col-xs-12">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="row">
                            <label for="basic" class="control-label col-md-3 col-sm-3 col-xs-12">Otros Contactos</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>

                                  <tr>
                                    
                                      <td>
                                        Nombre<input type="" id="nombre1" name="nombre1" class="form-control col-md-7 col-xs-12">
                                      </td>
                                      <td>
                                        Contacto<input type="" id="contacto1" name="contacto1" class="form-control col-md-7 col-xs-12">
                                      </td>
                                  </tr>
                                  <tr>   
                                      <td>
                                        Nombre<input type="" id="nombre2" name="nombre2" class="form-control col-md-7 col-xs-12">
                                      </td>
                                      <td>
                                        Contacto<input type="" id="contacto2" name="contacto2" class="form-control col-md-7 col-xs-12">
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                        Nombre<input type="" id="nombre3" name="nombre3" class="form-control col-md-7 col-xs-12">
                                      </td>
                                      <td>
                                        Contacto<input type="" id="contacto3" name="contacto3" class="form-control col-md-7 col-xs-12">
                                      </td>
                                    
                                   </tr>
                                   <tr>
                                      <td>
                                        Nombre<input type="" id="nombre4" name="nombre4" class="form-control col-md-7 col-xs-12">
                                      </td>
                                      <td>
                                        Contacto<input type="" id="contacto4" name="contacto4" class="form-control col-md-7 col-xs-12">
                                      </td>
                                    
                                   </tr>
                                   <tr>
                                      <td>
                                        Nombre<input type="" id="nombre5" name="nombre5" class="form-control col-md-7 col-xs-12">
                                      </td>
                                      <td>
                                        Contacto<input type="" id="contacto5" name="contacto5" class="form-control col-md-7 col-xs-12">
                                      </td>
                                    
                                   </tr>
                                    
                                  
                                </thead>
                              </table>
                            </div>
                          </div>
                            
                        </div>
	                      <div class="ln_solid"></div>
	                      <div class="form-group">
	                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	                          <a href="<?php echo site_url("Pacientes/index/");?>" class="btn btn-primary">Cancelar</a>
	                          <button type="" id="guardar" class="btn btn-success">Guardar</button>
	                        </div>
	                      </div>

	                    </form>
	                  </div>
	                </div>
	              </div>
	  </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="cerrar2"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"></h4>
          </div>
          <div class="modal-body">
            <p id="mensaje"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
          </div>
         </div>
       </div>
    </div>
    <!-- Modal Error-->
    <div class="modal fade" id="myModal_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id=""><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel_Error"></h4>
          </div>
          <div class="modal-body">
            <p id="mensaje_error"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="">Cerrar</button>
          </div>
         </div>
       </div>
    </div>
   </div>
</div>
<script >
  $(document).ready(function() 
              {
                $('#cerrar').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Pacientes')?>/";
                });
                $('#cerrar2').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Pacientes')?>/";
                });
              })
</script>
   <script type="text/javascript">
              $(document).ready(function() 
              {   var dni_repetido = 0;
                  $('#guardar').click(function()
                  {   
                     
                          $('#alta').validate
                          ({
                              rules: {
                                  nombre: { required: true},
                                  apellido: { required: true},
                                  dni: { required: true},
                                  
                              },
                              messages: {
                                  nombre:  {required: "Debe introducir el Nombre."}, 
                                  apellido:  {required: "Debe introducir el Apellido."}, 
                                  dni:  {required: "Debe introducir el DNI."}, 
                              },
                              submitHandler: function(form)
                              {
                                  $.ajax
                                  ({
                                          url: '<?php echo site_url("/Pacientes/guardar/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: $('#alta').serialize(),
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              console.log(data);
                                              if (data.status)
                                              {
                                                  console.log(data);
                                                  //alert('El Cliente se registro en forma satifactoria');
                                                  document.getElementById("myModalLabel").innerHTML = 'Registro de Pacientes';
                                                  document.getElementById("mensaje").innerHTML = 'El Paciente se registro en forma satifactoria';
                                                  $("#myModal").modal('show');
                                                  jQuery.fn.reset = function () 
                                                  {
                                                      $(this).each (function() 
                                                          { this.reset(); 
                                                          });
                                                  }
                                                  $("#alta").reset();
                                                  //document.location.href = "<?php echo site_url('Login')?>/";
                                              }
                                              else
                                              {
                                                  //alert('El Cliente ya esta registrado');
                                                  document.getElementById("myModalLabel_Error").innerHTML = 'Error';
                                                  document.getElementById("mensaje_error").innerHTML = 'Error';
                                                  $("#myModal_error").modal('show');
                                              }
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                            // alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                                  });
                              }
                          });
                  });
              });
              </script>
              <script type="text/javascript">
          $(document).ready(function()
            {
            $('#cancelar').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Pacientes/index')?>/";

            });
          });    
        </script>
        <script>
      $(document).ready(function() {
        $('#birthday').daterangepicker({
          singleDatePicker: true,
          //calender_style: "picker_3",
        showDropdowns: true
          
        }, function(start, end, label) {
          console.log(start.toISOString(), end.format('YYYY-MM-DD'), label);
        });
        $('#birthday2').daterangepicker({
          singleDatePicker: true,
          //calender_style: "picker_3",
        showDropdowns: true
          
        }, function(start, end, label) {
          console.log(start.toISOString(), end.format('YYYY-MM-DD'), label);
        });
      });
    </script>
<script>
$(document).ready(function()
{
  /*$('#agregar_cuenta').click(function() 
  {
    alert('hola munedo');
  });*/
});      
      
</script>

