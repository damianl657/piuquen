<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>DNI</th>
                          <th>Nombre</th>
                          <th>Apellido</th>
                          <th>Direccion Entrega</th>
                          <th>Contacto</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($listado as $fila):?>
                          <tr>
                                        <td><?=$fila->dni;?></td>
                                        <td><?=$fila->nombre;?></td>
                                        <td><?=$fila->apellido;?></td>
                                        <td><?=$fila->domicilio_entrega;?></td>
                                        <td><?=$fila->contacto;?></td>
                                        <td>
                                          <center>
                                            <a class="btn btn-sm btn-primary" href="<?php echo site_url("Pacientes/modificar/".$fila->id);?>" title="Editar" ><i class="si si-note"></i> </a>
                                            <a class="btn btn-sm btn-danger" onclick="confirm_baja('<?php echo $fila->id;?>')"  title="Eliminar"><i class="si si-trash"></i> </a>
                                            <a class="btn btn-sm btn-success" onclick="ver_detalles('<?php echo $fila->id;?>')" title="Ver Detalles"><i class="fa fa-list"></i> </a>
                                            
                                          </center>
                                          
                                        </td>
                          </tr>
                        <?endforeach;?>
                      </tbody>
                    </table>