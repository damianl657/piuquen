<!-- plantilla nueva-->
<!-- Main Container -->
<main id="main-container">
<!-- Page Content -->
<div class="content">
<!-- Default Elements -->
  <div class="block block-themed ">
    <div class="block-header bg-gd-sea">
      <h3 class="block-title"><a class="btn btn-secondary" href="javascript:window.history.back();"><i class="fa fa-arrow-left"></i></a>  </h3>
      <h3 class="block-title">Editar Producto Seleccionado</h3>
      <div class="block-options">
      </div>
  </div>
  <div class="block-content block-content-full">
    <div class="row justify-content-center py-20">
      <form id="alta" autocomplete="on" method="post" class="form-horizontal form-label-left">
      <input type="hidden" name="id" value="<?=$lote->id;?>" />
        <div class="col-xl-12">
          <div class="form-group">
            <div class="form-group row">
              <label class="col-lg-4 col-form-label" for="val-username">Produco <span class="text-danger">*</span></label>
              <div class="col-lg-8">
                <input type="text" class="form-control" id="nombre" disabled name="nombre" value="<?php echo $lote->productos->nombre;?>" placeholder="Ingrese nombre del producto">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-group row">
              <label class="col-lg-4 col-form-label" for="val-username">Lote <span class="text-danger">*</span></label>
              <div class="col-lg-8">
                <input type="text" class="form-control" id="lote" name="lote" disabled value="<?php echo $lote->lote;?>" placeholder="Ingrese Descripcion">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-group row">
              <label class="col-lg-4 col-form-label" for="val-username">Fecha de Vencimiento "Dia/Mes/Año" <span class="text-danger">*</span></label>
              <div class="col-lg-8">
                <input type="text" class="form-control" id="fecha_vencimiento" name="fecha_vencimiento" value ="<?php echo substr($lote->fecha_vencimiento,8,2).'/'.substr($lote->fecha_vencimiento,5,2).'/'.substr($lote->fecha_vencimiento,0,4); ?>" placeholder="Ingrese nombre del Alias">
              </div>
            </div>
          </div>
          
          
        </div>
        <div class="form-group row">
          <div class="col-lg-8 ml-auto">
            <a href="<?php echo site_url("Productos/index/");?>" class="btn btn-success">Cancelar</a>
            <button id ="guardar" type="guardar" class="btn btn-alt-primary">Guardar</button>
          </div>
        </div>
      </form>
    </div>
  </div>  

</div>
</main >
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-dialog-popout" role="document">
        
       <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title" id="myModalLabel"></h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                  <p id="mensaje"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-success" id="cerrar" data-dismiss="modal">
                    <i class="fa fa-check"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
  <!-- Modal Error-->
<div class="modal fade" id="myModal_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title" id="myModalLabel_Error"></h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                  <p id="mensaje_error"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-success"  data-dismiss="modal">
                    <i class="fa fa-check"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
<script >
  $(document).ready(function() 
              {
                $('#cerrar').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Lotes/lotes')?>/";
                });
                $('#cerrar2').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Lotes/lotes')?>/";
                });
                $('#fecha_vencimiento').datepicker({
                    format: 'dd/mm/yyyy',
                    //startDate: '-3d'
                });
              })

              $(document).ready(function() 
              {
                  $('#guardar').click(function()
                  {    
                          $('#alta').validate
                          ({
                              rules: {
                                fecha_vencimiento: { required: true},
                                  
                              },
                              fecha_vencimiento: {
                                birthday2:  {required: "Debe introducir Vencimiento."}, 
                                  
                              },
                              submitHandler: function(form)
                              {
                                  $.ajax
                                  ({
                                          url: '<?php echo site_url("/Lotes/registrarEditVencimiento/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: $('#alta').serialize(),
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              console.log(data);
                                              if (data.status)
                                              {
                                                  //console.log(data);
                                                  //alert('El Cliente se registro en forma satifactoria');
                                                  document.getElementById("myModalLabel").innerHTML = 'Lote';
                                                  document.getElementById("mensaje").innerHTML = 'El lote se modificó en forma satifactoria';
                                                  $("#myModal").modal('show');
                                                  jQuery.fn.reset = function () 
                                                  {
                                                      $(this).each (function() 
                                                          { this.reset(); 
                                                          });
                                                  }
                                                  $("#alta").reset();
                                                  
                                                  //document.location.href = "<?php echo site_url('Login')?>/";
                                              }
                                              else
                                              {
                                                  //alert('El Cliente ya esta registrado');
                                                  document.getElementById("myModalLabel_Error").innerHTML = 'Error';
                                                  document.getElementById("mensaje_error").innerHTML = 'Error';
                                                  $("#myModal_error").modal('show');
                                                  
                                              }
                                              
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                                  });
                              }
                          });
                  });
              });
              
          $(document).ready(function()
            {
            $('#cancelar').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Lotes/lotes')?>/";

            });
          });    
</script>
       