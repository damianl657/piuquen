<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>Lote</th>
                          <th>Producto</th>
                          <th>Cantidad</th>
                          <th>Vencimiento</th>
                           <th>Estado</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($listado as $fila):?>
                          <tr>
                                        <td><?=$fila->lote;?></td>
                                        <td><?=$fila->productos->nombre;?></td>
                                        <td><?=$fila->cantidad;?></td>
                                        <td><?php 
                                        if($fila->productos->con_vencimiento == 1) 
                                        {
                                          echo substr($fila->fecha_vencimiento,8,2).'/'.substr($fila->fecha_vencimiento,5,2).'/'.substr($fila->fecha_vencimiento,0,4);
                                        }
                                        else
                                        {
                                          echo "-";
                                        }
                                       ?>
                                        <td><?php 
                                        if($fila->estado == 1) 
                                        {
                                          echo "Activo";
                                        }
                                        else
                                        {
                                          echo "Inactivo";
                                        }
                                       ?>
                                          
                                        </td>
                                        <td>
                                          <center>
                                            <a class="btn btn-sm btn-primary" href="<?php echo site_url("Lotes/editVencimiento/".$fila->id);?>" title="Editar Vencimiento" ><i class="si si-note"></i> </a>
                                          </center>
                                          
                                        </td>
                          </tr>
                        <?endforeach;?>
                      </tbody>
                    </table>