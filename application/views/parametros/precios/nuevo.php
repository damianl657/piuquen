<!-- plantilla nueva-->
<!-- Main Container -->
<main id="main-container">
<!-- Page Content -->
<div class="content">
<!-- Default Elements -->
  <div class="block block-themed ">
    <div class="block-header bg-gd-sea">
      <h3 class="block-title"><a class="btn btn-secondary" href="javascript:window.history.back();"><i class="fa fa-arrow-left"></i></a>  </h3>
      <h3 class="block-title">Seleccione el Contrato y a continuacion el Producto</h3>
      <div class="block-options">
      </div>
    </div>
  
    <div class="block-content block-content-full">
      <div class="row justify-content-center py-20">
        <form id="alta" autocomplete="off" method="post" class="form-horizontal form-label-left">
          
          <div class="col-xl-12">
        
            <input type="hidden" id="user_id" name="user_id" value="0" />
           
            
            
            <div class="form-group">
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="val-username">Contrato <span class="text-danger">*</span></label>
                    <div class="col-lg-8">
                        <select class="js-select2 form-control" id="id_contrato" name="id_contrato">
                            <?php foreach ( $contratos as $contrato) { ?>
                                    <option  value="<?php echo $contrato->id;?>"><?php 
                                    echo "Expediente: ".$contrato->expediente." Resolucion: ".$contrato->resolucion." Fecha: ".$contrato->fecha; ?></option>
                              <?php } ?>
                                        
                                        
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="val-username">Producto <span class="text-danger">*</span></label>
                    <div class="col-lg-8">
                        <select class="js-select2 form-control" id="id_producto" name="id_producto">
                            <?php foreach ( $productos as $producto) { ?>
                                    <option  value="<?php echo $producto->id;?>"><?php 
                                    echo $producto->nombre; ?></option>
                              <?php } ?>
                                        
                                        
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
              <div class="form-group row">
                <label class="col-lg-4 col-form-label" for="val-username">Precio <span class="text-danger">*</span></label>
                <div class="col-lg-8">
                  <input type="text" class="form-control" id="precio" name="precio" >
                </div>
              </div>
            </div>
         
            
          <div class="form-group row">
            <div class="col-lg-8 ml-auto">
              <a href="<?php echo site_url("Precios/index/");?>" class="btn btn-success">Cancelar</a>
              <button id ="guardar" type="guardar" class="btn btn-alt-primary">Guardar</button>
            </div>
          </div>

        </form>
      </div>
    </div><!-- END block-content block-content-full -->
  </div><!-- END block -->
</div><!-- END content -->          
</main>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-popout" role="document">
        
      <div class="modal-content">
        <div class="block block-themed block-transparent mb-0">
          <div class="block-header bg-primary-dark">
            <h3 class="block-title" id="myModalLabel"></h3>
            <div class="block-options">
              <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
              <i class="si si-close"></i>
              </button>
            </div>
          </div>
          <div class="block-content">
                  <p id="mensaje"></p>
          </div>
        </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-alt-success" id="cerrar" data-dismiss="modal">
                    <i class="fa fa-check"></i> Cerrar
                </button>
        </div>
      </div>
    </div>
</div>
 <!-- Modal Error-->
 <div class="modal fade" id="myModal_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="block block-themed block-transparent mb-0">
          <div class="block-header bg-primary-dark">
            <h3 class="block-title" id="myModalLabel_Error"></h3>
            <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
            </div>
          </div>
          <div class="block-content">
                  <p id="mensaje_error"></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-alt-success"  data-dismiss="modal">
          <i class="fa fa-check"></i> Cerrar
          </button>
        </div>
      </div>
    </div>
</div>
<script >
  $(document).ready(function() 
              {
                $('#cerrar').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Precios')?>/";
                });
                $('#cerrar2').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Precios')?>/";
                });
              })

              $(document).ready(function() 
              {   
                  $('#guardar').click(function()
                  {   
                    $('#alta').validate
                          ({
                              rules: {
                                  
                                  id_contrato: { required: true},
                                  precio: { required: true, number: true},
                                  
                              },
                              messages: {
                                  id_contrato:  {required: "Debe introducir el Contrato."},
                                  precio: "Debe introducir un  precio válido.", 
                                  
                                  
                              },
                              submitHandler: function(form)
                              {
                     
                          
                                  $.ajax
                                  ({
                                          url: '<?php echo site_url("/Precios/guardar/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: $('#alta').serialize(),
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              console.log(data);
                                              if (data.status)
                                              {
                                                  console.log(data);
                                                  //alert('El Cliente se registro en forma satifactoria');
                                                  document.getElementById("myModalLabel").innerHTML = 'Asignación de Precios';
                                                  document.getElementById("mensaje").innerHTML = 'El Precio se Guardo en forma satifactoria';
                                                  $("#myModal").modal('show');
                                                  jQuery.fn.reset = function () 
                                                  {
                                                      $(this).each (function() 
                                                          { this.reset(); 
                                                          });
                                                  }
                                                  $("#alta").reset();
                                                  //document.location.href = "<?php echo site_url('Login')?>/";
                                              }
                                              else
                                              {
                                                  //alert('El Cliente ya esta registrado');
                                                  document.getElementById("myModalLabel_Error").innerHTML = 'Error';
                                                  document.getElementById("mensaje_error").innerHTML = 'El Precio ya esta Registrado en el Mismo Contrato';
                                                  $("#myModal_error").modal('show');
                                              }
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                            
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              alert(textStatus);
                                              //console.log(data);

                                          }
                                  });
                                }
                          })    
                  })
                          
                 
              });
              </script>
              <script type="text/javascript">
          $(document).ready(function()
            {
            $('#cancelar').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Contratos/index')?>/";

            });
          });    
        </script>
        
<script>
$(document).ready(function()
{
  /*$('#agregar_cuenta').click(function() 
  {
    alert('hola munedo');
  });*/
});      
      
</script>
