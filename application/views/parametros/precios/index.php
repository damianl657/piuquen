<!-- Main Container -->
<main id="main-container">
<!-- Page Content -->
<div class="content">
    
    <div class="col-md-12 col-lg-12">
            <!-- Default Elements -->
            <div class="block block-themed">
                <div class="block-header bg-gd-aqua">
                    <h3 class="block-title"><a class="btn btn-secondary" href="javascript:window.history.back();"><i class="fa fa-arrow-left"></i></a>  </h3>
                    <h3 class="block-title">Stock Total</h3>
                    <div class="block-options">
                       
                    </div>
                </div>
                <div id="loading"></div>
                <div class="block-content block-content-full" >
                    <form id="alta" autocomplete="on" method="post" class="form-horizontal form-label-left">
                    <div class="form-group">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label" for="val-username">Seleccione el Contrato <span class="text-danger">*</span></label>
                            <div class="col-lg-8">
                                <select class="js-select2 form-control" id="id_contrato" name="id_contrato">
                                        <?php foreach ( $contratos as $contrato) { ?>
                                            <option  value="<?php echo $contrato->id;?>"><?php 
                                            echo "Expediente: ".$contrato->expediente." Resolucion: ".$contrato->resolucion." Fecha: ".$contrato->fecha; ?></option>
                                    <?php } ?>

                                                
                                                
                                </select>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="block-content block-content-full" id="precios_productos">
                    
                </div>
            </div>
            <!-- END Default Elements -->
    </div>
    
</div>
<!-- END Page Content -->
</main>
<script >
  $(document).ready(function() 
              {
                $('#cerrar').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Precios')?>/";
                });
                $('#cerrar2').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Precios')?>/";
                });
              })
</script>
   <script type="text/javascript">
              
              </script>
              <script type="text/javascript">
          $(document).ready(function()
            {
            $('#cancelar').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Contratos/index')?>/";

            });
          });    
        </script>
<script>
      var id_contrato = $('select[name=id_contrato]').val();
      $.ajax
                                  ({
                                          url: '<?php echo site_url("/Precios/traer_productos/"); ?>',
                                          dataType:'text',
                                          type: 'GET',
                                          
                                          data :{id_contrato: id_contrato},
                                          beforeSend: function() {
           // $('#loadingNiv').show()
                                             $('#precios_productos').append("<img  id='loading_scroll' src='<?=base_url('assets/loading.gif');?>' height='50' width='50' />");
                                              //$('#loading_scroll').show();
                                             
                                          }, 
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              //console.log(data);
                                              $('#loading_scroll').remove();
                                            $("#precios_productos").html(data);
                                            $("#datatable-buttons").DataTable({
                                              responsive: true

                                            });
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                            
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              alert(textStatus);
                                              //console.log(data);

                                          }
                                  });


</script>
        
<script>

$(document).ready(function()
{
  /*$('#agregar_cuenta').click(function() 
  {
    alert('hola munedo');
  });*/
  $('#id_contrato').select2({});
  $("#id_contrato").change(function(event) 
  {
    //alert('hola');
    //console.log($('select[name=id_contrato]').val());
    var id_contrato = $('select[name=id_contrato]').val();
    $.ajax
                                  ({
                                          url: '<?php echo site_url("/Precios/traer_productos/"); ?>',
                                          dataType:'text',
                                          type: 'GET',
                                          
                                          data :{id_contrato: id_contrato},
                                          beforeSend: function() {
           // $('#loadingNiv').show()
                                             $('#precios_productos').append("<img  id='loading_scroll' src='<?=base_url('assets/loading.gif');?>' height='50' width='50' />");
                                              //$('#loading_scroll').show();
                                             
                                          }, 
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              //console.log(data);
                                              $('#loading_scroll').remove();
                                            $("#precios_productos").html(data);
                                            $("#datatable-buttons").DataTable({
                                              responsive: true

                                            });
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                            
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              alert(textStatus);
                                              //console.log(data);

                                          }
                                  });


  });

});      
      
</script>

