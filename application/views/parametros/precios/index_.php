<div class="">
    <div class="page-title">
      <div class="title_left">
       
        
      </div>
      <div class="clearfix"></div>
	  <div class="row">
	              <div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
	                  <div class="x_title">
	                    <h2>Seleccione el Contrato</h2>
	                    <ul class="nav navbar-right panel_toolbox">
	                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	                      </li>
	                      
	                      <li><a class="close-link"><i class="fa fa-close"></i></a>
	                      </li>
	                    </ul>
	                    <div class="clearfix"></div>
	                  </div>
	                  <div class="x_content">
	                    <br />
	                    <form id="alta" autocomplete="on" method="post" class="form-horizontal form-label-left">
                      <input type="hidden" id="producto_alta" name="producto_alta" value="nuevo" />
                      <input type="hidden" id="id_user_group" name="id_user_group" value="0" />
                      <input type="hidden" id="user_id" name="user_id" value="0" />
                        
	                      
                        <div class="form-group" id="group_prov">
                          <label for="basic" class="control-label col-md-3 col-sm-3 col-xs-12">Seleccione el Contrato</label>
                            <select id="id_contrato" class="selectpicker show-tick control-label col-md-9 col-sm-9 col-xs-12" data-live-search="true" required name="id_contrato">
                            
                              <?php foreach ( $contratos as $contrato) { ?>
                                    <option  value="<?php echo $contrato->id;?>"><?php 
                                    echo "Expediente: ".$contrato->expediente." Resolucion: ".$contrato->resolucion." Fecha: ".$contrato->fecha; ?></option>
                              <?php } ?>
                              
                            </select>
                        </div>
                        <div id="precios_productos">
                          
                        </div>
                        
	                      <div class="ln_solid"></div>
	                     

	                    </form>
	                  </div>
	                </div>
	              </div>
	  </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="cerrar2"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"></h4>
          </div>
          <div class="modal-body">
            <p id="mensaje"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
          </div>
         </div>
       </div>
    </div>
    <!-- Modal Error-->
    <div class="modal fade" id="myModal_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id=""><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel_Error"></h4>
          </div>
          <div class="modal-body">
            <p id="mensaje_error"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="">Cerrar</button>
          </div>
         </div>
       </div>
    </div>
   </div>
</div>
<script >
  $(document).ready(function() 
              {
                $('#cerrar').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Precios')?>/";
                });
                $('#cerrar2').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Precios')?>/";
                });
              })
</script>
   <script type="text/javascript">
              
              </script>
              <script type="text/javascript">
          $(document).ready(function()
            {
            $('#cancelar').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Contratos/index')?>/";

            });
          });    
        </script>
<script>
      var id_contrato = $('select[name=id_contrato]').val();
      $.ajax
                                  ({
                                          url: '<?php echo site_url("/Precios/traer_productos/"); ?>',
                                          dataType:'text',
                                          type: 'GET',
                                          
                                          data :{id_contrato: id_contrato},
                                          beforeSend: function() {
           // $('#loadingNiv').show()
                                             $('#precios_productos').append("<img  id='loading_scroll' src='<?=base_url('assets/loading.gif');?>' height='50' width='50' />");
                                              //$('#loading_scroll').show();
                                             
                                          }, 
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              //console.log(data);
                                              $('#loading_scroll').remove();
                                            $("#precios_productos").html(data);
                                            $("#datatable-buttons").DataTable({
                                              responsive: true

                                            });
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                            
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              alert(textStatus);
                                              //console.log(data);

                                          }
                                  });


</script>
        
<script>

$(document).ready(function()
{
  /*$('#agregar_cuenta').click(function() 
  {
    alert('hola munedo');
  });*/
  $("#id_contrato").change(function(event) 
  {
    //alert('hola');
    //console.log($('select[name=id_contrato]').val());
    var id_contrato = $('select[name=id_contrato]').val();
    $.ajax
                                  ({
                                          url: '<?php echo site_url("/Precios/traer_productos/"); ?>',
                                          dataType:'text',
                                          type: 'GET',
                                          
                                          data :{id_contrato: id_contrato},
                                          beforeSend: function() {
           // $('#loadingNiv').show()
                                             $('#precios_productos').append("<img  id='loading_scroll' src='<?=base_url('assets/loading.gif');?>' height='50' width='50' />");
                                              //$('#loading_scroll').show();
                                             
                                          }, 
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              //console.log(data);
                                              $('#loading_scroll').remove();
                                            $("#precios_productos").html(data);
                                            $("#datatable-buttons").DataTable({
                                              responsive: true

                                            });
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                            
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              alert(textStatus);
                                              //console.log(data);

                                          }
                                  });


  });

});      
      
</script>

