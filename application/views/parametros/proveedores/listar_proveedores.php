<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>Nombre</th>
                          <th>CUIT/CUIL</th>
                          <th>Demora Estimada</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($listado as $fila):?>
                          <tr>
                                        <td><?=$fila->nombre;?></td>
                                        <td><?=$fila->cuit;?></td>
                                        <td><?=$fila->demora;?></td>
                                        <td>
                                          <center>
                                            <a class="btn btn-sm btn-primary" href="<?php echo site_url("Proveedores/modificar/".$fila->id);?>" title="Editar" ><i class="glyphicon glyphicon-pencil"></i> </a>
                                            <a class="btn btn-sm btn-danger" onclick="confirm_baja('<?php echo $fila->id;?>')"  title="Eliminar"><i class="glyphicon glyphicon-trash"></i> </a>
                                            
                                          </center>
                                          
                                        </td>
                          </tr>
                        <?endforeach;?>
                      </tbody>
                    </table>