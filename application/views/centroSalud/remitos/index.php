<!-- Modal-->
<div class="modal fade" id="detalle" role="dialog">
          <div class="modal-dialog modal-lg" role="document" id="mdialTamanio">
            <div class="modal-content">
              <div class="modal-header">
                 <h4 class="modal-title" id="">Detalle Remito</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               
                 
              </div>
              <div class="modal-body">
                
                <form action="#" id="form_rechazar" method="post" class="form-horizontal">
                  <input type="hidden" value="" name="eventosid22"/> 
                    <div class="form-group" id="detalle_completo">
                      
                                <div class="x_content">
                    <div class="clearfix"></div>
                  </div>
                    </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
</div>
<!-- Main Container -->
<main id="main-container">
<!-- Page Content -->
<div class="content">
    
    <h2 class="content-heading">Remitos Generados</h2>
    <div class="col-md-12 col-lg-12">
            <!-- Default Elements -->
            <div class="block ">
                <div class="block-header block-header-default">
                    <h3 class="block-title"><a class="btn btn-secondary" href="javascript:window.history.back();"><i class="fa fa-arrow-left"></i></a>  </h3>
                    <h3 class="block-title">Lista de Remitos</h3>
                    <div class="block-options">
                        
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <table id="example1" class="table table-bordered table-striped table-vcenter js-dataTable-full" style="width: 100%" >
                        <thead>
                            <tr>
                                <th class="d-none d-sm-table-cell">Remito</th>
                                <th class="text-center" >Fecha</th>
                                <th class="text-center" >Destino</th>
                                <th class="d-none d-sm-table-cell" >Monto Total</th>
                               
                                <th class="text-center" >Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END Default Elements -->
    </div>
    
</div>
<!-- END Page Content -->
</main>
<!-- END Main Container -->
<script>
	function notificacion(icono,titulo){
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
          });
          Toast.fire({
                icon: icono,
                title: titulo
              });
        }
  function ver_detalles(id)
  {
            
              
             $.ajax({
            url: '<?php echo site_url("/Egresos_remito/detalle/"); ?>',
              dataType:'text',
                            type: 'get',
                            //dataType: "JSON",
            data :{id: id},
            success: function(data)
            {
              console.log(data);
              $("#detalle_completo").empty();
              $("#detalle_completo").html(data);
              
              
              
              $("#detalle").modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }

            })//alert(id);
            
    }  
    $(document).ready(function(){
        $('#example1').DataTable({
          'processing': true,
          'serverSide': true,
          'serverMethod': 'post',
          'columnDefs'  : [
            { 
              "targets": [-1], //last column
              "orderable": false, //set not orderable
            },],
          'ajax': {
             
             'url': '<?php echo site_url("/centroSalud/remitos/listar"); ?>'
          },
          'columns': [
             { data: 'nro_remito' },
             { data: 'fecha' },
             { data: 'id_paciente' },
             { data: 'monto_total' },
             { data: 'acciones' },
          ]
        });
     });

  
</script>