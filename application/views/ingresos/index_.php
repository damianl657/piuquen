<div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>
                     Ingresos
                      
                  </h3>
              </div>

              
            </div>
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    
                    
                    <div id="loading"></div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" id="listado_clientes">
                    
                    
                  </div>
                </div>
              </div>

              
            </div>
          </div>
          <div class="modal fade" id="detalle" role="dialog">
          <div class="modal-dialog modal-lg" role="document" id="mdialTamanio">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="">Detalle</h4>
                 
              </div>
              <div class="modal-body">
                
                <form action="#" id="form_rechazar" method="post" class="form-horizontal">
                  <input type="hidden" value="" name="eventosid22"/> 
                    <div class="form-group">
                      <table id="datatable-nodos2" class="table table-striped table-bordered dt-responsive nowrap">
                                  <tbody id="filas_eventos2">
                                      <tr>
                                        <th>Nombre: <label class="" id="nombre"> </label></th>
                                        <th>CUIT/CUIT: <label class="" id="cuit"> </label></th>
                                      </tr>
                                      
                                  </tbody>
                                </table>
                                <div class="x_content">
                    <div class="clearfix"></div>
                  </div>
                    </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="modal_baja_ingreso" role="dialog">
          <div class="modal-dialog modal-lg" role="document" id="mdialTamanio">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="">Baja de Ingreso</h4>
                 
              </div>
              <div class="modal-body">
                
                <form action="#" id="form_baja" method="post" class="form-horizontal">
                  <input type="hidden" value="" name="id_ingreso" id ="id_ingreso"/> 
                    <div class="form-group">
                      <label>¿Desea dar de baja el ingreso seleccionado?</label>
                      
                    </div>

                    <div class="clearfix"></div>

                  
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="baja_ingreso">Aceptar</button>
              </div>
            </div>
          </div>
        </div>
          <!-- Datatables -->
          <script >
            function confirm_baja(id)
          {
            //alert(id);
             //document.getElementById("id_cliente").innerHTML = id;
             $("#id_ingreso").val(id);
            $("#modal_baja_ingreso").modal('show');
          }
          </script>
        <script >

          traer_ingresos();
          function traer_ingresos()
          {
            //alert('hola');
            $.ajax
                ({
                  url: '<?php echo site_url("/Ingresos/listar_ingresos/"); ?>',
                  dataType:'text',
                 // dataType: "JSON",
                  //type: 'POST',
                  //dataType: "JSON",
                  beforeSend: function() {
           // $('#loadingNiv').show()
                     $('#listado_clientes').append("<img  id='loading_scroll' src='<?=base_url('assets/loading.gif');?>' height='50' width='50' />");
                      //$('#loading_scroll').show();
                     
                  }, 
                  success: function(data)
                  {
                      //alert('entro');
                      $('#loading_scroll').remove();
                      //onsole.log(data);
                      $("#listado_clientes").append(data);
                      reload_datatable();
                            
                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                            //alert('error');
                            $('#loading_scroll').remove();
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            //console.log(data);

                  }
                });
          }
          
          function reload_datatable() 
          {
              if ($("#datatable-buttons").length) 
              {
                $("#datatable-buttons").DataTable({
                  responsive: true

                });
            }
          }
          /*function baja_cliente()
          {
            alert('hola muenndo')
          }*/

        </script>
        <script >
          $(document).ready(function() 
              {
                $('#baja_ingreso').click(function(event) {
                  //alert('hola mundo');
                  $.ajax
                    ({
                                          url: '<?php echo site_url("/Ingresos/baja/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: $('#form_baja').serialize(),
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              //console.log(data);
                                              console.log('ok');
                                              
                                              $('#listado_clientes').empty();
                                              traer_ingresos();
                                              
                                              
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                    });
                  
                });

              })
        </script>
        <script>
          $(document).ready(function() {
            var handleDataTableButtons = function() {
              if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                  dom: "Bfrtip",
                  
                  responsive: true

                });
              }
            };

            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons();
                }
              };
            }();

            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });

            $('#datatable-responsive').DataTable();

            $('#datatable-scroller').DataTable({
              ajax: "js/datatables/json/scroller-demo.json",
              deferRender: true,
              scrollY: 380,
              scrollCollapse: true,
              scroller: true
            });

            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });

            //TableManageButtons.init();
          });
        </script>
        <script type="text/javascript">
          $(document).ready(function()
            {
            $('#alta').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Ingresos/nuevo')?>/";

            });
          });    
        </script>
        <script >
          function ver_detalles(id)
          {
            document.getElementById("nombre").innerHTML = "-";
             document.getElementById("cuit").innerHTML = "-";
              
              
             $.ajax({
            url: '<?php echo site_url("/Ingresos/detalle/"); ?>',
              dataType:'text',
                            type: 'get',
                            dataType: "JSON",
            data :{id: id},
            success: function(data)
            {
              console.log(data);
              document.getElementById("nombre").innerHTML = data.proveedor['nombre'];
              document.getElementById("descripcion").innerHTML = data.proveedor['descripcion'];
              
              
              
              $("#detalle").modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }

            })//alert(id);
            
          }
        </script>