<!-- Main Container -->
<main id="main-container">
<!-- Page Content -->
<div class="content">
    
    <div class="col-md-12 col-lg-12">
            <!-- Default Elements -->
            <div class="block block-themed">
                <div class="block-header bg-gd-aqua">
                    <h3 class="block-title"><a class="btn btn-secondary" href="javascript:window.history.back();"><i class="fa fa-arrow-left"></i></a>  </h3>
                    <h3 class="block-title">Ingresos</h3>
                    <div class="block-options">
                        
                    </div>
                </div>
                <div id="loading"></div>
                <div class="block-content block-content-full" id="listado_clientes">
                    
                </div>
            </div>
            <!-- END Default Elements -->
    </div>
    
</div>
<script >
    function confirm_baja(id)
          {
            //alert(id);
             //document.getElementById("id_cliente").innerHTML = id;
             $("#id_ingreso").val(id);
            $("#modal_baja_ingreso").modal('show');
          }
         

          traer_ingresos();
          function traer_ingresos()
          {
            //alert('hola');
            $.ajax
                ({
                  url: '<?php echo site_url("/Ingresos/listar_ingresos/"); ?>',
                  dataType:'text',
                 // dataType: "JSON",
                  //type: 'POST',
                  //dataType: "JSON",
                  beforeSend: function() {
           // $('#loadingNiv').show()
                     $('#listado_clientes').append("<img  id='loading_scroll' src='<?=base_url('assets/loading.gif');?>' height='50' width='50' />");
                      //$('#loading_scroll').show();
                     
                  }, 
                  success: function(data)
                  {
                      //alert('entro');
                      $('#loading_scroll').remove();
                      //onsole.log(data);
                      $("#listado_clientes").append(data);
                      reload_datatable();
                            
                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                            //alert('error');
                            $('#loading_scroll').remove();
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            //console.log(data);

                  }
                });
          }
          
          function reload_datatable() 
          {
              if ($("#datatable-buttons").length) 
              {
                $("#datatable-buttons").DataTable({
                  responsive: true

                });
            }
          }
          /*function baja_cliente()
          {
            alert('hola muenndo')
          }*/

       
          $(document).ready(function() 
              {
                $('#baja_ingreso').click(function(event) {
                  //alert('hola mundo');
                  $.ajax
                    ({
                                          url: '<?php echo site_url("/Ingresos/baja/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: $('#form_baja').serialize(),
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              //console.log(data);
                                              console.log('ok');
                                              
                                              $('#listado_clientes').empty();
                                              traer_ingresos();
                                              
                                              
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                    });
                  
                });

              })
        
          $(document).ready(function() {
            var handleDataTableButtons = function() {
              if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                  dom: "Bfrtip",
                  
                  responsive: true

                });
              }
            };

            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons();
                }
              };
            }();

            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });

            $('#datatable-responsive').DataTable();

            $('#datatable-scroller').DataTable({
              ajax: "js/datatables/json/scroller-demo.json",
              deferRender: true,
              scrollY: 380,
              scrollCollapse: true,
              scroller: true
            });

            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });

            //TableManageButtons.init();
          });
       
          $(document).ready(function()
            {
            $('#alta').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Ingresos/nuevo')?>/";

            });
          });    
      
          function ver_detalles(id)
          {
            document.getElementById("nombre").innerHTML = "-";
             document.getElementById("cuit").innerHTML = "-";
              
              
             $.ajax({
            url: '<?php echo site_url("/Ingresos/detalle/"); ?>',
              dataType:'text',
                            type: 'get',
                            dataType: "JSON",
            data :{id: id},
            success: function(data)
            {
              console.log(data);
              document.getElementById("nombre").innerHTML = data.proveedor['nombre'];
              document.getElementById("descripcion").innerHTML = data.proveedor['descripcion'];
              
              
              
              $("#detalle").modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }

            })//alert(id);
            
          }
</script>