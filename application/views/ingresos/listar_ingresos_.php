<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>Factura</th>
                          <th>Monto Total</th>
                          <th>Nº Pedido</th>
                          <th>Proveedor</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($listado as $fila):?>
                          <tr>
                                        <td><?php echo substr($fila->fecha,8,2).'/'.substr($fila->fecha,5,2).'/'.substr($fila->fecha,0,4); ?></td>
                                        <td><?=$fila->factura;?></td>
                                        <td><?=$fila->monto_total;?></td>
                                        <td><?=$fila->id_pedido;?></td>
                                        <td><?=$fila->proveedores->nombre;?></td>
                                        <td>
                                          <center>
                                            <a class="btn btn-sm btn-primary" href="<?php echo site_url("Ingresos/modificar/".$fila->id);?>" title="Editar" ><i class="glyphicon glyphicon-pencil"></i> </a>
                                            <a class="btn btn-sm btn-info"  title="Ver Remitos" href="<?php echo site_url("Remitos/index/".$fila->id);?>"><i class="fa fa-file-text"></i> </a>
                                            
                                          </center>
                                          
                                        </td>
                          </tr>
                        <?endforeach;?>
                      </tbody>
                    </table>