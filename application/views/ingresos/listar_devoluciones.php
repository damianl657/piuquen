<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>Remito</th>
                          <th>Fecha</th>
                          <th>Producto</th>
                          <th>Lote</th>
                          <th>Cantidad</th>
                          <th>Motivo</th>
                          <th>Usuario</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($listado as $fila):?>
                          <tr>
                                        <td><?=$fila->id_remito_egreso;?></td>
                                        <td><?php echo substr($fila->fecha,8,2).'/'.substr($fila->fecha,5,2).'/'.substr($fila->fecha,0,4); ?></td>
                                        <td><?=$fila->lotes->productos->nombre;?></td>
                                        <td><?=$fila->lotes->lote;?></td>
                                        <td><?=$fila->cantidad;?></td>
                                        <td><?=$fila->motivo;?></td>
                                        <td><?=$fila->users->last_name.' '.$fila->users->first_name;?></td>
                                        
                          </tr>
                        <?endforeach;?>
                      </tbody>
                    </table>