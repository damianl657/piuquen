<!-- Main Container -->
<main id="main-container">
<!-- Page Content -->
<div class="content">
    
    <div class="col-md-12 col-lg-12">
            <!-- Default Elements -->
            <div class="block block-themed">
                <div class="block-header bg-gd-aqua">
                    <h3 class="block-title"><a class="btn btn-secondary" href="javascript:window.history.back();"><i class="fa fa-arrow-left"></i></a>  </h3>
                    <h3 class="block-title">Seleccione el Pedido para registrar el Ingreso</h3>
                    <div class="block-options">
                        
                    </div>
                </div>
                <div id="loading"></div>
                <div class="block-content block-content-full" id="listado_clientes">
                    
                </div>
            </div>
            <!-- END Default Elements -->
    </div>
    
</div>
<!-- MODAL DETALLES-->
<div class="modal fade" id="detalle" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-popout" role="document" id="mdialTamanio">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title" id="">Detalle</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                        <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <table id="datatable-nodos2" class="table table-striped table-bordered dt-responsive nowrap">
                                  <tbody id="filas_eventos2">
                                      <tr>
                                        <th>Fecha de Pedido: <label class="" id="fecha"> </label></th>
                                        <th>Fecha de llegada: <label class="" id="fecha_llegada"> </label></th>
                                        <th>Proveedor: <label class="" id="proveedor"> </label></th>
                                      </tr>
                                      
                                  </tbody>
                                </table>
                                 <label class="" id="">Productos Pedidos</label>
                                <table id="table" class="table table-striped table-bordered dt-responsive nowrap ">
                                    <thead>
                                        <tr>
                                          <th>Producto</th>
                                          <th>Cantidad</th>
                                          
                                        </tr>
                                      </thead>
                                      <tbody id="items_pedidos">
                                    </tbody>
                              </table>
                
                </div>
                <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>       
            </div>
        </div>
    </div>
</div> 
<script >
        function confirm_baja(id)
          {
            //alert(id);
             //document.getElementById("id_cliente").innerHTML = id;
             $("#id_ingreso").val(id);
            $("#modal_baja_ingreso").modal('show');
          }
         

          traer_pedidos();
          function traer_pedidos()
          {
            //alert('hola');
            $.ajax
                ({
                  url: '<?php echo site_url("/Ingresos/listar_pedidos/"); ?>',
                  dataType:'text',
                 // dataType: "JSON",
                  //type: 'POST',
                  //dataType: "JSON",
                  beforeSend: function() {
           // $('#loadingNiv').show()
                     $('#listado_clientes').append("<img  id='loading_scroll' src='<?=base_url('assets/loading.gif');?>' height='50' width='50' />");
                      //$('#loading_scroll').show();
                     
                  }, 
                  success: function(data)
                  {
                      //alert('entro');
                      $('#loading_scroll').remove();
                      //onsole.log(data);
                      $("#listado_clientes").append(data);
                      reload_datatable();
                            
                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                            //alert('error');
                            $('#loading_scroll').remove();
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(errorThrown);
                            //console.log(data);

                  }
                });
          }
          
          function reload_datatable() 
          {
              if ($("#datatable-buttons").length) 
              {
                $("#datatable-buttons").DataTable({
                  responsive: true

                });
            }
          }
          /*function baja_cliente()
          {
            alert('hola muenndo')
          }*/

        
          $(document).ready(function() {
            var handleDataTableButtons = function() {
              if ($("#datatable-buttons").length) {
                $("#datatable-buttons").DataTable({
                  dom: "Bfrtip",
                  
                  responsive: true

                });
              }
            };

            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons();
                }
              };
            }();

            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });

            $('#datatable-responsive').DataTable();

            $('#datatable-scroller').DataTable({
              ajax: "js/datatables/json/scroller-demo.json",
              deferRender: true,
              scrollY: 380,
              scrollCollapse: true,
              scroller: true
            });

            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });

           //TableManageButtons.init();
          });
        
          function ver_detalles(id)
          {
            document.getElementById("fecha").innerHTML = "-";
            document.getElementById("fecha_llegada").innerHTML = "-";
            document.getElementById("proveedor").innerHTML = "-";
            $("#items_pedidos").empty();
              
              
            $.ajax({
            url: '<?php echo site_url("/Pedidos/detalle/"); ?>',
              dataType:'text',
                            type: 'get',
                            dataType: "JSON",
            data :{id: id},
            success: function(data)
            {
              console.log(data);
              var fecha = data.pedido['fecha'];
              fecha = fecha.substr(8,2)+'/'+fecha.substr(5,2)+'/'+fecha.substr(0,4);
              var fecha_llegada = data.pedido['fecha_llegada'];
              fecha_llegada = fecha_llegada.substr(8,2)+'/'+fecha_llegada.substr(5,2)+'/'+fecha_llegada.substr(0,4);
              document.getElementById("fecha").innerHTML = fecha;
              document.getElementById("fecha_llegada").innerHTML = fecha_llegada;
              document.getElementById("proveedor").innerHTML = data.proveedor['nombre'];
              var fila = '';
              if(data.items_pedidos.length > 0)
              {
                //alert(data.items_pedidos.length);
                for(var i=0; i < data.items_pedidos.length ; i++)
                {
                  var producto = '';
                  var id_prod = data.items_pedidos[i].id_producto;
                  for(var k=0; k < data.productos.length ; k++)
                          {
                            if(data.productos[k].id == id_prod)
                            {
                              producto = data.productos[k].nombre;
                            }
                          }
                  var fila = '<tr>'+'<td>'+producto+'</td>'+'<td>'+data.items_pedidos[i].cantidad+'</td>'+'</tr>';
                    $("#items_pedidos").append(fila); 
                }
              }
              
              
              $("#detalle").modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }

            })//alert(id);
            
          }
</script>