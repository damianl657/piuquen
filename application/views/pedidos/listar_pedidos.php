<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                      <thead>
                        <tr>
                          <th>Nº Pedido</th>
                          <th>Fecha de Pedido</th>
                          <th>Fecha de LLegada</th>
                          <th>Proveedor</th>
                          <th>Usuario</th>
                          <th>Estado</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?foreach($listado as $fila):?>
                          <tr>
                                        <td><?=$fila->id;?></td>
                                        <td><?php echo substr($fila->fecha,8,2).'/'.substr($fila->fecha,5,2).'/'.substr($fila->fecha,0,4); ?></td>
                                        <td><?php echo substr($fila->fecha_llegada,8,2).'/'.substr($fila->fecha_llegada,5,2).'/'.substr($fila->fecha_llegada,0,4); ?></td>
                                        
                                        <td><?=$fila->Proveedores->nombre;?></td>
                                        <td><?=$fila->users->last_name.' '.$fila->users->first_name;?></td>
                                        <td><?=$fila->estado_pedido;?></td>
                                        <td>
                                          <center>
                                            <a class="btn btn-sm btn-primary" href="<?php echo site_url("Pedidos/modificar/".$fila->id);?>" title="Editar" ><i class="glyphicon glyphicon-pencil"></i> </a>
                                            <a class="btn btn-sm btn-danger" onclick="confirm_baja('<?php echo $fila->id;?>')"  title="Eliminar"><i class="glyphicon glyphicon-trash"></i> </a>
                                            <a class="btn btn-sm btn-success" onclick="ver_detalles('<?php echo $fila->id;?>')" title="Ver Detalles"><i class="fa fa-list"></i> </a>
                                          </center>
                                          
                                        </td>
                          </tr>
                        <?endforeach;?>
                      </tbody>
                    </table>