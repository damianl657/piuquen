<div class="">
    <div class="page-title">
      <div class="title_left">
       
        
      </div>
      <div class="clearfix"></div>
	  <div class="row">
	              <div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
	                  <div class="x_title">
	                    <h2>Formulario de Pedidos</h2>
	                    <ul class="nav navbar-right panel_toolbox">
	                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	                      </li>
	                      <?php //var_dump($this->session->userdata('user_id')); ?>
	                      <li><a class="close-link"><i class="fa fa-close"></i></a>
	                      </li>
	                    </ul>
	                    <div class="clearfix"></div>
	                  </div>
	                  <div class="x_content">
	                    <br />
	                    <form id="alta" autocomplete="on" method="post" class="form-horizontal form-label-left">
                      <input type="hidden" id="producto_alta" name="producto_alta" value="nuevo" />
                      <input type="hidden" id="id_user_group" name="id_user_group" value="0" />
                      <input type="hidden" id="user_id" name="user_id" value="0" />
                        
	                    <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="birthday" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" name="fecha">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de Llegada <span class="required">*</span>
                          </label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input id="birthday2" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" name="fecha_llegada">
                          </div>
                      </div>

                        
                        <div class="form-group" id="group_prov">
                          <label for="basic" class="control-label col-md-3 col-sm-3 col-xs-12">Proveedor</label>
                            <select id="id_proveedor" class="selectpicker show-tick control-label col-md-3 col-sm-3 col-xs-12" data-live-search="true" required name="id_proveedor">
                              <?php foreach ( $proveedores as $proveedor) { ?>
                                    <option  value="<?php echo $proveedor->id;?>"><?php echo $proveedor->nombre; ?></option>
                              <?php } ?>
                              
                            </select>
                        </div>
                        <div class="form-group">
                          <div class="row">
                            <label for="basic" class="control-label col-md-3 col-sm-3 col-xs-12">Productos a Pedir</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>

                                  
                                  <tr>   
                                      <td>
                                          Producto
                                          <select id="id_producto" class="selectpicker show-tick control-label " data-live-search="true" required name="id_producto">
                                           
                                              <?php foreach ( $productos as $producto) { ?>
                                                    <option  value="<?php echo $producto->id;?>"><?php echo $producto->nombre; ?></option>
                                              <?php } ?>
                              
                                         </select>
                                      </td>
                                      <td>
                                          Cantidad
                                          <input type="text" id="cantidad" name="cantidad"  class="form-control col-md-7 col-xs-12">
                                      </td>
                                      <td>
                                      <br>
                                        <a id="agregar_carrito" class="btn btn-primary">Agregar</a>
                                      </td>
                                  </tr>
                                </thead>
                              </table>
                            </div>
                          </div>
                            
                        </div>
                        <div class="form-group">
                            
                                  <div id="contenidodelcarrito"></div>
                                  
                                
                        </div>
	                      <div class="ln_solid"></div>
	                      <div class="form-group">
	                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	                          <a href="<?php echo site_url("Pedidos/index/");?>" class="btn btn-primary">Cancelar</a>
	                          <button type="" id="guardar" class="btn btn-success">Guardar</button>
	                        </div>
	                      </div>

	                    </form>
	                  </div>
	                </div>
	              </div>
	  </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="cerrar2"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"></h4>
          </div>
          <div class="modal-body">
            <p id="mensaje"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
          </div>
         </div>
       </div>
    </div>
    <!-- Modal Error-->
    <div class="modal fade" id="myModal_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id=""><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel_Error"></h4>
          </div>
          <div class="modal-body">
            <p id="mensaje_error"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="">Cerrar</button>
          </div>
         </div>
       </div>
    </div>
   </div>
</div>
<script >
  $(document).ready(function() 
              {
                $('#cerrar').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Pedidos')?>/";
                });
                $('#cerrar2').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Pedidos')?>/";
                });
              })
</script>
   <script type="text/javascript">
              $(document).ready(function() 
              {   var dni_repetido = 0;
                  $('#guardar').click(function()
                  {   
                     
                          $('#alta').validate
                          ({
                              rules: {
                                  birthday: { required: true},
                                  birthday2:{ required: true},
                              },
                              messages: {
                                  birthday:  {required: "Debe introducir el Nombre."}, 
                                  birthday2:  {required: "Debe introducir el Nombre."},
                              },
                              submitHandler: function(form)
                              {
                                  $.ajax
                                  ({
                                          url: '<?php echo site_url("/Pedidos/guardar/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: $('#alta').serialize(),
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              console.log(data);
                                              if (data.status)
                                              {
                                                  console.log(data);
                                                  //alert('El Cliente se registro en forma satifactoria');
                                                  document.getElementById("myModalLabel").innerHTML = 'Registro de Pedidos';
                                                  document.getElementById("mensaje").innerHTML = 'El Pedido se registro en forma satifactoria';
                                                  $("#myModal").modal('show');
                                                  jQuery.fn.reset = function () 
                                                  {
                                                      $(this).each (function() 
                                                          { this.reset(); 
                                                          });
                                                  }
                                                  $("#alta").reset();
                                                  //document.location.href = "<?php echo site_url('Login')?>/";
                                              }
                                              else
                                              {
                                                  //alert('El Cliente ya esta registrado');
                                                  document.getElementById("myModalLabel_Error").innerHTML = 'Error';
                                                  document.getElementById("mensaje_error").innerHTML = 'Error';
                                                  $("#myModal_error").modal('show');
                                              }
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                            // alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                                  });
                              }
                          });
                  });
              });
              </script>
              <script type="text/javascript">
          $(document).ready(function()
            {
            $('#cancelar').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Productos/index')?>/";

            });
          });    
        </script>
        <script>
      $(document).ready(function() {
        $('#birthday').daterangepicker({
          singleDatePicker: true,
          //calender_style: "picker_3",
        showDropdowns: true
          
        }, function(start, end, label) {
          console.log(start.toISOString(), end.format('YYYY-MM-DD'), label);
        });
        $('#birthday2').daterangepicker({
          singleDatePicker: true,
          //calender_style: "picker_3",
        showDropdowns: true
          
        }, function(start, end, label) {
          console.log(start.toISOString(), end.format('YYYY-MM-DD'), label);
        });
      });
    </script>
<script>
$(document).ready(function()
{
  /*$('#agregar_cuenta').click(function() 
  {
    alert('hola munedo');
  });*/
});      
      
</script>

    <script >
  $(document).ready(function() 
    {
      $('#agregar_carrito').click(function(event) {
                  
        //alert('hola');
        if(($("#cantidad").val()!= ''))
        {
          $.ajax
          ({
              url: '<?php echo site_url("Pedidos/agregar_items"); ?>',
                  dataType:'text',
                  type: 'POST',
                  data: $('#alta').serialize(),
                  success: function(data)
                  {
                    $("#cantidad").val('');
                    $("#contenidodelcarrito").html(data);
                    
                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                     //alert('error');
                      console.log(jqXHR);
                      console.log(textStatus);
                      console.log(errorThrown);
                  } 
          });
        }
          
      });
               
    })
</script>
<script>
  function eliminar_items(id)
  {
    //alert(id);
    $.ajax
          ({
              url: '<?php echo site_url("Pedidos/eliminar_items"); ?>',
                  dataType:'text',
                  type: 'get',
                  data: {id_individuo: id },
                  success: function(data)
                  {
                    $("#contenidodelcarrito").html(data);
                    
                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                     //alert('error');
                      console.log(jqXHR);
                      console.log(textStatus);
                      console.log(errorThrown);
                  } 
          });

  }
</script>
