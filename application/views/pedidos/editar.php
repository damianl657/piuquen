<script >
  id_pedido = <?php echo $pedido->id;?>
</script>
<div class="">
    <div class="page-title">
      <div class="title_left">
       
        
      </div>
      <div class="clearfix"></div>
	  <div class="row">
	              <div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="x_panel">
	                  <div class="x_title">
	                    <h2>Editar Pedido Seleccionado</h2>
	                    <ul class="nav navbar-right panel_toolbox">
	                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	                      </li>
	                      
	                      <li><a class="close-link"><i class="fa fa-close"></i></a>
	                      </li>
	                    </ul>
	                    <div class="clearfix"></div>
	                  </div>
	                  <div class="x_content">
	                    <br />
	                    <form id="alta" autocomplete="on" method="post" class="form-horizontal form-label-left">
                      <input type="hidden" name="id" value="<?=$pedido->id;?>" />
	                    <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha<span class="required">*</span>
                          </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="birthday" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" name="fecha" value="<?php echo date('d/m/Y', strtotime($pedido->fecha));?>">
                        </div>
                      </div>
                       <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Fecha de llegada<span class="required">*</span>
                          </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="birthday2" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" name="fecha_llegada" value="<?php echo date('d/m/Y', strtotime($pedido->fecha_llegada));?>">
                        </div>
                      </div>

	                     <div class="form-group">
                          <label for="basic" class="control-label col-md-3 col-sm-3 col-xs-12">Proveedor</label>
                            <select id="id_proveedor" class="selectpicker show-tick control-label col-md-3 col-sm-3 col-xs-12" data-live-search="true" required name="id_proveedor">
                              <option value="<?php echo $pedido->id_proveedor;?>"><?php echo $pedido->proveedores->nombre; ?></option>
                              <?php foreach ( $proveedores as $proveedor) { ?>
                                    <option  value="<?php echo $proveedor->id;?>"><?php echo $proveedor->nombre; ?></option>
                              <?php } ?>
                              
                            </select>
                        </div>
	                      <div class="form-group">
                          <div class="row">
                            <label for="basic" class="control-label col-md-3 col-sm-3 col-xs-12">Productos a Pedir</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>

                                  
                                  <tr>   
                                      <td>
                                          Producto
                                          <select id="id_producto" class="selectpicker show-tick control-label " data-live-search="true" required name="id_producto">
                                           
                                              <?php foreach ( $productos as $producto) { ?>
                                                    <option  value="<?php echo $producto->id;?>"><?php echo $producto->nombre; ?></option>
                                              <?php } ?>
                              
                                         </select>
                                      </td>
                                      <td>
                                          Cantidad
                                          <input type="text" id="cantidad" name="cantidad"  class="form-control col-md-7 col-xs-12">
                                      </td>
                                      <td>
                                      <br>
                                        <a id="agregar_carrito" class="btn btn-primary">Agregar</a>
                                      </td>
                                  </tr>
                                </thead>
                              </table>
                            </div>
                          </div>
                            
                        </div>
                        <div class="form-group">
                        <div id="contenidodelcarrito">
                                    <!--aca comienza el grupo familiar-->
                                    <br>
                                          
                                            <center><h2>Items Pedidos<small></small></h2></center>
                                          
                                            <table class="table table-striped table-bordered dt-responsive nowrap">
                                              <thead>
                                                <tr>
                                                  <th>Producto</th>
                                                    <th>Cantidad</th>
                                                    <th>Acciones</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                <tbody id="lista_items">
                                                                
                                                                    <?php
                                                                    
                                                                    foreach ( $pedido->items_pedidos as $items) { 
                                                                      if($items->estado == 1)
                                                                      {


                                                                        ?>
                                                                        <tr >
                                                                          
                                                                          
                                                                          <th>
                                                                              <?php 
                                                                              $p = Productos_eloquent::find($items->id_producto);
                                                                              echo $p->nombre;?>
                                                                          </th>
                                                                          <th>
                                                                              <?php //print_r($datos) ;
                                                                              echo $items->cantidad;?>
                                                                          </th>
                                                                          <th>
                                                                              <a class="btn btn-sm btn-danger" onclick="confirm_baja_items('<?php echo $items->id;?>')"  title="Eliminar"><i class="glyphicon glyphicon-trash"></i> </a>
                                                                             <a class="btn btn-sm btn-primary" onclick="editar_items('<?php echo $items->id;?>')" href="<?php echo site_url("Pedidos/modificar_items/".$items->id);?>" title="Editar" ><i class="glyphicon glyphicon-pencil"></i> </a>
                                                                               
                                                                          </th> 
                                                                            
                                                                       
                                                                        </tr>
                                            
                                                                    <?php 
                                                                    }
                                                                    }
                                                                    foreach ( $this->cart->contents() as $item) // recorrer cart por si agregaron anteriormente mienbros familiares
                                                                    { 
                                                                      if($item['name'] == 'pedido')
                                                                          {

                                                                              $datos = $item['options'];
                                                                              ?>
                                                                              <tr>
                                                                               
                                                                                <th>
                                                                                    <?php 
                                                                                    $p = Productos_eloquent::find($datos['id_producto']);
                                                                                    echo $p->nombre;?>
                                                                                </th>
                                                                                <th>
                                                                                    <?php echo $datos['cantidad'];?>
                                                                                </th>
                                                                                
                                                                                <th>
                                                                                    <a class="btn btn-sm btn-danger" onclick="eliminar_items('<?php echo $item['rowid'];?>')"  title="Eliminar"><i class="glyphicon glyphicon-trash"></i> </a>
                                                                                </th> 
                                                                                  
                                                                             
                                                                              </tr>
                                                
                                                                  <?php }
                                                                  
                                                                   }
                                                                    //echo $acu;
                                                                    ?>
                                                            </tbody>
                                            </table>
                                    <!--fin grupo familiar -->
                                  </div>
                        </div>
                        
	                      
                        
                        <br>
                        
                         
                       
	                      
	                      
	                      <div class="ln_solid"></div>
	                      <div class="form-group">
	                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	                          <a href="<?php echo site_url("Pedidos/index/");?>" class="btn btn-primary">Cancelar</a>
	                          <button type="" id="guardar" class="btn btn-success">Guardar</button>
	                        </div>
	                      </div>

	                    </form>
	                  </div>
	                </div>
	              </div>
	  </div>
    <!-- Modal Error-->
    <div class="modal fade" id="myModal_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id=""><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel_Error"></h4>
          </div>
          <div class="modal-body">
            <p id="mensaje_error"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="">Cerrar</button>
          </div>
         </div>
       </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="cerrar2"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"></h4>
        </div>
        <div class="modal-body">
          <p id="mensaje"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" id="cerrar">Cerrar</button>
        </div>
       </div>
     </div>
   </div>
   </div>
</div>
<script >
  $(document).ready(function() 
              {
                $('#cerrar').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Pedidos')?>/";
                });
                $('#cerrar2').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Pedidos')?>/";
                });
              })
</script>
   <script type="text/javascript">
              $(document).ready(function() 
              {
                  $('#guardar').click(function()
                  {    
                          $('#alta').validate
                          ({
                              rules: {
                                  nombre: { required: true},
                                  
                              },
                              messages: {
                                  nombre:  {required: "Debe introducir el Nombre."}, 
                                  
                              },
                              submitHandler: function(form)
                              {
                                  $.ajax
                                  ({
                                          url: '<?php echo site_url("/Pedidos/update/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: $('#alta').serialize(),
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              console.log(data);
                                              if (data.status)
                                              {
                                                  //console.log(data);
                                                  //alert('El Cliente se registro en forma satifactoria');
                                                  document.getElementById("myModalLabel").innerHTML = 'Pedido';
                                                  document.getElementById("mensaje").innerHTML = 'El Pedido se modificó en forma satifactoria';
                                                  $("#myModal").modal('show');
                                                  jQuery.fn.reset = function () 
                                                  {
                                                      $(this).each (function() 
                                                          { this.reset(); 
                                                          });
                                                  }
                                                  $("#alta").reset();
                                                  
                                                  //document.location.href = "<?php echo site_url('Login')?>/";
                                              }
                                              else
                                              {
                                                  //alert('El Cliente ya esta registrado');
                                                  document.getElementById("myModalLabel_Error").innerHTML = 'Error';
                                                  document.getElementById("mensaje_error").innerHTML = 'Error';
                                                  $("#myModal_error").modal('show');
                                                  
                                              }
                                              
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                                  });
                              }
                          });
                  });
              });
              </script>
              <script type="text/javascript">
          $(document).ready(function()
            {
            $('#cancelar').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Productos/index')?>/";

            });
          });    
        </script>
        <script>
      $(document).ready(function() {
        $('#birthday').daterangepicker({
          singleDatePicker: true,
          //calender_style: "picker_3",
        showDropdowns: true
          
        }, function(start, end, label) {
          console.log(start.toISOString(), end.format('YYYY-MM-DD'), label);
        });
        $('#birthday2').daterangepicker({
          singleDatePicker: true,
          //calender_style: "picker_3",
        showDropdowns: true
          
        }, function(start, end, label) {
          console.log(start.toISOString(), end.format('YYYY-MM-DD'), label);
        });
      });
    </script>
<script>
$(document).ready(function()
{
  /*$('#agregar_cuenta').click(function() 
  {
    alert('hola munedo');
  });*/
});      
      
</script>
<div class="modal fade" id="modal_baja_individuo" role="dialog">
          <div class="modal-dialog modal-lg" role="document" id="mdialTamanio">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="">Baja Items Producto</h4>
                 
              </div>
              <div class="modal-body">
                
                <form action="#" id="form_items_pedido" method="post" class="form-horizontal">
                  <input type="hidden" value="" name="id_items_pedido" id ="id_items_pedido"/> 
                    <div class="form-group">
                      <label>¿Desea dar de baja este items del Pedido?</label>
                      
                    </div>

                    <div class="clearfix"></div>

                  
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="baja_items_pedido">Aceptar</button>
              </div>
            </div>
          </div>
        </div>
  <script >
            function confirm_baja_items(id)
          {
            //alert(id);
             //document.getElementById("id_cliente").innerHTML = id;
             $("#id_items_pedido").val(id);
            $("#modal_baja_individuo").modal('show');
          }
          
          </script>
          <script >
          $(document).ready(function() 
              {
                $('#baja_items_pedido').click(function(event) {
                  
                  //alert('hola mundo');
                  $.ajax
                    ({
                                          url: '<?php echo site_url("/Pedidos/baja_items_pedido/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: $('#form_items_pedido').serialize(),
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              console.log(data);
                                              document.location.href = "<?php echo site_url('Pedidos/modificar')?>/"+data.id_pedido;
                                              
                                              
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                             //alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                    });
                  
                });

              })
        </script>
        <script >
  $(document).ready(function() 
    {
      $('#agregar_carrito').click(function(event) {
                  
        //alert('hola');
        if(($("#cantidad").val()!= ''))
        {
          $.ajax
          ({
              url: '<?php echo site_url("pedidos/agregar_items_2"); ?>',
                  dataType:'text',
                  type: 'POST',
                  data: $('#alta').serialize(),
                  success: function(data)
                  {
                    //accion ='';
                    //fila = '<tr ><th>'+$("#apellido_i").val()+'</th><th>'+$("#nombre_i").val()+'</th><th>'+$("#dni_i").val()+'</th><th>'+$("#birthday3").val()+'</th><th>'+$("#edad_i").val()+'</th>'+'</tr>';
                    //$("#lista_grupo_familiar").append(fila);
                    $("#lista_items").html(data);
                    $("#cantidad").val('');
                    
                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                     //alert('error');
                      console.log(jqXHR);
                      console.log(textStatus);
                      console.log(errorThrown);
                  } 
          });
        }
          
      });
               
    })
</script>
<script>
  function eliminar_items(id)
  {
    //alert(id);
    $.ajax
          ({
              url: '<?php echo site_url("Pedidos/eliminar_items_cart"); ?>',
                  dataType:'text',
                  type: 'get',
                  "headers": {    
                    "id_pedido" : id_pedido
                  },
                  data: {id_items: id },
                  success: function(data)
                  {
                    $("#lista_items").html(data);
                    
                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                     //alert('error');
                      console.log(jqXHR);
                      console.log(textStatus);
                      console.log(errorThrown);
                  } 
          });

  }
</script>