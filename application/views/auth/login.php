<!DOCTYPE html>
<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
        <link rel="stylesheet" href="<?=base_url('assets/bootstrap/dist/css/bootstrap.min.css')?>" media="screen">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?=base_url('assets/font-awesome/css/font-awesome.min.css')?>" media="screen">
        <!-- Custom Theme Style -->
        
        <link rel="stylesheet" href="<?=base_url('assets/css_inspinia/animate.css')?>" media="screen">
        <link rel="stylesheet" href="<?=base_url('assets/css_inspinia/style.css')?>" media="screen">
        <!-- jQuery -->
        <script src="<?=base_url('assets/jquery/dist/jquery.min.js')?>"></script>
        <!-- Bootstrap -->
        <script src="<?=base_url('assets/bootstrap/dist/js/bootstrap.min.js')?>"></script>
        <script src="<?=base_url('assets/js/jquery.validate.min.js')?>"></script>

    <title>Drogueria Piuquen | Login</title>

   

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
           
            <h3>Bienvenido a Drogueria Piuquen</h3>
            <p>
            </p>
            
            <form class="m-t form-horizontal" role="form" autocomplete="on" method="post" id="form_login">
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Username" id ="username_txt" name ="username_txt" required="">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password_txt" id="password_txt" placeholder="Password" required="">
                </div>
                 <div class="form-group" id ="empresas">
                                          
                </div>
                <button type="submit" id="ingresar" class="btn btn-primary block full-width m-b">Login</button>

                
                
            </form>
            <p class="m-t"> <small><i class="fa fa-user"> </i> Desarrollado por Damian Leal - Lic. Sistemas de Información - Contacto: <i class="fa fa-phone"></i> 2645671551 - <i class="fa fa-google-plus"></i> damianl657@gmail.com</small> </p>
        </div>
    </div>

<script type="text/javascript">
$(document).ready(function() 
{
    $('#ingresar').click(function()
    {    
            $('#form_login').validate
            ({
                rules: {
                    username_txt: { required: true},
                    password_txt: { required: true}
                    
                },
                messages: {
                    username_txt:  {required: "Debe introducir su Usuario."}, 
                    password_txt:  {required: "Debe Introducir su Password"}, 
                },
                submitHandler: function(form)
                {
                    $.ajax
                    ({
                            url: '<?php echo site_url("/Auth/registrar_login/"); ?>',
                            dataType:'text',
                            type: 'POST',
                            dataType: "JSON",
                            data: $('#form_login').serialize(),
                            success: function(data)
                            {
                                //alert('entro');
                                console.log(data);
                                if (data.status)
                                {
                                    
                                    
                                    

                                    document.location.href = "<?php echo site_url('home')?>/";
                                }
                                else
                                {
                                    alert('Usuario o Contraseña Incorrecta');
                                    //document.location.href = "<?php //echo site_url('Login/ingreso')?>/";
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown,data)
                            {
                              console.log(jqXHR);console.log(textStatus);
                                console.log(errorThrown);

                            }
                    });
                }
            });
    });
});
</script>     
<script type="text/javascript">
$(document).ready(function() 
{
    $('#empresas').change(function()
    {    
           //alert($('select[id=select_empresa]').val());
           if($('select[id=select_empresa]').val() !=0)
           {    
                var id_empresa = $('select[id=select_empresa]').val();
                $.ajax
                ({
                    url: '<?php echo site_url("/Auth/ingresar/"); ?>',
                    dataType:'text',
                    type: 'GET',
                    dataType: "JSON",
                    data: {id_empresa:id_empresa},
                    success: function(data)
                    {
                        console.log('ok');
                        console.log(data);
                        if(data.status == true)
                        {
                            console.log(data.empresa);
                            document.location.href = "<?php echo site_url('home')?>/";
                        }
                        
                        

                    },
                    error: function (jqXHR, textStatus, errorThrown,data)
                    {
                       //alert('error');
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                    
                    }
                });
           }
    }); 

});
</script>     
</body>

</html>