<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?=base_url('login/index');?>" class="site_title"></i> <span>Drogueria Piuquen</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">

                <img src="<?=base_url('assets/img/avatar_2x.png')?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bienvenido,</span>
                <h2><?=$this->session->userdata('first_name');?>, <?=$this->session->userdata('last_name');?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <br>
                <h3>Menu</h3>
                <ul class="nav side-menu">
                   <li><a href="<?=base_url('Login/index');?>"><i class="fa fa-home"></i> Home </a>
                    
                  </li>
                  <li><a><i class="fa fa-male"></i> Clientes <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?=base_url('Clientes/index');?>">Listar</a>
                      </li>
                      <li><a href="<?=base_url('Clientes/inactivos');?>">Listar Inactivos</a>
                      </li>
                      
                    </ul>
                  </li>
                   <li><a><i class="fa fa-android"></i> Soporte Tecnico <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?=base_url('Soportes_tecnicos/nuevo');?>">Registrar Ingreso a Soporte Tecnico</a>
                      </li>
                      <li><a href="<?=base_url('Soportes_tecnicos/index');?>">Equipos en Soporte Tecnico</a></a>
                      </li>
                    </ul>
                  </li>
                </ul> 
              </div>  
            </div> 
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" href="<?=base_url('login/salir');?>" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>