<!-- plantilla nueva-->
<!-- Main Container -->
<main id="main-container">
<!-- Page Content -->
<div class="content">
<!-- Default Elements -->
  <div class="block block-themed ">
    <div class="block-header bg-gd-sea">
      <h3 class="block-title"><a class="btn btn-secondary" href="javascript:window.history.back();"><i class="fa fa-arrow-left"></i></a>  </h3>
      <h3 class="block-title">Formulario de Egresos</h3>
      <div class="block-options">
      </div>
    </div>
  
    <div class="block-content block-content-full">
      <div class="row justify-content-center py-20">
        <form id="alta" autocomplete="off" method="post" class="form-horizontal form-label-left">
          
          <div class="col-xl-12">
            <input type="hidden" id="producto_alta" name="producto_alta" value="nuevo" />
            <input type="hidden" id="id_user_group" name="id_user_group" value="0" />
            <input type="hidden" id="user_id" name="user_id" value="0" />
           
            
            <div class="form-group">
              <div class="form-group row">
                <label class="col-lg-4 col-form-label" for="val-username">Fecha <span class="text-danger">*</span></label>
                <div class="col-lg-8">
                  <input type="text" class="form-control datepicker" id="fecha" name="fecha" >
                </div>
              </div>
            </div>
            <div class="form-group">
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="val-username">Servicio <span class="text-danger">*</span></label>
                    <div class="col-lg-8">
                        <select class="js-select2 form-control" id="id_servicio" name="id_servicio">
                            <option  value="ninguno">...</option>
                            <?php 
                                foreach($servicios as $servicio)
                                {
                                    echo "<option value=".$servicio->id.">".$servicio->nombre."</option>";
                                }
                            ?>
                                        
                                        
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-group row">
                    <label class="col-lg-4 col-form-label" for="val-username">Paciente <span class="text-danger">*</span></label>
                    <div class="col-lg-8">
                        <select class="js-select2 form-control" id="id_paciente" name="id_paciente">
                            <option  value="ninguno">...</option>
                            <?php 
                                foreach($pacientes as $paciente)
                                {
                                    echo "<option value=".$paciente->id.">".$paciente->nombre.' '.$paciente->apellido.' DNI:'.$paciente->dni."</option>";
                                }
                            ?>
                                        
                                        
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-grou prow">
                    <label for="basic" class="col-lg-4 col-form-label">Seleccione el Producto y la cantidad</label>
                    <div class="col-lg-8">
                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                                <tr>   
                                    <td>
                                          Producto
                                          <select id="id_producto" class="js-select2 form-control " data-live-search="true" required name="id_producto">
                                           
                                              <?php foreach ( $productos as $producto) { ?>
                                                    <option  value="<?php echo $producto->id;?>"><?php echo $producto->nombre; ?></option>
                                              <?php } ?>
                              
                                         </select>
                                    </td>
                                    <td>
                                          Cantidad
                                          <input type="text" id="cantidad" name="cantidad"  class="form-control ">
                                          <div id="notify_cantidad">

                                          </div>
                                    </td>
                                    <td>
                                      <br>
                                        <a id="agregar_carrito" class="btn btn-alt-success">Agregar</a>
                                    </td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                            
            </div>
            <div class="form-group">
                            
                <div id="contenidodelcarrito"></div>
                                  
                                
            </div>
            
          <div class="form-group row">
            <div class="col-lg-8 ml-auto">
              <a href="<?php echo site_url("Egresos_remito/index/");?>" class="btn btn-success">Cancelar</a>
              <button id ="guardar" type="guardar" class="btn btn-alt-primary">Guardar</button>
            </div>
          </div>

        </form>
      </div>
    </div><!-- END block-content block-content-full -->
  </div><!-- END block -->
</div><!-- END content -->          
</main>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-popout" role="document">
        
      <div class="modal-content">
        <div class="block block-themed block-transparent mb-0">
          <div class="block-header bg-primary-dark">
            <h3 class="block-title" id="myModalLabel"></h3>
            <div class="block-options">
              <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
              <i class="si si-close"></i>
              </button>
            </div>
          </div>
          <div class="block-content">
                  <p id="mensaje"></p>
          </div>
        </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-alt-success" id="cerrar" data-dismiss="modal">
                    <i class="fa fa-check"></i> Cerrar
                </button>
        </div>
      </div>
    </div>
</div>
  <!-- Modal Error-->
<div class="modal fade" id="myModal_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="block block-themed block-transparent mb-0">
          <div class="block-header bg-primary-dark">
            <h3 class="block-title" id="myModalLabel_Error"></h3>
            <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
            </div>
          </div>
          <div class="block-content">
                  <p id="mensaje_error"></p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-alt-success"  data-dismiss="modal">
          <i class="fa fa-check"></i> Cerrar
          </button>
        </div>
      </div>
    </div>
</div>
<script >
  $(document).ready(function() 
              {
                $('#cerrar').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Egresos_remito')?>/";
                });
                $('#cerrar2').click(function(event) {
                  
                  document.location.href = "<?php echo site_url('Egresos_remito')?>/";
                });
                $('#fecha').datepicker({
                    format: 'dd/mm/yyyy',
                    //startDate: '-3d'
                });
                $('#id_producto').select2({});
                $('#id_paciente').select2({});
                $('#id_servicio').select2({});
                $('#id_producto').change(function() {
                    $('#notify_cantidad').empty();
                    //
                    $('#notify_cantidad').empty();
                    var cantidad = $("#cantidad").val();
                    if(cantidad > 0){
                        var id_producto = $('select[name=id_producto]').val();
                        $.ajax
                        ({
                            url: '<?php echo site_url("/Productos/obtener_cantidad/"); ?>',
                            dataType:'text',
                            type: 'get',
                                    "headers": {    
                                        "id_producto" : id_producto
                                    },
                            dataType: "JSON",
                            data: {cantidad:cantidad},
                            success: function(data)
                            {
                                console.log('ok');
                                console.log(data);

                                if(data.status == true)
                                { //alert(data);
                                
                                
                                    //notificacion ='<div ><strong>'+data.mensaje+'</strong></div>';
                                    //alert('hola muendo');
                                    notificacion = '<div class="alert alert-info alert-dismissable" role="alert">'+
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                                            '<span aria-hidden="true">×</span>'+
                                        '</button>'+
                                        '<p class="mb-0">'+data.mensaje+'</p>'+
                                    '</div>';

                                    dni_repetido = 1;
                                    
                                
                                $('#notify_cantidad').append(notificacion);
                                //$('#notify_email').append(notificacion);
                                
                                }
                                else
                                {
                                $('#notify_cantidad').empty();
                                }
                                

                            },
                            error: function (jqXHR, textStatus, errorThrown,data)
                            {
                            //alert('error');
                                console.log(jqXHR);
                                console.log(textStatus);
                                console.log(errorThrown);
                            
                            }
                        }); 
                    }
                    //
                });
              })
</script>
<script type="text/javascript">
              $(document).ready(function() 
              {   var dni_repetido = 0;
                  $('#guardar').click(function()
                  {   
                        //$("#guardar").hide();
                          $('#alta').validate
                          ({
                              rules: {
                                  fecha: { required: true},
                                  birthday2:{ required: true},
                              },
                              messages: {
                                fecha:  {required: "Debe introducir la fecha."}, 
                                  birthday2:  {required: "Debe introducir el Nombre."},
                              },
                              submitHandler: function(form)
                              {
                                  $.ajax
                                  ({
                                          url: '<?php echo site_url("/Egresos_remito/guardar/"); ?>',
                                          dataType:'text',
                                          type: 'POST',
                                          dataType: "JSON",
                                          data: $('#alta').serialize(),
                                          success: function(data)
                                          {
                                              //alert('entro');
                                              console.log(data);
                                              if (data.status)
                                              {
                                                  console.log(data);
                                                  //alert('El Cliente se registro en forma satifactoria');
                                                  document.getElementById("myModalLabel").innerHTML = 'Remito';
                                                  document.getElementById("mensaje").innerHTML = 'El Remito se registro en forma satifactoria';
                                                  $("#myModal").modal('show');
                                                  jQuery.fn.reset = function () 
                                                  {
                                                      $(this).each (function() 
                                                          { this.reset(); 
                                                          });
                                                  }
                                                  $("#alta").reset();
                                                  $("#guardar").show();
                                                  //document.location.href = "<?php echo site_url('Login')?>/";
                                              }
                                              else
                                              {
                                                  //alert('El Cliente ya esta registrado');
                                                  document.getElementById("myModalLabel_Error").innerHTML = 'Error';
                                                  document.getElementById("mensaje_error").innerHTML = 'Error';
                                                  $("#myModal_error").modal('show');
                                              }
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                            // alert('error');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                                  });
                              }
                          });
                  });
              });
              
          $(document).ready(function()
            {
            $('#cancelar').click(function() 
            {
              //alert('hola');
              document.location.href = "<?php echo site_url('Egresos_remito/index')?>/";

            });
          });    
</script>
        
<script>
$(document).ready(function()
{
  /*$('#agregar_cuenta').click(function() 
  {
    alert('hola munedo');
  });*/
});      
      
</script>

<script >
  $(document).ready(function() 
    {
      $('#agregar_carrito').click(function(event) {
                  
        //alert('hola');
        if(($("#cantidad").val()!= ''))
        {
          $.ajax
          ({
              url: '<?php echo site_url("Egresos_remito/agregar_items"); ?>',
                  dataType:'text',
                  type: 'POST',
                  data: $('#alta').serialize(),
                  success: function(data)
                  {
                    $("#cantidad").val('');
                    $("#contenidodelcarrito").html(data);
                    
                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                     //alert('error');
                      console.log(jqXHR);
                      console.log(textStatus);
                      console.log(errorThrown);
                  } 
          });
        }
          
      });
               
    })
</script>
<script>
  function eliminar_items(id)
  {
    //alert(id);
    $.ajax
          ({
              url: '<?php echo site_url("Egresos_remito/eliminar_items"); ?>',
                  dataType:'text',
                  type: 'get',
                  data: {id_items: id },
                  success: function(data)
                  {
                    $("#contenidodelcarrito").html(data);
                    
                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                     //alert('error');
                      console.log(jqXHR);
                      console.log(textStatus);
                      console.log(errorThrown);
                  } 
          });

  }
</script>
<script>
$(document).ready(function()
{
  $('#cantidad').keyup(function(e)
  {
    //alert('hola');
    $('#notify_cantidad').empty();
    var cantidad = $("#cantidad").val();
    var id_producto = $('select[name=id_producto]').val();
    $.ajax
    ({
        url: '<?php echo site_url("/Productos/obtener_cantidad/"); ?>',
        dataType:'text',
        type: 'get',
                  "headers": {    
                    "id_producto" : id_producto
                  },
        dataType: "JSON",
        data: {cantidad:cantidad},
        success: function(data)
        {
            console.log('ok');
            console.log(data);

            if(data.status == true)
            { //alert(data);
              
              
                //notificacion ='<div ><strong>'+data.mensaje+'</strong></div>';
                notificacion = '<div class="alert alert-info alert-dismissable" role="alert">'+
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                                            '<span aria-hidden="true">×</span>'+
                                        '</button>'+
                                        '<p class="mb-0">'+data.mensaje+'</p>'+
                                    '</div>';
                //alert('hola muendo');
                dni_repetido = 1;
                
              
               $('#notify_cantidad').append(notificacion);
              //$('#notify_email').append(notificacion);
              
            }
            else
            {
              $('#notify_cantidad').empty();
            }
            

        },
        error: function (jqXHR, textStatus, errorThrown,data)
        {
           //alert('error');
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        
        }
    });
  });
  
  
});
</script>
