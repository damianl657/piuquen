<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <title>Drogueria Piuquen - Servicio</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
         
        
        
        
        
        
    </head>
   
        <body>
<div class="panel panel-primary" id="myPrintArea">
   <div class="container">
       <table style="width:100%">
           <tr>
                <th style="width:50%">
                            
                </th>
                <th style="width:50%">
                    <div class="pull-right">
                    
                    <br>
                    <br>
                    <br>
                     <br>
                    <br>
                        <p style="font-size:18"><?php echo substr($egreso->fecha,8,2).'/'.substr($egreso->fecha,5,2).'/'.substr($egreso->fecha,0,4);?></p>
                    <br>
                    <br>
                    <br>
                    </div>
                </th>
          </tr>
       </table>
   </div>
   <?php
   if($egreso->id_servicio != 0)
    {
      ?>
      
       <table width="100%" style="border-collapse:collapse; font-size:13">
           <tr>
               <td width="30%"> </td>
               <td height="35"><strong><?php echo $egreso->servicios->razon_social;?> </strong></td>
                   
                
          </tr>
       </table>
      
       <table  width="100%" style="border-collapse:collapse; font-size:13px">
           <tr>
               <td width="10%"> </td>
               <td height="18"><strong><?php echo $egreso->servicios->direccion; ?></strong> </td>
          </tr>
       </table>
        <table  width="100%" style="border-collapse:collapse; font-size:13px">
           <tr>
               <td width="10%"></td>
               <td height="35" width="40%"> <!-- aqui va el iva--></td>
               <td height="35" width="20%"></td>
               <td height="35"><strong>30-70949057-1 </strong></td>
          </tr>
       </table>
       <table     width="100%" style="border-collapse:collapse; font-size:13">
           <tr>
               <td width="25%"> </td>
               <td>
                
               </td>
                   
                
          </tr>
       </table>
      <?
                                                                    
    }
    
   ?>
  
   
                  
                    
                    <br>
    <div class="panel-body">
        <div class="container">
            
            <div class="table-responsive">
            <br>
            <br>
            <br>
                <table border="1" cellspacing="1" cellpadding="1"  width="95%" style="border-collapse:collapse; font-size:13px">
                  <thead>
                    <tr id="">
                        <th class="" width="10%">cant</th>
                        <th class="">Descripcion</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    foreach ( $egreso->items_remito_egreso as $item) 
                    { 
                    ?>
                      <tr>
                        <td><center>
                            <?php echo $item->cantidad;?></center>
                        </td>
                        <td>
                            <?php 
                              if($item->lotes->productos->con_vencimiento == '0')
                             {
                                echo $item->lotes->productos->nombre.' Lote: '.$item->lotes->lote.' Sin Vto';
                            }
                            else
                            { $fecha_vencimiento = substr($item->lotes->fecha_vencimiento,8,2).'-'.substr($item->lotes->fecha_vencimiento,5,2).'-'.substr($item->lotes->fecha_vencimiento,0,4);
                              echo $item->lotes->productos->nombre.' Lote: '.$item->lotes->lote.' Vto: '.$fecha_vencimiento;
                            }?>
                        </td>
                      </tr>
                            
                                              <?php 
                                                }
                                                    //echo $acu;
                                                    ?>
                  </tbody>
                 </table>
       
              
          </div>
    </div>    
  </div>        
        <script type="text/javascript">
        function imprimir(){
          if (parseInt(navigator.appVersion)>4)
            window.print();
        }
        </script>
   
</body>
</html>
        